const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs'); 
const moment = require('moment'); 
const path = require('path');
const formidable = require('formidable');
var cron = require('node-cron');
var hmacSha1 = require("hmac_sha1")
var Base64 = require('js-base64').Base64;
var md5 = require('md5');
var crypto = require('crypto');
const plaid = require('plaid');
var bodyParser = require('body-parser');
const { loanPayment} = require('./student-methodfi');
// var expressWinston = require('express-winston');
// Logger configuration
const winston = require('winston');
const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: 'logs/sila/sila_controller.'+moment().format('YYYY_MM_DD')+'.log'
        })
    ]
};
const logger_main = winston.createLogger(logConfiguration);
//const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
const plaidClient = new plaid.Client({
  clientID: config.app.PLAID_CLIENT_ID,
  secret: config.app.PLAID_SECRET,
  env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
var http = require("https");
var Request = require("request");
// Initialize WebHooks module.
var WebHooks = require('node-webhooks');
//SILA SETUP
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();

//KMS SETUP
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');

// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);
// const io = require('socket.io')(3111);
// io.on('connection', (socket) => {
//   console.log('A client just joined on');
//   io.emit('data', { some: 'data' });
// });


module.exports = {
    webHook: async(req, res, next)=>{
        try{
            console.log(req.body);
            // console.log("WEBHOOK", res);
            var eventtype = req.body.event_type;
            var status = req.body.event_details.outcome; 
            var getid = req.body.event_details.entity; 
            var student = getid.replace("id", "Id");
            var getstudent = await models.student.findOne({'studentId': student});
            console.log(student);
            if(eventtype == 'kyc'){
                if( status == 'pending' ) {
                    global.io.emit('kycpending', { 'kyc':req.body, 'kycType': getstudent.Status.kycType, 'verification_status': getstudent.Status.isKYCVerified });
                } else if(status == 'passed') {
                    global.io.emit('kycverification', { 'kyc':req.body, 'kycType': getstudent.Status.kycType, 'verification_status': getstudent.Status.isKYCVerified });
                    if(getstudent.Status.kycType == 'noKyc') {
                        await models.student.updateOne({ 'studentId': student }, { $set: { "Status.isKYCVerified": true, "Status.kycType": "fullKyc" } }, async function (kycerr,kycitem) {
                            if(kycerr) {
                                console.log(kycerr);
                            } else {
                                await models.student.findOne({ 'studentId': student }, async function (studenterror, studentitem) {
                                    if (studenterror) {
                                        console.log(studenterror);
                                    }
                                    else {
                                        if(studentitem.EmailId != undefined) {
                                            var msg = {
                                                to: studentitem.EmailId,
                                                from: 'app@sloanapp.com',
                                                templateId: 'd-b6cc5041a1514475a2d4c7f323118818',
                                                dynamicTemplateData: {
                                                    name: studentitem.fname+' '+studentitem.lname,
                                                },
                                            };
                                            sgMail.send(msg);
                                        }
                                    }
                                })
                                console.log(kycitem);
                            }
                        });
                    } else {
                        await models.student.updateOne({ 'studentId': student }, { $set: { "Status.isKYCVerified": true, "Status.kycType": "noKyc" } }, async function (kycerr,kycitem) {
                            if(kycerr) {
                                console.log(kycerr);
                            } else {
                                await models.student.findOne({ 'studentId': student }, async function (studenterror, studentitem) {
                                    if (studenterror) {
                                        console.log(studenterror);
                                    }
                                    else {
                                        if(studentitem.EmailId != undefined) {
                                            var msg = {
                                                to: studentitem.EmailId,
                                                from: 'app@sloanapp.com',
                                                templateId: 'd-b6cc5041a1514475a2d4c7f323118818',
                                                dynamicTemplateData: {
                                                    name: studentitem.fname+' '+studentitem.lname,
                                                },
                                            };
                                            sgMail.send(msg);
                                        }
                                    }
                                })
                                console.log(kycitem);
                            }
                        });

                    }
                } else if(status == 'review') {
                    global.io.emit('kycreview', { 'kyc':req.body, 'kycType': getstudent.Status.kycType, 'verification_status': getstudent.Status.isKYCVerified });
                    await models.student.updateOne({ 'studentId': student }, { $set: { "Status.isKYCVerified": false } }, async function (kycerr,kycitem) {
                        if(kycerr) {
                            console.log(kycerr);
                        } else {
                            await models.student.findOne({ 'studentId': student }, async function (studenterror, studentitem) {
                                if (studenterror) {
                                    console.log(studenterror);
                                }
                                else {
                                    console.log(studentitem);
                                }
                            })
                            console.log(kycitem);
                        }
                    });
                } else {
                    global.io.emit('kycfailed', { 'kyc':req.body });
                    await models.student.updateOne({ 'studentId': student }, { $set: { "Status.isKYCVerified": false } }, async function (kycerr,kycitem) {
                        if(kycerr) {
                            console.log(kycerr);
                        } else {
                            await models.student.findOne({ 'studentId': student }, async function (studenterror, studentitem) {
                                if (studenterror) {
                                    console.log(studenterror);
                                }
                                else {
                                    console.log(studentitem);
                                }
                            })
                            console.log(kycitem);
                        }
                    });
                }
            }       
            if(eventtype == "transaction" && status == "success") {
                var studentId = req.body.event_details.entity;
                var tid = req.body.event_details.transaction;                
                console.log(tid);
                // if(studentId == 'sloanreserve') {


                //     await models.payment.findOne({ 'paymentId': tid }, async function (error, item) {
                //         if(error) {
                //             console.log(error);
                //         } else {
                //             console.log(item.status);
                //             if(item.status == "pending") {
                //                 // await models.payment.findOne({ 'paymentId': tid, "status": "pending" }, async function (getpaymentitem, getpaymenterror) {
                //                 //     if(getpaymenterror){
                //                 //         console.log('Could not find payment');
                //                 //     }
                //                 //     else {
                //                         await models.payment.updateOne({ 'paymentId': tid, "status": "pending" }, { $set: { "status": "transfer" } }, async function (update_error, update_item) {
                //                             if(update_error) {
                //                                 console.log(update_error);
                //                             } else {
                //                                 await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                //                                     if(get_error) {
                //                                         console.log(get_error);
                //                                     } else {
                //                                         const reserveWalletKey = config.reserve.walletKey;
                //                                         const reserveWalletAddress = config.reserve.walletAddress;
                //                                         var amount = (parseFloat(item.amount)*100);
                //                                         var userHandle = config.reserve.handle;
                //                                         var wpk = await decryptKeys(reserveWalletKey);
                //                                         var msg = "Transfer Funds";
                //                                         var destination = item.studentId+".silamoney.eth";
                //                                         var destination_wallet = await decryptKeys(get_item.walletKey);
                //                                         var destination_address = await decryptKeys(get_item.walletAddress);
                //                                         var id = item._id;
                //                                         await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                //                                         .then(async (response) => {
                //                                             await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                //                                                 if(trans_error) {
                //                                                     console.log(trans_error);
                //                                                 } else {
                //                                                     console.log(trans_item);
                //                                                 }
                //                                             });
                //                                         })
                //                                         .catch((err) => { 
                //                                             console.log(err);
                //                                         });
                //                                     }
                //                                 });
                //                             }
                //                         });
                //                 //     }
                //                 // });
                //             } 
                //             if(item.status == "transfer") {
                //                 await models.payment.updateOne({ 'paymentId': tid, "status": "transfer" }, { $set: { "status": "redeem" } }, async function (update_error, update_item) {
                //                     if(update_error) {
                //                         console.log(update_error);
                //                     } else {
                //                         await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                //                             if(get_error) {
                //                                 console.log(get_error);
                //                             } else {
                //                                 const reserveWalletKey = config.reserve.walletKey;
                //                                 var amount = parseFloat(item.amount)*100;
                //                                 var userHandle = item.studentId+".silamoney.eth";
                //                                 var wpk = await decryptKeys(reserveWalletKey);
                //                                 var msg = "Redeem Funds";
                //                                 var account_name = "SLOAN_RESERVE";
                //                                 var id = item._id;
                //                                 await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
                //                                 .then(async (response) => {
                //                                     await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                //                                         if(trans_error) {
                //                                             console.log(trans_error);
                //                                         } else {
                //                                             console.log(trans_item);
                //                                         }
                //                                     });
                //                                 })
                //                                 .catch((err) => { 
                //                                     console.log(err);
                //                                 });
                //                             }
                //                         });
                //                     }
                //                 });
                //             }
                //             if(item.status == "redeem") {
                //                 await models.payment.updateOne({ 'paymentId': tid, "status": "redeem" }, { $set: { "status": "success" } }, async function (update_error, update_item) {
                //                     if(update_error) {
                //                         console.log(update_error);
                //                     } else {
                //                         await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                //                            if(get_error) {
                //                                 console.log(get_error);
                //                             } else {
                //                                 console.log(get_item);
                //                             }
                //                         })
                //                     }
                //                 });
                //             }
                //         }
                //     });


                // }
                await models.payment.findOne({ 'paymentId': tid }, async function (error, item) {
                    if(error) {
                        logger_main.info("Error > " + error);
                    } else {
                        console.log(item.status);
                        if(item.status == "pending") {
                            // await models.payment.findOne({ 'paymentId': tid, "status": "pending" }, async function (getpaymentitem, getpaymenterror) {
                            //     if(getpaymenterror){
                            //         console.log('Could not find payment');
                            //     }
                            //     else {
                                    await models.payment.updateOne({ 'paymentId': tid, "status": "pending" }, { $set: { "status": "transfer" } }, async function (update_error, update_item) {
                                        if(update_error) {
                                            console.log(update_error);
                                        } else {
                                            if(item.paymentType == 'loan') {
                                                await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                                    if(get_error) {
                                                        console.log(get_error);
                                                    } else {
                                                        const fboWalletKey = config.fboaccount.walletKey;
                                                        const fboWalletAddress = config.fboaccount.walletAddress;
                                                        var amount = (parseFloat(item.amount)*100) - parseFloat((config.app.FEE_AMOUNT)*100);
                                                        var userHandle = item.studentId+".silamoney.eth";
                                                        var wpk = await decryptKeys(get_item.walletKey);
                                                        var msg = "Transfer Funds";
                                                        var destination = config.fboaccount.handle;
                                                        var destination_wallet = await decryptKeys(fboWalletKey);
                                                        var destination_address = await decryptKeys(fboWalletAddress);
                                                        var id = item._id;
                                                        await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                                                        .then(async (response) => {
                                                            await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                                if(trans_error) {
                                                                    console.log(trans_error);
                                                                } else {
                                                                    logger_main.info("Transactions > " + trans_item);
                                                                }
                                                            });
                                                        })
                                                        .then(async (response1) => {
                                                            var fees = parseFloat((config.app.FEE_AMOUNT)*100);
                                                            const destWalletKey = config.feeaccount.walletKey;
                                                            const destWalletAddress = config.feeaccount.walletAddress;
                                                            var fee_dest = config.feeaccount.handle;
                                                            var dest_wallet = await decryptKeys(destWalletKey);
                                                            var dest_address = await decryptKeys(destWalletAddress);
                                                            var dest_msg = "Sloan Fees";
                                                            await Sila.transferSila(fees, userHandle, wpk, fee_dest, dest_wallet, dest_address, dest_msg)
                                                        })
                                                        .catch((err) => { 
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            } else if(item.paymentType == 'roundups'){
                                                await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                                    if(get_error) {
                                                        console.log(get_error);
                                                    } else {
                                                        const fboWalletKey = config.fboaccount.walletKey;
                                                        const fboWalletAddress = config.fboaccount.walletAddress;
                                                        var amount = (parseFloat(item.amount)*100);
                                                        var userHandle = studentId+".silamoney.eth";
                                                        var wpk = await decryptKeys(get_item.walletKey);
                                                        var msg = "Transfer Funds";
                                                        var destination = config.fboaccount.handle;
                                                        var destination_wallet = await decryptKeys(fboWalletKey);
                                                        var destination_address = await decryptKeys(fboWalletAddress);
                                                        var id = item._id;
                                                        await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                                                        .then(async (response) => {
                                                            await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                                if(trans_error) {
                                                                    console.log(trans_error);
                                                                } else {
                                                                    logger_main.info("Transactions > " + trans_item);
                                                                }
                                                            });
                                                        })
                                                        .catch((err) => { 
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            } else if(item.paymentType == 'roundup_fees'){
                                                await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                                    if(get_error) {
                                                        console.log(get_error);
                                                    } else {
                                                        const feeWalletKey = config.feeaccount.walletKey;
                                                        const feeWalletAddress = config.feeaccount.walletAddress;
                                                        var amount = (parseFloat(item.amount)*100);
                                                        var userHandle = studentId+".silamoney.eth";
                                                        var wpk = await decryptKeys(get_item.walletKey);
                                                        var msg = "Transfer Round Up Fees";
                                                        var destination = config.feeaccount.handle;
                                                        var destination_wallet = await decryptKeys(feeWalletKey);
                                                        var destination_address = await decryptKeys(feeWalletAddress);
                                                        var id = item._id;
                                                        await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                                                        .then(async (response) => {
                                                            await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                                if(trans_error) {
                                                                    console.log(trans_error);
                                                                } else {
                                                                    logger_main.info("Transactions > " + trans_item);
                                                                }
                                                            });
                                                        })
                                                        .catch((err) => { 
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            }
                                            else if(item.paymentType == 'giftcard'){
                                                await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                                    if(get_error) {
                                                        console.log(get_error);
                                                    } else {
                                                        const reserveWalletKey = config.reserve.walletKey;
                                                        const reserveWalletAddress = config.reserve.walletAddress;
                                                        var amount = (parseFloat(item.amount)*100);
                                                        var userHandle = config.reserve.handle;
                                                        var wpk = await decryptKeys(reserveWalletKey);
                                                        var msg = "Transfer Funds";
                                                        var destination = item.studentId+".silamoney.eth";
                                                        var destination_wallet = await decryptKeys(get_item.walletKey);
                                                        var destination_address = await decryptKeys(get_item.walletAddress);
                                                        var id = item._id;
                                                        await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                                                        .then(async (response) => {
                                                            await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                                if(trans_error) {
                                                                    console.log(trans_error);
                                                                } else {
                                                                    logger_main.info("Transactions > " + trans_item);
                                                                }
                                                            });
                                                        })
                                                        .catch((err) => { 
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    });
                            //     }
                            // });
                        } 
                        if(item.status == "transfer") {
                            await models.payment.updateOne({ 'paymentId': tid, "status": "transfer" }, { $set: { "status": "redeem" } }, async function (update_error, update_item) {
                                if(update_error) {
                                    console.log(update_error);
                                } else {
                                    if(item.paymentType == 'giftcard'){
                                        await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                            if(get_error) {
                                                console.log(get_error);
                                            } else {
                                                const reserveWalletKey = config.reserve.walletKey;
                                                var amount = parseFloat(item.amount)*100;
                                                var userHandle = item.studentId+".silamoney.eth";
                                                var wpk = await decryptKeys(reserveWalletKey);
                                                var msg = "Redeem Funds";
                                                var account_name = "SLOAN_RESERVE";
                                                var id = item._id;
                                                await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
                                                .then(async (response) => {
                                                    await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                        if(trans_error) {
                                                            console.log(trans_error);
                                                        } else {
                                                            logger_main.info("Transactions > " + trans_item);
                                                        }
                                                    });
                                                })
                                                .catch((err) => { 
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    } else if(item.paymentType == 'loan') {
                                        await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                            if(get_error) {
                                                console.log(get_error);
                                            } else {
                                                const fboWalletKey = config.fboaccount.walletKey;
                                                var amount = parseFloat(item.amount)*100;
                                                var userHandle = config.fboaccount.handle;
                                                var wpk = await decryptKeys(fboWalletKey);
                                                var msg = "Redeem Funds";
                                                var account_name = "FBO_Bank_New";
                                                var id = item._id;
                                                await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
                                                .then(async (response) => {
                                                    await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                        if(trans_error) {
                                                            console.log(trans_error);
                                                        } else {
                                                            logger_main.info("Transactions > " + trans_item);
                                                        }
                                                    });
                                                })
                                                .catch((err) => { 
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    } else if(item.paymentType == 'roundups'){
                                        await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                            if(get_error) {
                                                console.log(get_error);
                                            } else {
                                                const fboWalletKey = config.fboaccount.walletKey;
                                                var amount = parseFloat(item.amount)*100;
                                                var userHandle = config.fboaccount.handle;
                                                var wpk = await decryptKeys(fboWalletKey);
                                                var msg = "Redeem Funds";
                                                var account_name = "FBO_Bank_New";
                                                var id = item._id;
                                                await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
                                                .then(async (response) => {
                                                    await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                        if(trans_error) {
                                                            console.log(trans_error);
                                                        } else {
                                                            logger_main.info("Transactions > " + trans_item);
                                                        }
                                                    });
                                                })
                                                .catch((err) => { 
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        if(item.status == "redeem") {
                            await models.payment.updateOne({ 'paymentId': tid, "status": "redeem" }, { $set: { "status": "success" } }, async function (update_error, update_item) {
                                if(update_error) {
                                    console.log(update_error);
                                } else {
                                    if(item.paymentType == 'loan') {
                                        await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                            // await models.institute.findOne({ 'ins_id': item.loan_ins }, async function (get_errorx, get_itemx) {
                                            //     var dateObj = new Date(Math.round((new Date()).getTime()))
                                            //     var dateString = dateObj.toGMTString()
                                            //     hmac_Sha1 = new hmacSha1('base64');
                                            //     // var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`, `application/json,,/transactions,${dateString}`);
                                            //     var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`, `application/json,,/transactions,${dateString}`)
                                                
                                            //     var url = 'https://apix.arcusapi.com/transactions';
                                            //     //let hash = crypto.createHash('md5').update(a, 'utf8').digest('base64');
                                            //     let headers1;
                                            //     headers1 = {
                                            //         'Authorization': `APIAuth c24da1160d5f4563190e5cb6e0103094:${sha1EncodedHash}`,
                                            //         'X-Date': `${dateString}`,
                                            //         'Content-Type': 'application/json',
                                            //         'Accept': 'application/vnd.regalii.v3.2+json',
                                            //         'Sec-Fetch-Mode': 'cors'
                                            //     };
                                            //     var postdta = {
                                            //       "rpps_biller_id": get_itemx.rpps_biller_id,
                                            //       "account_number": item.loanAccount,
                                            //       "phone_number": get_item.mobileNo,
                                            //       "amount": item.amount,
                                            //       "currency": "USD",
                                            //       "payer_born_on": get_item.dob,
                                            //       "first_name": get_item.fname,
                                            //       "last_name": get_item.lname,
                                            //       "address": {
                                            //         "street": get_item.address.address1,
                                            //         "zip_code": get_item.address.zip,
                                            //         "city": get_item.address.city,
                                            //         "state": get_item.address.state
                                            //       }
                                            //     };
                                            //     var options = {
                                            //         json: true,
                                            //         url: url,
                                            //         method: 'POST',
                                            //         headers: headers1,
                                            //         body: postdta,
                                            //     }
                                            //     Request(options, function (error, response, body) { 
                                            //         logger_main.info("Loan Payment > " + response.body);
                                            //     })
                                            // })
                                            var requestLoandata = {
                                                "amount": item.amount,
                                                "destination": item.loanToken,
                                                "source": item.bankToken
                                            };

                                            var payment = await loanPayment(requestLoandata);
                                            logger_main.info("Loan Payment > " + payment);
                                            
                                        })
                                    } 
                                    else if(item.paymentType == 'giftcard') {
                                        if(item.status == "redeem") {
                                            await models.payment.updateOne({ 'paymentId': tid, "status": "redeem" }, { $set: { "status": "success" } }, async function (update_error, update_item) {
                                                if(update_error) {
                                                    console.log(update_error);
                                                } else {
                                                    await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                                       if(get_error) {
                                                            console.log(get_error);
                                                        } else {
                                                            console.log(get_item);
                                                        }
                                                    })
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
            console.log(req.body);
            res.status(200).end();
        } catch(e) {
            console.log('Error',e);
        }
    },
    test_ins: async(req, res, next)=>{
        try{
            let bankname = req.body.bankname ? req.body.bankname : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Bank Name",
                isError: true
            });
            let aggrQuery = [
                { '$match': { $or: [
                    {'name': RegExp(bankname, 'i')}
                ]}}
            ];
            await models.institute.aggregate(aggrQuery).exec( async function(searchErr, searchedItem){
                if(!searchErr){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchedItem[0].rpps_biller_id
                    })
                }
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            });
        }
    },
    payment_loan_manual: async(req, res, next) => {
    	try {
    		let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Student Id",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Loan Amount",
                isError: true
            });
            let loan_ins = req.body.loan_ins ? req.body.loan_ins : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Loan institute",
                isError: true
            });
            let loanAccount = req.body.loanAccount ? req.body.loanAccount : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Loan Account",
                isError: true
            });
    		await models.student.findOne({ 'studentId': studentId }, async function (get_error, get_item) {
	            await models.institute.findOne({ 'ins_id': loan_ins }, async function (get_errorx, get_itemx) {
	                var dateObj = new Date(Math.round((new Date()).getTime()))
	                var dateString = dateObj.toGMTString()
	                hmac_Sha1 = new hmacSha1('base64');
	                // var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`, `application/json,,/transactions,${dateString}`);
	                var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`, `application/json,,/transactions,${dateString}`)
	                
	                var url = 'https://apix.arcusapi.com/transactions';
	                //let hash = crypto.createHash('md5').update(a, 'utf8').digest('base64');
	                let headers1;
	                headers1 = {
	                    'Authorization': `APIAuth c24da1160d5f4563190e5cb6e0103094:${sha1EncodedHash}`,
	                    'X-Date': `${dateString}`,
	                    'Content-Type': 'application/json',
	                    'Accept': 'application/vnd.regalii.v3.2+json',
	                    'Sec-Fetch-Mode': 'cors'
	                };
	                var postdta = {
	                  "rpps_biller_id": get_itemx.rpps_biller_id,
	                  "account_number": loanAccount,
	                  "phone_number": get_item.mobileNo,
	                  "amount": amount,
	                  "currency": "USD",
	                  "payer_born_on": get_item.dob,
	                  "first_name": get_item.fname,
	                  "last_name": get_item.lname,
	                  "address": {
	                    "street": get_item.address.address1,
	                    "zip_code": get_item.address.zip,
	                    "city": get_item.address.city,
	                    "state": get_item.address.state
	                  }
	                };
	                var options = {
	                    json: true,
	                    url: url,
	                    method: 'POST',
	                    headers: headers1,
	                    body: postdta,
	                }
	                Request(options, function (error, response, body) { 
	                    res.json({
	                        isError: false,
	                        message: errorMsgJSON['ResponseMsg']['200'],
	                        statuscode: 200,
	                        details: response.body
	                    })
	                })
	            })
	        })

    	} catch(e) {
    		res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            });
    	}
    },
    webHookArcus: async(req, res, next)=>{
        logger_main(req,body);
    }
}

// webHooks = new WebHooks({
//     db: {"checkEvents": ["13.56.117.240:3000/webhook"]}, // just an example
// })

// var emitter = webHooks.getEmitter()
 
// emitter.on('*.success', function (shortname, statusCode, body) {
//     console.log('Success on trigger webHook' + shortname + 'with status code', statusCode, 'and body', body)
// })

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}

async function findInstitution(data) {
    let aggrQuery = [
        { '$match': { $or: [
            {'name': data}
        ]}}
    ]
    await models.institute.aggregate(aggrQuery).exec( async function(searchErr, searchedItem){
        if(!searchErr){
            var bankid = searchedItem[0].rpps_biller_id;
            console.log(bankid);
            return bankid;
        }
    });
}