const mongoose = require('mongoose');
const models = require('../model/common');
const bcrypt = require("bcrypt");
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
var Request = require("request");
//SILA SETUP
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
//KMS SETUP
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);
module.exports = {   
    register:(req, res, next)=>{
        try{
            let fname = req.body.fname ? req.body.fname : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - First Name",
                isError: true,
            });
            let lname = req.body.lname ? req.body.lname : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Last Name",
                isError: true,
            });
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Password",
                isError: true,
            });
            var dx = new Date();
            var nx = dx.getTime();
            var customToken = nx;
            console.log(emailIdTakenFromUser,':',password,':',nx);
            if (!req.body.EmailId || !req.body.password) { return; }
            let emailId=emailIdTakenFromUser.toLowerCase().trim();
            let type = "sloanId";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            let d = new Date();
            let timeAnddate = d.toISOString();
            let findQuery={'EmailId':emailId};
            models.student.findOne(findQuery, function (error, result) {
                if(error){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    console.log(result);
                    if(result == null ){
                        bcrypt.genSalt(10, (err, salt) => {
                            bcrypt.hash(password, salt, (err, hashedPass) => {
                                let insertedValues={
                                    '_id': new mongoose.Types.ObjectId(),'fname':fname,'lname':lname,'studentId':autouserid,'EmailId':emailId,
                                    'Status.signedUpVia':'PAYOFF','salt':salt,'memberSince':timeAnddate,'password':hashedPass,
                                    'customToken':customToken, 'isLoggedIn': true
                                }
                                 models.student.create(insertedValues, function (insertedError, insertedResult) {
                                    if(insertedError){
                                        res.json({
                                            isError: true,
                                            message:  errorMsgJSON['ResponseMsg']['404'],
                                            statuscode:  404,
                                            details: "INSERT ERRROR"
                                        });
                                    }
                                    else{
                                        bcrypt.hash(password, insertedResult.salt, (error, hashedPass) => {
                                            if(error){
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                           }
                                           else{
                                               if(insertedResult.password==hashedPass){
                                                    const token = token_gen({
                                                        'studentId':insertedResult.studentId,
                                                        'userType': 'STUDENT'
                                                    });
                                                    insertedResult.Status.authTocken = token;
                                                    insertedResult.totalLoan=insertedResult.loanDetails.length;
                                                    let minDue=0;
                                                    if(insertedResult.loanDetails.length!==0){
                                                        insertedResult.loanDetails.map(a=>{
                                                            minDue=minDue+a.balance;
                                                        })
                                                    }
                                                    insertedResult.minDue=minDue;
                                                    let datesArry=[];
                                                    insertedResult.loanDetails.map(a=>{
                                                        a.payments.map(b=>{
                                                            datesArry.push(b.date) 
                                                        })
                                                    });
                                                    
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: insertedResult
                                                    });
                                               }
                                               else{
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['101'],
                                                        statuscode: 101,
                                                        details: null
                                                    });
                                               }
                                           }
                                        })
                                    }
                                })
                            })
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['202'],
                            statuscode: 202,
                            details: null
                        }) 
                    }
                }  
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    signin: async(req, res, next)=>{
        try{
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - password",
                isError: true,
            });
            if (!req.body.EmailId || !req.body.password) { return; }
            let emailId=emailIdTakenFromUser.toLowerCase().trim();
            let aggreQuery = [{
                $match: { 'EmailId': emailId }
            }];
            await models.student.aggregate(aggreQuery).exec(async (err, item) => {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
               }
               else{
                   if(item.length==0){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['102'],
                            statuscode: 102,
                            details: null
                        });
                   }
                   else{
                        await bcrypt.hash(password, item[0].salt, async (error, hashedPass) => {
                            if(error){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                           }
                           else{
                               if(item[0].password==hashedPass){
                                    const token = token_gen({
                                        'studentId':item[0].studentId,
                                        'userType': 'STUDENT'
                                    });
                                    item[0].Status.authTocken = token;
                                    item[0].totalLoan=item[0].loanDetails.length;
                                    let minDue=0;
                                    item[0].sloanbalance = item[0].sloanbalance;
                                    item[0].minDue=minDue;
                                    item[0].isLoggedIn=true;
                                    let agquery = [
                                        { $match : { 
                                            "$and":[
                                                { "studentId" : item[0].studentId },{ "flag": false }
                                            ]} 
                                        },
                                        {
                                            $group:
                                            {
                                                _id: "$studentId",
                                                totalAmount: { $sum: "$roundup" }
                                            }
                                        }
                                    ];
                                    await models.roundups.aggregate(agquery).exec(async (errpay, itempay) => {
                                        if(errpay) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        } else {
                                            if(itempay.length > 0) {
                                                item[0].totalsaved = itempay[0]['totalAmount'].toFixed(2);
                                            } else {
                                                item[0].totalsaved = 0;
                                            }
                                            await models.student.updateOne({ 'EmailId': emailId }, { $set: { 'isLoggedIn': true }}, function (err, result){
                                                if(err) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['103'],
                                                        statuscode: 103,
                                                        details: err
                                                    });
                                                } else {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['201'],
                                                        statuscode: 200,
                                                        details: item[0]
                                                    });
                                                }
                                            })
                                        }
                                    });
                               }
                               else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['101'],
                                        statuscode: 101,
                                        details: null
                                    });
                               }
                           }
                        })
                   }
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   },
    getUserById: async (req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide studentId",
                isError: true
            });
            var query = [
                { $match : { "studentId" : studentId } },
                { "$project": {"fname":1, "lname": 1, "address": 1, "EmailId": 1, "mobileNo": 1, "studentId": 1, "stripe_customer_ID": 1, "stripe_subscription_ID": 1, "payitoff_borrow_ID": 1, "payitoff_borrow_UUID": 1}}
            ];
            await models.student.aggregate(query).exec( async function (error, item) {
               if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: error
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getUsers: async (req, res, next)=>{
        try{
            // let studentId = req.body.studentId ? req.body.studentId : res.json({
            //     message: "Please Provide studentId",
            //     isError: true
            // });
            // var query = [
            //     { $match : { "studentId" : studentId } },
            //     { "$project": {"fname":1, "lname": 1, "address": 1, "EmailId": 1, "mobileNo": 1}}
            // ];
            var query = [
                { "$project": {"fname":1, "lname": 1, "address": 1, "EmailId": 1, "mobileNo": 1, "studentId": 1, "stripe_customer_ID": 1, "stripe_subscription_ID": 1, "payitoff_borrow_ID": 1, "payitoff_borrow_UUID": 1 }}
            ];
            await models.student.aggregate(query).exec( async function (error, item) {
               if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: error
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    updateUserbyId: async (req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide studentId",
                isError: true
            });
            let stripe_customer_ID = req.body.stripe_customer_ID ? req.body.stripe_customer_ID : undefined;
            let stripe_subscription_ID = req.body.stripe_subscription_ID ? req.body.stripe_subscription_ID : undefined;
            let payitoff_borrow_ID = req.body.payitoff_borrow_ID  ? req.body.payitoff_borrow_ID  : undefined;
            let payitoff_borrow_UUID = req.body.payitoff_borrow_UUID ? req.body.payitoff_borrow_UUID : undefined;

            let data = {};

            if(stripe_customer_ID !== undefined) {
            	data.stripe_customer_ID = stripe_customer_ID;
            }

            if(stripe_subscription_ID !== undefined) {
            	data.stripe_subscription_ID = stripe_subscription_ID;
            }

            if(payitoff_borrow_ID !== undefined) {
            	data.payitoff_borrow_ID = payitoff_borrow_ID;
            }

            if(payitoff_borrow_UUID !== undefined) {
            	data.payitoff_borrow_UUID = payitoff_borrow_UUID;
            }

            console.log(data);

            await models.student.updateOne({"studentId": studentId},{ $set: data }, async function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: err
                    });
                }
                else
                {
                    if(item.nModified > 0) {
                    	const user = await models.student.find({"studentId": studentId});
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: user
                        });
                    } else {
                        res.json({
                            isError: false,
                            message: "Data Could not be added",
                            statuscode: 500,
                            details: item
                        });
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
}


function authcrypto(data){
    var private_key = 'f4d0028da26cc3eb4071a89b453419af113871e53812f0636c57e981f044a2d2';
    // Stringify the message. (The string that gets hashed should be
    // guaranteed to be the same as what is sent in the request.)
    // NOTE: if testing the example strings, you can just declare them as
    // strings, e.g. var message = 'Sila';
    var message = data

    // Generate the message hash using the Keccak 256 algorithm.
    var msg_hash = EthCrypto.hash.keccak256(message);

    // // Create a signature using your private key and the hashed message.
    var signature = EthCrypto.sign(private_key, msg_hash);

    // // The EthCrypto library adds a leading '0x' which should be removed 
    // // from the signature.
    signature = signature.substring(2);

    // The raw message should then be sent in an HTTP request body, and the signature
    // should be sent in a header.
    
    return data;
}

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}