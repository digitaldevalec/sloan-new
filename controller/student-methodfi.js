const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs'); 
const moment = require('moment'); 
const path = require('path');
const formidable = require('formidable');
const cron = require('node-cron');
const plaid = require('plaid');
const bodyParser = require('body-parser');
const winston = require('winston');
const http = require("https");
const Request = require("request");
// var expressWinston = require('express-winston');
// Logger configuration
const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: 'logs/method/method_controller.'+moment().format('YYYY_MM_DD')+'.log'
        })
    ]
};

const { Method, Environments } = require('method-node');

const methodclient = new Method({
  apiKey: config.app.methodfi_api,
  env: Environments.dev,
});


const createUser = async(req) => {
    const entity = await methodclient.entities.create({
      type: 'individual',
      individual: {
        first_name: req.first_name,
        last_name: req.last_name,
        phone: req.phone,
        email: req.email,
        dob: req.dob,
      },
      address: {
        line1: req.line1,
        line2: req.line2,
        city: req.city,
        state: req.state,
        zip: req.zip,
      },
    });
    if(entity.hasOwnProperty("id")) {
        console.log(entity);
        return entity.id;
    } else {
      console.log(entity);
    }
};

const linkAcc = async(req) => {
    const account = await methodclient.accounts.create({
      holder_id: req.account_holder_id,
      ach: {
        routing: req.routing,
        number: req.number,
        type: req.type,
      },
    });
    if(account.hasOwnProperty("id")) {
        return(account.id);
    }
};

const linkLoan = async(req) => {
    const account = await methodclient.accounts.create({
      holder_id: req.account_holder_id,
      liability: {
        mch_id: req.mch_id,
        account_number: req.account_number,
      },
    });
    console.log(req);
    console.log(account);
    if(account.hasOwnProperty("id")) {
        return(account.id);
    }
};

const loanPayment = async(req) => {
    const payment = await methodclient.payments.create({
      amount: req.amt,
      source: req.source,
      destination: req.destination,
      description: 'Loan Pmt',
    });
    if(payment.hasOwnProperty("id")) {
        return(payment.id);
    }
};

const getMerchantByIns = async(ins) => {
  const payment =  await methodclient.merchants.list({
    "provider_id.plaid": ins
  });

  console.log(payment);
  if(payment) {
      return(payment[0].mch_id);
  }
};



module.exports = {
  createUser,
  linkAcc,
  linkLoan,
  loanPayment,
  getMerchantByIns
};
