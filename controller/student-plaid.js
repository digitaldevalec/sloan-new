const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const plaid = require('plaid');
const fs = require('fs'); 
const moment = require('moment'); 
const bodyParser = require('body-parser');
const winston = require('winston');
var WebHooks = require('node-webhooks');
// var expressWinston = require('express-winston');
// Logger configuration
const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: 'logs/plaid/plaid_controller.'+moment().format('YYYY_MM_DD')+'.log'
        })
    ]
};
const { linkAcc, linkLoan, getMerchantByIns } = require('./student-methodfi');
const logger_main = winston.createLogger(logConfiguration);
const plaidClientSandbox = new plaid.Client({
  clientID: '5d1fc9f84fa1190016b491a3',
  secret: '465fe994f2595d372976e04d544ec5',
  env: plaid.environments.sandbox
});
//LIVE PLAID
const plaidClient = new plaid.Client({
  clientID: config.app.PLAID_CLIENT_ID,
  secret: config.app.PLAID_SECRET,
  env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
var http = require("https");
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');


module.exports = {
    savePlaidData: async(req, res, next)=>{
        try{
            let account_num = req.body.account_num;
            let account_routing = req.body.account_routing;
            let account_name = req.body.account_name;
            let account_type = req.body.account_type;
            let account_class = req.body.account_class;
            let studentId = req.body.studentId;
            let actoken = req.body.access_token;
            var request = require("request");

            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    var userDataforMethod = {
                        "account_holder_id": item.mfi.userId,
                        "routing": account_routing,
                        "number": account_num,
                        "type": 'checking'
                    };
        
                    var mflinkbank = await linkAcc(userDataforMethod);

                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    if(bankobj == undefined) {
                        var banks = item.bankAccounts;
                        let obj = banks.find(o => o.accountNum === account_num);
                        if(obj == undefined) {
                            //console.log(wpk);
                            // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                            const response = await Sila.linkAccountDirect(
                              userHandle,
                              wpk,
                              account_num,
                              account_routing,
                              account_num,
                              account_class
                            )
                            console.log("Response for SILA", response);
                            if(response.statusCode == 200) {
                                var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'mfi_token':mflinkbank, 'roundup': 'false'  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                if(updatebankdata) {
                                    res.json({
                                        isError: false,
                                        message: "Bank Account Added successfully",
                                        statuscode: 200,
                                        details: bankdetails
                                    });
                                }
                            }
                            else
                            {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['500'],
                                    statuscode: 500,
                                    details: 'Bank Add Failed. Problem Linking Bank to SILA',
                                    resp: response
                                });
                            }
                        } else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['403'],
                                statuscode: 403,
                                details: 'Bank Already Exists'
                            });
                        }
                    } else {
                        var banks = item.bankAccounts;
                        let obj = banks.find(o => o.accountNum === account_num);
                        if(obj == undefined) {
                            var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                            var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                            //var achaccountdetails = newbody.nodes[0].info;
                            //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                            //if(updatedata) {
                            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                            if(updatebankdata) {
                                res.json({
                                    isError: false,
                                    message: "Bank Account Added successfully!",
                                    statuscode: 200,
                                    details: bankdetails
                                });
                            }
                        } else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['403'],
                                statuscode: 403,
                                details: 'Bank Already Exists '
                            });
                        }
                    }
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    isRoundUpsOn:(req, res, next)=>{
        try{
            let accountNum = req.body.accountNum;
            let accountRouting = req.body.accountRouting;
            let bankName = req.body.bankName;
            let bankDispName = req.body.bankDispName;
            let accountType = req.body.accountType;
            let accountClass = req.body.accountClass;
            let syn = req.body.account_class;
            let studentId = req.body.studentId;
            let reference = req.body.reference;
            let access_token = req.body.access_token;

            var data = {
                "reference" : reference,
                "bankName" : bankName,
                "bankDispName" : bankDispName,
                "accountNum" : accountNum,
                "accountRouting" : accountRouting,
                "accountType" : accountType,
                "accountClass" : accountClass,
                "access_token" : access_token,
                "roundup" : true
            };
          
            // let status = req.body.status ? req.body.status : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Notification Status",
            //     isError: true,
            // });
            // let bankacc = req.body.bankacc ? req.body.bankacc : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Bank Account",
            //     isError: true,
            // });
            if ( !req.body.studentId) { return; }
            // let notificationStatus;
            // if(status == "true"){
            //     notificationStatus=true;
            // }
            // else{
            //     notificationStatus=false;
            // }
            // studentId = req.body.studentId;
            models.student.updateOne({ 'studentId': studentId }, {$set:{ 'roundUps': data }}, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else{
                    models.student.findOne({ 'studentId': studentId }, function (error, item) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: "Round Ups Is Turned On",
                                statusCode: 200,
                                details: item
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    isRoundUpsOff:(req, res, next)=>{
        try{
            let studentId = req.body.studentId;
          
            // let status = req.body.status ? req.body.status : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Notification Status",
            //     isError: true,
            // });
            // let bankacc = req.body.bankacc ? req.body.bankacc : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Bank Account",
            //     isError: true,
            // });
            if ( !req.body.studentId) { return; }
            // let notificationStatus;
            // if(status == "true"){
            //     notificationStatus=true;
            // }
            // else{
            //     notificationStatus=false;
            // }
            // studentId = req.body.studentId;
            models.student.updateOne({ 'studentId': studentId }, {$pull:{ 'roundUps': {'roundup':true} }}, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else{
                    models.student.findOne({ 'studentId': studentId }, function (error, item) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: "Round Ups Is Turned Off",
                                statusCode: 200,
                                details: null
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getInstitutions: async(req, res, next)=>{
        try {
            var count = 50;
            var offset = 0;
            // Pull institutions
            plaidClient.getInstitutions(count, offset, (err, result) => {
                // Handle err
                //const institutions = result.institutions;
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: result
                });
            });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });

        }
    },
    getFirstInstitutions: async(req, res, next)=>{
        try{
            var request = require('request');
            var count = req.body.count ? req.body.count : 200;
            var options = {
              'method': 'POST',
              'url': 'https://sandbox.plaid.com/institutions/get',
              'headers': {
                'Content-Type': 'application/json',
                'User-Agent': 'Plaid Postman'
              },
              body: JSON.stringify({"client_id":"5d1fc9f84fa1190016b491a3","secret":"465fe994f2595d372976e04d544ec5","count":count,"offset":0})

            };

            request(options, function (err, resp) { 
                if (err) {
                    console.log("ERROR",resp);
                }
                else {
                    var receiveddata = JSON.parse(resp.body);
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        data: receiveddata
                    });
                }
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    saveLoanData: async(req, res, next)=>{
        try{
            var request = require('request');
            let loandata = req.body.loandata;
            let studentId = req.body.studentId;
            console.log("LOAN DATA", loandata);
            await models.student.updateOne({ 'studentId': studentId }, { $push: {"loanDetails":loandata }}, async function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: "Error",
                        statuscode: 405,
                        details: null
                    });
                    console.log(updatederror);
                }
                else{
                    if(updatedItem.nModified == 1) {
                        res.json({
                            isError: false,
                            message: "Updated Loan Table",
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['405'],
                            statuscode: 405,
                            details: null
                        }); 
                    }
                    console.log(updatedItem);
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    saveLoanDataNew: async(req, res, next)=>{
        try{
            var request = require('request');
            let loandata = req.body.loandata;
            let studentId = req.body.studentId;
            
            var getdatanew = await models.student.findOne({'studentId': studentId});
            var institutions = await models.institute.find({});
            // var check = institutions.find(o => o.ins_id === loandata[0].item.institution_id);
            var check = 'true';

            if(check != undefined) {

                for(var i = 0; i < loandata.length; i++) {

                    var ins_id = loandata[i].item.institution_id;

                    const merchant = await getMerchantByIns(ins_id);

                    console.log("MERCHANT ID",merchant);

                    var userDataforMethod = {
                        "account_holder_id": getdatanew.mfi.userId,
                        "mch_id": merchant,
                        "account_number": loandata[i].account_number
                    };

                    var getLoanLinkMF = await linkLoan(userDataforMethod);

                    console.log(getLoanLinkMF);
                    loandata[i].destination = getLoanLinkMF;
                }

                const updatedUser = await models.student.findOneAndUpdate({ 'studentId': studentId }, { $push: {"loanDetails":loandata }});
                console.log(updatedUser);
                if(updatedUser) {
                    res.json({
                        isError: false,
                        message: "Loan Added",
                        statuscode: 200,
                        details: loandata
                    })
                }
                else{
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    }); 
                }
                res.json({
                    isError: false,
                    message: "Loan Institution Supported",
                    statuscode: 200,
                    details: check
                })
            } else {
                res.json({
                    isError: true,
                    message: "Loan Institution Not Supported",
                    statuscode: 200,
                    details: check
                })
            }
            // res.json({
            //     isError: false,
            //     message: 'successfully',
            //     statuscode: 200,
            //     loancheck: ( check != undefined ) ? true : false,
            //     new: loandata[0].item.institution_id
            // });
            
        }
        // db.getCollection('students').aggregate([
        //         {"$unwind" : "$loanDetails" },
        //         {"$match": {"loanDetails": { "$gt": {} }, "studentId":"ARINDAM" } },
        //         { "$project": {"studentId":1, "loanDetails": 1}}
        //     ])
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    checkLoan: async(req, res, next)=>{
        try{
            var request = require('request');
            let loandata = req.body.loandata;
            let studentId = req.body.studentId;
            
            // const updatedUser = await models.student.findOneAndUpdate({ 'studentId': studentId }, { $push: {"loanDetails":loandata }});
            // console.log(updatedUser);
            // if(updatedUser) {
            //     res.json({
            //         isError: false,
            //         message: "Loan Added",
            //         statuscode: 200,
            //         details: loandata
            //     })
            // }
            // else{
            //     res.json({
            //         isError: true,
            //         message: errorMsgJSON['ResponseMsg']['405'],
            //         statuscode: 405,
            //         details: null
            //     }); 
            // }
            var institutions = await models.institute.find({});
            var check = institutions.find(o => o.ins_id === loandata.item.institution_id);

            if(Object.keys(check).length > 0) {
                res.json({
                    isError: false,
                    message: "Loan Institution Not Supported",
                    statuscode: 200,
                    details: check
                })
            } else {
                res.json({
                    isError: false,
                    message: "Loan Added",
                    statuscode: 200,
                    details: check
                })
            }
            
        }
        // db.getCollection('students').aggregate([
        //         {"$unwind" : "$loanDetails" },
        //         {"$match": {"loanDetails": { "$gt": {} }, "studentId":"ARINDAM" } },
        //         { "$project": {"studentId":1, "loanDetails": 1}}
        //     ])
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    plaidWebhook: async(req, res, next)=>{
        try{
            const event = req.body;
            logger_main.info(JSON.stringify('=======================PRODUCTION=======================' + JSON.stringify(req.body)));
            if (Object.keys(event).length > 0) {
                if(event.message.webhook_code == "AUTOMATICALLY_VERIFIED") {
                    console.log("AUTO VERIFIED");
                    // const accessToken = await lookupAccessToken(event.message.item_id);
                    // Non Arrow (standard way)
                    const itemId = event.message.item_id ? event.message.item_id : '';
                    const accountID = event.message.account_id ? event.message.account_id : '';
                    // console.log(accountID + ' > ' + itemId);
                    // var accessToken; 
                    let query = [{ "$unwind": "$bankAccounts" }, { "$match" : { "bankAccounts.item_id" : itemId } }, { "$project": { "studentId":1, "bankAccounts":1 } }];
                    await models.student.aggregate(query).exec(async function (error123, item123) {
                        if (error123) {
                            console.log(error123)
                        }
                        else 
                        {
                            var accessToken = item123[0].bankAccounts.access_token;
                            logger_main.info("accessToken > " + JSON.stringify(accessToken));
                            const authResponse = await plaidClient.getAuth(accessToken);
                            const numbers = authResponse.numbers;                      
                            logger_main.info("authResponse > " + JSON.stringify(numbers));
                            const getaccounts = await plaidClient.getAccounts(accessToken).catch((err) => {
                                if(err) {
                                    console.log(err);
                                }
                            });
                            const account = getaccounts.accounts.find((a) => a.account_id === accountID);
                            const verificationStatus = account.verification_status;

                            logger_main.info(JSON.stringify(item123));
                            logger_main.info("Account Details > ", JSON.stringify(numbers));
                            logger_main.info("Account Details > ", JSON.stringify(account));
                            logger_main.info("Verification Status > ", JSON.stringify(verificationStatus));


                            //SAVE BANK
                            let account_num = numbers.ach[0].account;
                            let account_routing = numbers.ach[0].routing;
                            let account_name = item123[0].bankAccounts.bankDispName;
                            let account_type = account.type;
                            let account_class = account.subtype;
                            let studentId = item123[0].studentId;
                            let actoken = accessToken;
                            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: error
                                    });
                                }
                                else {
                                    const userHandle = studentId+'.silamoney.eth';
                                    var wpk = await decryptKeys(item.walletKey);
                                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                                    // console.log(getallbanks);
                                    const allbanks = getallbanks.data;
                                    bankobj = allbanks.find(o => o.account_name === account_num);
                                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                                    // if(deletednewbankdata)
                                    // {
                                        if(bankobj == undefined) {
                                            var banks = item.bankAccounts;
                                            let obj = banks.find(o => o.accountNum === account_num);
                                            console.log(obj);
                                            if(obj == undefined) {
                                                console.log(obj);
                                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                                const response = await Sila.linkAccountDirect(
                                                  userHandle,
                                                  wpk,
                                                  account_num,
                                                  account_routing,
                                                  account_num,
                                                  account_class
                                                )
                                                console.log(response);
                                                if(response.statusCode == 200) {
                                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                                    if(updatebankdata) {
                                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                                        if(deletednewbankdata)
                                                        {
                                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: bankNewData.bankAccounts,

                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: "Bank Add Failed. Problem Linking Bank to SILA"
                                                    });
                                                }
                                            } else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: "Bank Exists"
                                                });
                                            }
                                        } else {
                                            var banks = item.bankAccounts;
                                            let obj = banks.find(o => o.accountNum === account_num);
                                            if(obj == undefined) {
                                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                                //var achaccountdetails = newbody.nodes[0].info;
                                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                                //if(updatedata) {
                                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                                    if(updatebankdata) {
                                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                                        if(deletednewbankdata)
                                                        {
                                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: bankNewData.bankAccounts,

                                                            });
                                                        }
                                                    }
                                            } else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: "Bank Exists"
                                                });
                                            }
                                        }
                                    // }
                                }
                            })
                            //END SAVE BANK
                        }
                    });
                    console.log(accessToken);
                } else if(event.message.webhook_code == "VERIFICATION_EXPIRED") {
                    // handle verification failure; prompt user to re-authenticate
                    console.error('Verification failed for', event.message.item_id);
                } else {
                    // Unexpected event type
                    return response.status(200).end();
                }
            } else {
                logger_main.info("Webhook Is not for microdeposit > " + JSON.stringify(event));
            }
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    plaidWebhookSandbox: async(req, res, next)=>{
        try{
            const event = req.body;
            logger_main.info(JSON.stringify('=======================SANBOX=======================' + JSON.stringify(req.body)));
            if (Object.keys(event).length > 0) {
                if(event.message.webhook_code == "AUTOMATICALLY_VERIFIED") {
                    console.log("AUTO VERIFIED");
                    // const accessToken = await lookupAccessToken(event.message.item_id);
                    // Non Arrow (standard way)
                    const itemId = event.message.item_id ? event.message.item_id : '';
                    const accountID = event.message.account_id ? event.message.account_id : '';
                    // console.log(accountID + ' > ' + itemId);
                    // var accessToken; 
                    let query = [{ "$unwind": "$bankAccounts" }, { "$match" : { "bankAccounts.item_id" : itemId } }, { "$project": { "studentId":1, "bankAccounts":1 } }];
                    await models.student.aggregate(query).exec(async function (error123, item123) {
                        if (error123) {
                            console.log(error123)
                        }
                        else 
                        {
                            var accessToken = item123[0].bankAccounts.access_token;
                            logger_main.info("accessToken > " + JSON.stringify(accessToken));
                            const authResponse = await plaidClientSandbox.getAuth(accessToken);
                            const numbers = authResponse.numbers;                      
                            logger_main.info("authResponse > " + JSON.stringify(numbers));
                            const getaccounts = await plaidClientSandbox.getAccounts(accessToken).catch((err) => {
                                if(err) {
                                    console.log(err);
                                }
                            });
                            const account = getaccounts.accounts.find((a) => a.account_id === accountID);
                            const verificationStatus = account.verification_status;

                            console.log(item123);
                            console.log("Account Details > ", numbers);
                            console.log("Account Details > ", account);
                            console.log("Verification Status > ", verificationStatus);


                            //SAVE BANK
                            let account_num = numbers.ach[0].account;
                            let account_routing = numbers.ach[0].routing;
                            let account_name = item123[0].bankAccounts.bankDispName;
                            let account_type = account.type;
                            let account_class = account.subtype;
                            let studentId = item123[0].studentId;
                            let actoken = accessToken;
                            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: error
                                    });
                                }
                                else {
                                    const userHandle = studentId+'.silamoney.eth';
                                    var wpk = await decryptKeys(item.walletKey);
                                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                                    // console.log(getallbanks);
                                    const allbanks = getallbanks.data;
                                    bankobj = allbanks.find(o => o.account_name === account_num);
                                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                                    // if(deletednewbankdata)
                                    // {
                                        if(bankobj == undefined) {
                                            var banks = item.bankAccounts;
                                            let obj = banks.find(o => o.accountNum === account_num);
                                            console.log(obj);
                                            if(obj == undefined) {
                                                console.log(obj);
                                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                                const response = await Sila.linkAccountDirect(
                                                  userHandle,
                                                  wpk,
                                                  account_num,
                                                  account_routing,
                                                  account_num,
                                                  account_class
                                                )
                                                console.log(response);
                                                if(response.statusCode == 200) {
                                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                                    if(updatebankdata) {
                                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                                        if(deletednewbankdata)
                                                        {
                                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: bankNewData.bankAccounts,

                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: "Bank Add Failed. Problem Linking Bank to SILA"
                                                    });
                                                }
                                            } else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: "Bank Exists"
                                                });
                                            }
                                        } else {
                                            var banks = item.bankAccounts;
                                            let obj = banks.find(o => o.accountNum === account_num);
                                            if(obj == undefined) {
                                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                                //var achaccountdetails = newbody.nodes[0].info;
                                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                                //if(updatedata) {
                                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                                    if(updatebankdata) {
                                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                                        if(deletednewbankdata)
                                                        {
                                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: bankNewData.bankAccounts,

                                                            });
                                                        }
                                                    }
                                            } else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: "Bank Exists"
                                                });
                                            }
                                        }
                                    // }
                                }
                            })
                            //END SAVE BANK
                        }
                    });
                    console.log(accessToken);
                } else if(event.message.webhook_code == "VERIFICATION_EXPIRED") {
                    // handle verification failure; prompt user to re-authenticate
                    console.error('Verification failed for', event.message.item_id);
                } else {
                    // Unexpected event type
                    return response.status(200).end();
                }
            } else {
                logger_main.info("Webhook Is not for microdeposit > " + JSON.stringify(event));
            }
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    verifyMicro: async(req, res, next)=>{
        try {
            logger_main.info("==============================================VERIFY SAME DAY MICRO DEPOSIT==============================================");
            var accessToken = req.body.access_token;
            logger_main.info("accessToken > " + JSON.stringify(accessToken));
            const authResponse = await plaidClient.getAuth(accessToken);
            const numbers = authResponse.numbers;                      
            logger_main.info("authResponse > " + JSON.stringify(numbers));

            logger_main.info("Account Details Numbers > ", JSON.stringify(numbers));
            logger_main.info("Verification Status > ", JSON.stringify(req.body.verification_status));

            //SAVE BANK
            let account_num = numbers.ach[0].account;
            let account_routing = numbers.ach[0].routing;
            let account_name = req.body.bankDispName;
            let account_type = "PERSONAL";
            let account_class = "CHECKING";
            let studentId = req.body.studentId;
            let actoken = accessToken;
            let itemId = req.body.item_id;
            let requestId = req.body.request_id;

            var data = {
                account_num, account_routing, account_name, account_type, account_class, studentId, actoken
            }
            console.log(data);
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    // console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                    // if(deletednewbankdata)
                    // {
                        if(bankobj == undefined) {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            console.log(obj);
                            if(obj == undefined) {
                                console.log(obj);
                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                const response = await Sila.linkAccountDirect(
                                  userHandle,
                                  wpk,
                                  account_num,
                                  account_routing,
                                  account_num,
                                  account_class
                                )
                                console.log(response);
                                if(response.statusCode == 200) {
                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        logger_main.info("Update Bank > ", JSON.stringify(updatebankdata));
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: "Bank Add Failed. Problem Linking Bank to SILA"
                                    });
                                }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        } else {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            if(obj == undefined) {
                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        logger_main.info("Update Bank Else Part> ", JSON.stringify(updatebankdata));
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        }
                    // }
                }
            })
            // END SAVE BANK
            // res.json({
            //     isError: false,
            //     message: errorMsgJSON['ResponseMsg']['200'],
            //     statuscode: 200,
            //     details: {
            //         numbers: numbers,
            //         authResponse: authResponse,
            //         verificationStatus: verificationStatus
            //     },

            // });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    verifyAutoMicro: async(req, res, next)=>{
        try {
            logger_main.info("==============================================VERIFY AUTOMATED MICRO DEPOSIT==============================================");
            var accessToken = req.body.access_token;
            logger_main.info("accessToken > " + JSON.stringify(accessToken));
            const authResponse = await plaidClient.getAuth(accessToken);
            const numbers = authResponse.numbers;                      
            logger_main.info("authResponse > " + JSON.stringify(numbers));

            logger_main.info("Account Details Numbers > ", JSON.stringify(numbers));
            logger_main.info("Verification Status > ", JSON.stringify(req.body.verification_status));
            const getaccounts = await plaidClient.getAccounts(accessToken).catch((err) => {
                if(err) {
                    console.log(err);
                }
            });

            const account = getaccounts.accounts.find((a) => a.account_id === numbers.ach[0].account_id);
            const verificationStatus = req.body.verification_status;
            logger_main.info("Account Details NUMBERS > ", numbers);
            logger_main.info("Account Details > ", account);

            //SAVE BANK
            let account_num = numbers.ach[0].account;
            let account_routing = numbers.ach[0].routing;
            let account_name = req.body.bankDispName;
            let account_type = account.type;
            let account_class = account.subtype;
            let studentId = req.body.studentId;
            let actoken = accessToken;
            let itemId = req.body.item_id;
            let requestId = req.body.request_id;

            var data = {
                account_num, account_routing, account_name, account_type, account_class, studentId, actoken
            }
            console.log(data);
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    // console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                    // if(deletednewbankdata)
                    // {
                        logger_main.info("BANK FOUND > ", bankobj);
                        if(bankobj == undefined) {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            console.log(obj);
                            if(obj == undefined) {
                                console.log(obj);
                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                const response = await Sila.linkAccountDirect(
                                  userHandle,
                                  wpk,
                                  account_num,
                                  account_routing,
                                  account_num,
                                  account_class
                                )
                                console.log(response);
                                if(response.statusCode == 200) {
                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        logger_main.info("UPDATED DB > ", updatebankdata);
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: "Bank Add Failed. Problem Linking Bank to SILA"
                                    });
                                }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        } else {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            if(obj == undefined) {
                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        logger_main.info("UPDATED DB ELSE PART > ", updatebankdata);
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        }
                    // }
                }
            })
            // END SAVE BANK
            // res.json({
            //     isError: false,
            //     message: errorMsgJSON['ResponseMsg']['200'],
            //     statuscode: 200,
            //     details: {
            //         numbers: numbers,
            //         authResponse: authResponse,
            //         verificationStatus: verificationStatus
            //     },

            // });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    verifyMicroSandbox: async(req, res, next)=>{
        try {
            var accessToken = req.body.access_token;
            console.log("accessToken > " + JSON.stringify(accessToken));
            const authResponse = await plaidClientSandbox.getAuth(accessToken);
            const numbers = authResponse.numbers;                      
            console.log("authResponse > " + JSON.stringify(numbers));

            console.log("Account Details Numbers > ", JSON.stringify(numbers));
            console.log("Verification Status > ", JSON.stringify(req.body.verification_status));

            //SAVE BANK
            let account_num = numbers.ach[0].account;
            let account_routing = numbers.ach[0].routing;
            let account_name = req.body.bankDispName;
            let account_type = req.body.account_type;
            let account_class = req.body.account_class;
            let studentId = req.body.studentId;
            let actoken = accessToken;
            let itemId = req.body.item_id;
            let requestId = req.body.request_id;

            var data = {
                account_num, account_routing, account_name, account_type, account_class, studentId, actoken
            }
            console.log(data);
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    // console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                    // if(deletednewbankdata)
                    // {
                        if(bankobj == undefined) {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            console.log(obj);
                            if(obj == undefined) {
                                console.log(obj);
                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                const response = await Sila.linkAccountDirect(
                                  userHandle,
                                  wpk,
                                  account_num,
                                  account_routing,
                                  account_num,
                                  account_class
                                )
                                console.log(response);
                                if(response.statusCode == 200) {
                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: bankNewData.bankAccounts
                                    });
                                }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: bankNewData.bankAccounts
                                });
                            }
                        } else {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            if(obj == undefined) {
                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                console.log("ELSE PART >>>>>>>>>>> ", bankdetails);
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                            } else {                                
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        }
                    // }
                }
            })
            // END SAVE BANK
            // res.json({
            //     isError: false,
            //     message: errorMsgJSON['ResponseMsg']['200'],
            //     statuscode: 200,
            //     details: {
            //         numbers: numbers,
            //         authResponse: authResponse,
            //         verificationStatus: verificationStatus
            //     },

            // });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    verifyAutoMicroSandbox: async(req, res, next)=>{
        try {
            var accessToken = req.body.access_token;
            console.log("accessToken > " + JSON.stringify(accessToken));
            const authResponse = await plaidClientSandbox.getAuth(accessToken);
            const numbers = authResponse.numbers;                      
            console.log("authResponse > " + JSON.stringify(numbers));

            console.log("Account Details Numbers > ", JSON.stringify(numbers));
            console.log("Verification Status > ", JSON.stringify(req.body.verification_status));
            const getaccounts = await plaidClientSandbox.getAccounts(accessToken).catch((err) => {
                if(err) {
                    console.log(err);
                }
            });

            const account = getaccounts.accounts.find((a) => a.account_id === numbers.ach[0].account_id);
            const verificationStatus = req.body.verification_status;
            console.log("Account Details > ", numbers);
            console.log("Account Details > ", account);

            //SAVE BANK
            let account_num = numbers.ach[0].account;
            let account_routing = numbers.ach[0].routing;
            let account_name = req.body.bankDispName;
            let account_type = account.type;
            let account_class = account.subtype;
            let studentId = req.body.studentId;
            let actoken = accessToken;
            let itemId = req.body.item_id;
            let requestId = req.body.request_id;

            var data = {
                account_num, account_routing, account_name, account_type, account_class, studentId, actoken
            }
            console.log(data);
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    // console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    // var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "item_id": itemId }}});
                    // if(deletednewbankdata)
                    // {
                        if(bankobj == undefined) {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            console.log(obj);
                            if(obj == undefined) {
                                console.log(obj);
                                // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                                const response = await Sila.linkAccountDirect(
                                  userHandle,
                                  wpk,
                                  account_num,
                                  account_routing,
                                  account_num,
                                  account_class
                                )
                                console.log(response);
                                if(response.statusCode == 200) {
                                    var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                    var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false', 'item_id': itemId  }
                                    var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: "Bank Add Failed. Problem Linking Bank to SILA"
                                    });
                                }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        } else {
                            var banks = item.bankAccounts;
                            let obj = banks.find(o => o.accountNum === account_num);
                            if(obj == undefined) {
                                var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                                var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                    if(updatebankdata) {
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "request_id": requestId }}});
                                        if(deletednewbankdata)
                                        {
                                            const bankNewData = await models.student.findOne({ 'studentId': studentId });
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: bankNewData.bankAccounts,

                                            });
                                        }
                                    }
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: "Bank Exists"
                                });
                            }
                        }
                    // }
                }
            })
            // END SAVE BANK
            // res.json({
            //     isError: false,
            //     message: errorMsgJSON['ResponseMsg']['200'],
            //     statuscode: 200,
            //     details: {
            //         numbers: numbers,
            //         authResponse: authResponse,
            //         verificationStatus: verificationStatus
            //     },

            // });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    getBalance: async(req, res, next) => {
        let account_num = req.body.account_num;
        let studentId = req.body.studentId;
        let actoken = req.body.access_token;
        let blnc = await checkAccountBalance(actoken, account_num);
        res.json({
            isError: false,
            message: errorMsgJSON['ResponseMsg']['200'],
            statuscode: 200,
            balance: blnc
        });
    }
}

async function lookupAccessToken(id) {
    var resp = '';
    var query = [{ "$unwind": "$bankAccounts" }, { "$match" : { "bankAccounts.item_id" : id } }, { "$project": { "studentId":1, "bankAccounts":1 } }];
    await models.student.aggregate(query).exec(async function (error, item) {
        if (error) {
            console.log(error)
        }
        else 
        {
            resp = item[0].bankAccounts.access_token;
        }
    });
    return resp;
}

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    //console.log(data);
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}


// CHECK IF EMPTY OBJECT
async function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


async function checkAccountBalance(actoken, account_num) {
    const account_mask = account_num.slice(account_num.length - 4);
    console.log("====GET MASK====", account_mask);
    let getBalance = await plaidClient.getBalance(actoken);
    console.log(getBalance);
    let obj = getBalance.accounts.find(o => o.mask === account_mask);
    return obj.balances.available;
}