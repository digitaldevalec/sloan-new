const mongoose = require('mongoose');
function toLower (v) {
    return v.toLowerCase();
}
var recurringSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    studentId : String,
    amount: {type: Number,default:""},
    bankId : { type: String, default: ""},
    bankName : { type: String, default: ""},
    accountNum : { type: String, default: ""},
    accountRouting : { type: String, default: ""},
    accountType : { type: String, default: ""},
    accountClass : { type: String, default: ""},
    loanId:{type:String,default:""},
    loanname:{type:String,default:""},
    loanAccount:{type:String,default:""},
    frequency:{type: String,default:""},
    next_payment_date:{type: String,default:""},
    paymentId : String,
    loan_ins: {type: String,default:""},
    access_token: {type: String,default:""}
},
{
    timestamps: true 
})


const recurring = mongoose.model("recurring", recurringSchema, "recurringSetups")

module.exports = recurring;