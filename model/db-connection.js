
const mongoose = require('mongoose');

const config =  require('../configaration/environment');
const path = require("path");
const tunnel = require('tunnel-ssh');

var configdata = {
    username : 'ubuntu',
    host: '54.176.67.107:3000',
    privateKey:require('fs').readFileSync(path.resolve(__dirname, "../sloanapp.pem")),
    port:22,
    dstPort:27017,
    localPort: 27010
};

var server = tunnel(configdata, function (error, server) {
  if(error){
      console.log("SSH connection error: " + error);
  }

  mongoose.connect(config.app.dbURI,{
    useNewUrlParser:true,
    useCreateIndex: true
  });

  mongoose.connection.on('connected', function () {  
    console.log(' Mongoose default connection open to ' +  config.app.dbURI);
  }); 

    // If the connection throws an error
  mongoose.connection.on('error',function (err) {  
    console.log('Mongoose default connection error: ' + err);
  });
    // When the connection is disconnected
  mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose default connection disconnected'); 
  });

    // If the Node process ends, close the Mongoose connection 
  process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0); 
    }); 
  });
});


mongoose.connect(config.app.dbURI,{
    useNewUrlParser:true,
    useCreateIndex: true
});
mongoose.connection.on('connected', function () {  
  console.log(' Mongoose default connection open to ' +  config.app.dbURI);
}); 

  // If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
});
  // When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

  // If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
  mongoose.connection.close(function () { 
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
  }); 
});
   

