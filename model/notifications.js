const mongoose = require('mongoose');
var notificationSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    "studentId" : String,
    "action" : {type:String, default:""},
    "actionDesc" : {type:String, default:""},
    "amount" : {type:Number, default: 0.00},
    "isRead" : {type:Boolean, default:false},
    "type" : {type:String, default:""}
},
{
    timestamps: true 
})

const notifications = mongoose.model("notifications", notificationSchema)

module.exports = notifications;