const mongoose = require('mongoose');
const instituteSchema = new mongoose.Schema({
            _id : mongoose.Schema.Types.ObjectId,
            "rpps_biller_id": String,
            "biller_type": String,
            "biller_class": String,
            "name":String,
            "country": String,
            "state": String,
            "territory_code": String,
            "currency": String,
            "ins_id": String
},
{
    timestamps: true 
})
const instituteContainer = mongoose.model("institute", instituteSchema)
module.exports = instituteContainer