const studentMainCtrl = require('../controller/student-main');
const studentSilaCtrl = require('../controller/student-sila');
const studentGCCtrl = require('../controller/student-giftcard');
const express =  require('express');
const studentMainRoutes =  express.Router();
const checkAuth = require('../middleware/check-auth');
/**************************************************************
 * Student Main Related routes
***************************************************************/
studentMainRoutes.post('/getbanks',studentMainCtrl.getBanks);
studentMainRoutes.post('/addfunds',studentMainCtrl.addfunds);
studentMainRoutes.post('/deletebank',studentMainCtrl.deleteBank);
studentMainRoutes.post('/recommendedlist',studentMainCtrl.recommendedList);
studentMainRoutes.post('/editprofile',studentMainCtrl.editProfile);
studentMainRoutes.post('/addactivity',studentMainCtrl.addActivity);
studentMainRoutes.post('/deleteactivity',studentMainCtrl.deleteActivity);
studentMainRoutes.post('/uploadphoto',studentMainCtrl.uploadPhoto);
studentMainRoutes.post('/getdevicedata',studentMainCtrl.getDeviceData);
studentMainRoutes.post('/keywordsearch',studentMainCtrl.keywordSearch);
studentMainRoutes.post('/requestmoney',studentMainCtrl.requestMoney);
studentMainRoutes.post('/notificationlist',studentMainCtrl.notificationList);
studentMainRoutes.post('/getstudentdata',studentMainCtrl.getStudentData);
studentMainRoutes.post('/notificationchange',studentMainCtrl.notificationChangeState);
studentMainRoutes.post('/paymenthistory',studentMainCtrl.paymentHistory);
studentMainRoutes.post('/userTransactions',studentMainCtrl.userAllTransactions);
studentMainRoutes.post('/userAllTransactions',studentMainCtrl.userAllTransactions);
studentMainRoutes.post('/checkManualTransactions',studentMainCtrl.checkManualTransactions);
studentMainRoutes.post('/banktransaction',studentMainCtrl.banktransaction);
studentMainRoutes.post('/getAutoPayments',studentMainCtrl.getAutoPayments);
studentMainRoutes.post('/addAutoPayment',studentMainCtrl.addAutoPayment);
studentMainRoutes.post('/deleteAutoPayment',studentMainCtrl.deleteAutoPayment);
studentMainRoutes.post('/editAutoPayment',studentMainCtrl.editAutoPayment);
studentMainRoutes.post('/deleteLoan',studentMainCtrl.deleteLoan);
studentMainRoutes.post('/wallettransaction',studentMainCtrl.wallettransaction);
studentMainRoutes.post('/getplaiddata',studentMainCtrl.getPlaidData);
studentMainRoutes.post('/getplaiddatatest',studentMainCtrl.getPlaidDataTEST);

studentMainRoutes.post('/cancelTransaction',studentMainCtrl.cancelTransaction);
studentMainRoutes.post('/silaTransactions',studentMainCtrl.silaTransactions);

studentMainRoutes.post('/createLinkToken',studentMainCtrl.createLinkToken);
studentMainRoutes.post('/createLinkTokenSandbox',studentMainCtrl.createLinkTokenSandbox);
studentMainRoutes.post('/createLinkTokenBank',studentMainCtrl.createLinkTokenBank);
studentMainRoutes.post('/createLinkTokenBankVerify',studentMainCtrl.createLinkTokenBankVerify);
studentMainRoutes.post('/createLinkTokenBankVerifySandbox',studentMainCtrl.createLinkTokenBankVerifySandbox);
studentMainRoutes.post('/createLinkTokenBankSandbox',studentMainCtrl.createLinkTokenBankSandbox);
studentMainRoutes.post('/createLinkTokenMicro',studentMainCtrl.createLinkTokenMicro);
studentMainRoutes.post('/createMicroDepositToken',studentMainCtrl.createMicroDepositToken);
studentMainRoutes.post('/createMicroDepositTokenSandbox',studentMainCtrl.createMicroDepositTokenSandbox);
studentMainRoutes.post('/createAutoMicroDepositToken',studentMainCtrl.createAutoMicroDepositToken);
studentMainRoutes.post('/createAutoMicroDepositTokenSandbox',studentMainCtrl.createAutoMicroDepositTokenSandbox);
studentMainRoutes.post('/getLoans',studentMainCtrl.getLiabilities);
studentMainRoutes.post('/webhook_method',studentMainCtrl.webHookMethod);


studentMainRoutes.post('/getItems',studentMainCtrl.getItemDetails);

//Sila Endpoints
studentMainRoutes.post('/webhook_sila',studentSilaCtrl.webHook);
studentMainRoutes.post('/webhook_arcus',studentSilaCtrl.webHookArcus);
studentMainRoutes.post('/test_ins',studentSilaCtrl.test_ins);
studentMainRoutes.post('/emergency_payment',studentSilaCtrl.payment_loan_manual);


//GIFT CARD ENDPOINTS
studentMainRoutes.post('/createGiftCard',studentGCCtrl.createGiftCards);
studentMainRoutes.get('/getGiftCards',studentGCCtrl.getGiftCards);
studentMainRoutes.post('/useGiftCard',studentGCCtrl.useGiftCard);


//test
studentMainRoutes.post('/testRoundups',studentMainCtrl.testRoundUps_BACKEDUP);
// studentMainRoutes.post('/manualFees',studentMainCtrl.manualFees);

module.exports = studentMainRoutes;