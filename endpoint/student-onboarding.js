
const studentOnboardCtrl = require('../controller/student-onboarding');
const express =  require('express');
const studentOnboardRoutes =  express.Router();
const checkAuth = require('../middleware/check-auth');
/**************************************************************
 * Student Onboarding Related routes
***************************************************************/
studentOnboardRoutes.post('/signup-via-social', studentOnboardCtrl.signupViaSocial);
studentOnboardRoutes.post('/manual-signup', studentOnboardCtrl.manualSignup);
studentOnboardRoutes.post('/signin',  studentOnboardCtrl.signin);
studentOnboardRoutes.post('/logout',  studentOnboardCtrl.logout);
studentOnboardRoutes.post('/internal',  studentOnboardCtrl.internalLogin);
studentOnboardRoutes.post('/save-basic-info', studentOnboardCtrl.saveBasicInfo);
studentOnboardRoutes.post('/update-kyc', studentOnboardCtrl.updateKyc);
studentOnboardRoutes.post('/delete-kyc', studentOnboardCtrl.deleteKyc);
studentOnboardRoutes.post('/update-permission', studentOnboardCtrl.updateUserPermission);
studentOnboardRoutes.post('/save-address', studentOnboardCtrl.saveAddress);
studentOnboardRoutes.post('/saveAddress', studentOnboardCtrl.saveAddressNoKyc);
studentOnboardRoutes.post('/verify_ssn', studentOnboardCtrl.verifySsn);
studentOnboardRoutes.post('/verify_ssn_new', studentOnboardCtrl.verifySsnNew);
studentOnboardRoutes.post('/verify_ssn_id', studentOnboardCtrl.uploadKYCPhoto);
studentOnboardRoutes.post('/update_ssn_new', studentOnboardCtrl.updateSsnNew);
studentOnboardRoutes.post('/is-notify-on', studentOnboardCtrl.isNotifyOn);
studentOnboardRoutes.post('/send-otp', studentOnboardCtrl.sendOtp);
studentOnboardRoutes.post('/verify-otp',studentOnboardCtrl.verifyOtp);
studentOnboardRoutes.post('/verify-otp-new',studentOnboardCtrl.verifyOtpNew);
studentOnboardRoutes.post('/verify-otp-email',studentOnboardCtrl.verifyOtpEmail);
studentOnboardRoutes.post('/resetPassword', studentOnboardCtrl.resetPassword);
studentOnboardRoutes.post('/goto-dashboard', studentOnboardCtrl.gotoDashboard);
studentOnboardRoutes.post('/dashboard-page', studentOnboardCtrl.dashboardPage);
studentOnboardRoutes.post('/forget-password', studentOnboardCtrl.forgetPassword);
studentOnboardRoutes.post('/change-password', studentOnboardCtrl.changePassword);
// studentOnboardRoutes.post('/testcreateuser', studentOnboardCtrl.testcreateuser);


studentOnboardRoutes.post('/checkhandle', studentOnboardCtrl.checkhandle);
studentOnboardRoutes.post('/checkSilaBalance', studentOnboardCtrl.checkAccountBalance);
studentOnboardRoutes.post('/checkAccountBalance', studentOnboardCtrl.checkAccountBalanceP);
studentOnboardRoutes.post('/registerwithkycsila', studentOnboardCtrl.registerwithkycsila);
studentOnboardRoutes.post('/registerBusiness', studentOnboardCtrl.registerBusiness);



studentOnboardRoutes.post('/getSilaTransactions', studentOnboardCtrl.getSilaTransactions);
studentOnboardRoutes.get('/generateWallet', studentOnboardCtrl.generateWallet);




studentOnboardRoutes.post('/checkkyc', studentOnboardCtrl.checkkyc);
studentOnboardRoutes.post('/requestkyc', studentOnboardCtrl.requestkyc);
studentOnboardRoutes.post('/getEntity', studentOnboardCtrl.getEntity);
studentOnboardRoutes.post('/getEntities', studentOnboardCtrl.getEntities);
studentOnboardRoutes.post('/linkbanksila', studentOnboardCtrl.linkbanksila);
studentOnboardRoutes.post('/getbanksSila', studentOnboardCtrl.getbanksSila);

studentOnboardRoutes.post('/issueSila', studentOnboardCtrl.issueSila);
studentOnboardRoutes.post('/issueSilaFromFBO', studentOnboardCtrl.issueSilaFromFBO);

studentOnboardRoutes.post('/getWallet', studentOnboardCtrl.getWallet);
studentOnboardRoutes.post('/getSilaBalance', studentOnboardCtrl.getSilaBalance);
studentOnboardRoutes.post('/transferSila', studentOnboardCtrl.transferSila);
studentOnboardRoutes.post('/redeemSila', studentOnboardCtrl.redeemSila);
studentOnboardRoutes.post('/getnaics', studentOnboardCtrl.getnaics);
studentOnboardRoutes.post('/encryptData', studentOnboardCtrl.encryptData);
studentOnboardRoutes.post('/decryptData', studentOnboardCtrl.decryptData);

module.exports = studentOnboardRoutes;