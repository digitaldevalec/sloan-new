const studentAdminCtrl = require('../controller/student-admin');
const express =  require('express');
const studentAdminRoutes =  express.Router();
/**************************************************************
 * Student Main Related routes
***************************************************************/
studentAdminRoutes.get('/getUsers',studentAdminCtrl.getUsers);
studentAdminRoutes.post('/getUsersById',studentAdminCtrl.getUsersById);
studentAdminRoutes.post('/getPaymentById',studentAdminCtrl.getPaymentById);
studentAdminRoutes.get('/getGiftcards',studentAdminCtrl.getGiftcards);
studentAdminRoutes.post('/createGiftCards',studentAdminCtrl.createGiftCards);
studentAdminRoutes.post('/removeGiftCard',studentAdminCtrl.removeGiftCard);
studentAdminRoutes.post('/fboToReserve',studentAdminCtrl.fbotoreserve);

module.exports = studentAdminRoutes;