const studentMiscCtrl = require('../controller/student-misc');
const express =  require('express');
const studentMiscRoutes =  express.Router();
/**************************************************************
 * Student Main Related routes
***************************************************************/
studentMiscRoutes.post('/register',studentMiscCtrl.register);
studentMiscRoutes.post('/signin',studentMiscCtrl.signin);
studentMiscRoutes.post('/getUserById',studentMiscCtrl.getUserById);
studentMiscRoutes.get('/getUsers',studentMiscCtrl.getUsers);
studentMiscRoutes.post('/updateUserById',studentMiscCtrl.updateUserbyId);
// studentMiscRoutes.post('/getPaymentById',studentMiscCtrl.getPaymentById);
// studentMiscRoutes.get('/getGiftcards',studentMiscCtrl.getGiftcards);
// studentMiscRoutes.post('/fboToReserve',studentMiscCtrl.fbotoreserve);

module.exports = studentMiscRoutes;