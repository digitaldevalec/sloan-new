
const studentPlaidCtrl = require('../controller/student-plaid');
const express =  require('express');
const checkAuth = require('../middleware/check-auth');
const studentPlaidRoutes =  express.Router();
/**************************************************************
 * Student Plaid Related routes
***************************************************************/
studentPlaidRoutes.post('/save-plaid-data', studentPlaidCtrl.savePlaidData);
studentPlaidRoutes.post('/get_plaid_institutions', studentPlaidCtrl.getInstitutions);
studentPlaidRoutes.post('/get_first_institutions', studentPlaidCtrl.getFirstInstitutions);
studentPlaidRoutes.post('/isRoundUpsOn', studentPlaidCtrl.isRoundUpsOn);
studentPlaidRoutes.post('/isRoundUpsOff', studentPlaidCtrl.isRoundUpsOff);
//studentPlaidRoutes.post('/createbanklogin',studentPlaidCtrl.createBankLogin);
studentPlaidRoutes.post('/saveloandata',studentPlaidCtrl.saveLoanData);
studentPlaidRoutes.post('/saveloandatanew',studentPlaidCtrl.saveLoanDataNew);
studentPlaidRoutes.post('/testCheckLoan',studentPlaidCtrl.checkLoan);
studentPlaidRoutes.post('/webhook_plaid',studentPlaidCtrl.plaidWebhook);
studentPlaidRoutes.post('/webhook_plaid_sandbox',studentPlaidCtrl.plaidWebhookSandbox);
studentPlaidRoutes.post('/verifyMicroSandbox',studentPlaidCtrl.verifyMicroSandbox);
studentPlaidRoutes.post('/verifyMicro',studentPlaidCtrl.verifyMicro);

studentPlaidRoutes.post('/getBalance',studentPlaidCtrl.getBalance);

studentPlaidRoutes.post('/verifyAutoMicroSandbox',studentPlaidCtrl.verifyAutoMicroSandbox);
studentPlaidRoutes.post('/verifyAutoMicro',studentPlaidCtrl.verifyAutoMicro);

module.exports = studentPlaidRoutes;