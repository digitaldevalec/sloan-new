// 'dbURI': 'mongodb://localhost:27017/sloan',
const env = process.env.NODE_ENV || 'localhost';
let localhost =  {
    app:{
        'SERVER_URL': 'http://13.56.117.240',
        'PORT': '3000',
        'dbURI': 'mongodb://127.0.0.1:27017/sloan',
        'JWT_KEY': "7379aa26c0deb3ccfae49ff5e412ea57",
        'FORMATTED_URL':'http://13.56.117.240:3000',
        'PLAID_CLIENT_ID':'5d1fc9f84fa1190016b491a3',
        'PLAID_SECRET':'0aeba71784b23ba07f12cd6fbb5f6c',
        'PUBLIC_KEY':'367207e72c92c392125d679242ed8e',
        'SGAPI': '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'//'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'
    },
    sila:{
        'handle': 'sloan',
        'key': "f4d0028da26cc3eb4071a89b453419af113871e53812f0636c57e981f044a2d2"
    }   
}
let test =  {
    app:{
        'PORT': '<test port>',
        'dbURI': '<test dbURI>',
    }
}

const config = {
   
    localhost,
    
};

module.exports = config[env]

// LIVE DATA


// app:{
//     'SERVER_URL': 'http://13.56.117.240',
//     'PORT': '3000',
//     'dbURI': 'mongodb://127.0.0.1:27017/sloan',
//     'JWT_KEY': "7379aa26c0deb3ccfae49ff5e412ea57",
//     'FORMATTED_URL':'http://13.56.117.240:3000',
//     'PLAID_CLIENT_ID':'5d1fc9f84fa1190016b491a3',
//     'PLAID_SECRET':'0aeba71784b23ba07f12cd6fbb5f6c'
//     'PUBLIC_KEY':'367207e72c92c392125d679242ed8e'
// },
// sila:{
//     'handle': 'sloan',
//     'key': "f4d0028da26cc3eb4071a89b453419af113871e53812f0636c57e981f044a2d2"
// }