const jwt = require('jsonwebtoken');
const jwtconf = require('../configaration/commonResources');
const models = require('../model/common');
const token_gen = require('../service/jwtTokenGenerator');

module.exports = async (req,res,next) => {
    const token = req.headers.authorization && req.headers.authorization != '' ? req.headers.authorization.split(" ")[1]:res.json({
        message : 'Improper Authorization Token',
        returnData : req.headers.authorization,
        statuscode : 214,
        isError:true
    });
    try{
        console.log(token)
        const decoded = jwt.verify(token, jwtconf.JWT_KEY); 
        req.userData = decoded;   
        let studentId =req.userData.studentId  
        let userType =req.userData.userType  
        await models.student.findOne({studentId :studentId},function (err, item) {
            uExists = item               
        })
       
        if(uExists == null){
            console.log('uExists token got', token)
            res.json({ isError:true,message : 'You are not registered',statuscode : 102})
        }
        else{
            let currentTimeStamp = Math.round((new Date()).getTime() / 1000);
            console.log('not uExists token got', currentTimeStamp);
            
            (decoded.exp <= currentTimeStamp) ? res.json({
                isError:true,
                statuscode: 504,
                message : 'Session expired',
                token :  token_gen({
                    ...decoded
                })
            }) : next();
        }
 

    }catch(error){
        
 /**
         * If error then check the type of error
         * if error type is TokenExpiredError : send new token 
         * else if error type is JsonWebTokenError : send error
         */

        if(error.name.toString()=='TokenExpiredError'){
            //send new token as previous token expired
            // retrive previous token expression
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            let prevPayload = JSON.parse(Buffer.from(base64, 'base64').toString('binary'));
            //create new token with previous payload
            const newToken = token_gen({
                'studentId': prevPayload.studentId,
                'userType' : prevPayload.userType
            });

            res.json({
                message : 'Session expired',
                isError:true,
                errorType : error.message,
                statuscode : 504,
                token :  newToken
            })
        }
        else if(error.name.toString()=='JsonWebTokenError'){
            // send error response
            res.json({
                isError:true,
                message : 'Auth Failed',
                errorType : error.message,
                statuscode : 404
            });
        }
     
    }
    
}