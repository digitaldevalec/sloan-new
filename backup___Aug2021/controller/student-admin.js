const mongoose = require('mongoose');
const models = require('../model/common');
const bcrypt = require("bcrypt");
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
var Request = require("request");
//SILA SETUP
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
//KMS SETUP
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);
module.exports = {   
    /**
     * @abstract 
     * Get all users
    */
    getUsers: async (req, res, next)=>{
        try{
            let aggreQuery = [{
                $project: { 'studentId': 1, 'fname': 1, 'lname': 1, 'EmailId': 1, 'profilePic': 1, 'loanDetails': 1, 'bankAccounts': 1, 'roundUps': 1   }
            }];
            await models.student.aggregate(aggreQuery).exec(async (err, item) => {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: err
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   	},

    getUsersById: async (req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide studentId",
                isError: true
            });
            await models.student.findOne({ 'studentId': studentId }, async function (err, item) {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: err
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    getPaymentById: async (req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide studentId",
                isError: true
            });
            await models.payment.find({ 'studentId': studentId }, async function (err, item) {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: err
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

   	getGiftcards: async (req, res, next)=>{
        try{
            let aggreQuery = [{
                $project: { 'giftCardId': 1, 'amount': 1, 'numberOfUses': 1, 'type': 1, 'studentId': 1  }
            }];
            await models.giftcards.aggregate(aggreQuery).exec(async (err, item) => {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: err
                    });
               }
               else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   	},

    createGiftCards: async(req, res, next)=>{
        try{
            let giftCardId = req.body.giftCardId ? req.body.giftCardId : res.json({
                message: "Please Provide giftCard ID",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide amount",
                isError: true
            });
            let numberOfUses = req.body.numberOfUses ? req.body.numberOfUses : res.json({
                message: "Please Provide number Of Uses",
                isError: true
            });
            let type = req.body.type ? req.body.type : res.json({
                message: "Please Provide Type of giftcard",
                isError: true
            });
            let expiry = req.body.expiry ? req.body.expiry : res.json({
                message: "Please Provide Expiry of giftcard",
                isError: true
            });

            giftCardId = giftCardId.toUpperCase();
            await models.giftcards.create({ "_id" : new mongoose.Types.ObjectId(), "giftCardId":giftCardId, "amount":amount, "numberOfUses":numberOfUses, "type":type, "expiry":expiry }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: "Gift Card Created Successfully",
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },

    removeGiftCard: async(req, res, next)=>{
        try{
            let giftCardId = req.body.giftCardId ? req.body.giftCardId : res.json({
                message: "Please Provide giftCard ID",
                isError: true
            });
            await models.giftcards.deleteOne({ "giftCardId" : giftCardId}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: "Gift Card Deleted Successfully",
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },


   	fbotoreserve: async(req, res, next)=>{
        try{
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide Amonut",
                isError: true
            });
            // FBO DETAILS
            const fboWalletKey = config.fboaccount.walletKey;
            const fboWalletAddress = config.fboaccount.walletAddress;
            var wpk = await decryptKeys(fboWalletKey);
            var wad = await decryptKeys(fboWalletAddress);
            var sender_handle = config.fboaccount.handle;

            const check_bal = await Sila.getSilaBalance(wad);
            var checkbalance = parseFloat(check_bal.data.sila_balance)/100;

            //RESERVE DETAILS
            var destination = config.reserve.handle;
            var dwpk = config.reserve.walletKey;
            var dwad = config.reserve.walletAddress;
            var destination_wallet = await decryptKeys(dwpk);
            var destination_address = await decryptKeys(dwad);

            var msg = "Transfer Funds";

            if(checkbalance >= amount) {
            	await Sila.transferSila(amount, sender_handle, wpk, destination, destination_wallet, destination_address, msg)
	            .then(async (response) => {
	                await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : "FBO", "paymentId" : "RESERVE", "amount" : amount, "fullAmount" : amount, "paymentFrom" : "SLOAN FBO", "bankId" : "SLOAN FBO", "bankName" : 'SLOAN FBO', "accountNum" : 'SLOAN FBO', "accountRouting" : 'SLOAN FBO', "accountType" : 'SLOAN FBO', "accountClass" : 'SLOAN FBO', "paymentType" : "SLOAN FBO TO SLOAN RESERVE", "loanId" : "NA", "loanAccount": "NA", "frequency": "One Time", "status": "admin_transfer", "loan_ins":"NA" }, function (perror, pitem) {
	                    if(!perror) {
	                        res.json({
			                    isError: false,
			                    message: 'FBO TO RESERVE SUCCESSFULL',
			                    statuscode: 200,
			                    details: response
			                }); 
	                    }
	                    else
	                    {
	                        res.json({
	                            isError: true,
	                            message: 'Database Error',
	                            statuscode: 404,
	                            details: perror
	                        }); 
	                    }
	                })
	            })
	            .catch((err) => { 
	                console.log(err);
	            });
            } else {
            	res.json({
                    isError: false,
                    message: 'BALANCE LOW',
                    statuscode: 200,
                    details: checkbalance
                }); 
            }
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'No Banks Available'
            });
        }
    },
}


function authcrypto(data){
    var private_key = 'f4d0028da26cc3eb4071a89b453419af113871e53812f0636c57e981f044a2d2';
    // Stringify the message. (The string that gets hashed should be
    // guaranteed to be the same as what is sent in the request.)
    // NOTE: if testing the example strings, you can just declare them as
    // strings, e.g. var message = 'Sila';
    var message = data

    // Generate the message hash using the Keccak 256 algorithm.
    var msg_hash = EthCrypto.hash.keccak256(message);

    // // Create a signature using your private key and the hashed message.
    var signature = EthCrypto.sign(private_key, msg_hash);

    // // The EthCrypto library adds a leading '0x' which should be removed 
    // // from the signature.
    signature = signature.substring(2);

    // The raw message should then be sent in an HTTP request body, and the signature
    // should be sent in a header.
    
    return data;
}

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}