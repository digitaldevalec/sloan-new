const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs');
const moment = require('moment');
const path = require('path');
const formidable = require('formidable');
var cron = require('node-cron');
const plaid = require('plaid');
var bodyParser = require('body-parser');
//const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
//LIVE PLAID
const plaidClient = new plaid.Client({
    clientID: config.app.PLAID_CLIENT_ID,
    secret: config.app.PLAID_SECRET,
    env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
    handle: config.sila.handle,
    key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// Initialize WebHooks module.
var WebHooks = require('node-webhooks')

var http = require("https");
var Request = require("request");
// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);

module.exports = {
    createGiftCards: async(req, res, next)=>{
        try{
            let giftCardId = req.body.giftCardId ? req.body.giftCardId : res.json({
                message: "Please Provide giftCard ID",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide amount",
                isError: true
            });
            let numberOfUses = req.body.numberOfUses ? req.body.numberOfUses : res.json({
                message: "Please Provide number Of Uses",
                isError: true
            });
            let type = req.body.type ? req.body.type : res.json({
                message: "Please Provide Type of giftcard",
                isError: true
            });
            let expiry = req.body.expiry ? req.body.expiry : res.json({
                message: "Please Provide Expiry of giftcard",
                isError: true
            });

            giftCardId = giftCardId.toUpperCase();

            await models.giftcards.create({ "_id" : new mongoose.Types.ObjectId(), "giftCardId":giftCardId, "amount":amount, "numberOfUses":numberOfUses, "type":type, "expiry":expiry }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: "Gift Card Created Successfully",
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },
    useGiftCard: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let giftcard = req.body.giftcard ? req.body.giftcard : res.json({
                message: "Please Provide GIFT CARD",
                isError: true
            });
            giftcard = giftcard.toUpperCase();
            await models.giftcards.findOne({ 'giftCardId': giftcard }, async function (gerror, gitem) {
                if (gerror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['500'],
                        statuscode: 500,
                        details: gerror
                    });
                }
                else {
                    console.log(gitem);
                    if(gitem) {                
                        const userHandle = studentId+'.silamoney.eth';
                        var silamt = parseFloat(gitem.amount)*100;
                        var amount = gitem.amount;
                        if(gitem.type == 'OPU') {
                            if(gitem.studentId.indexOf(studentId) === -1) {
                            	console.log(gitem.studentId.indexOf(studentId));
                                await models.giftcards.update({ 'giftCardId': giftcard }, { $push:{'studentId':studentId} }, async function (error, item) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['500'],
                                            statuscode: 500,
                                            details: error
                                        });
                                    }
                                    else {
                                        console.log("Student > ",studentId);
                                        await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                                            if (error) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['405'],
                                                    statuscode: 405,
                                                    details: error
                                                });
                                            }
                                            else {
                                                var wpk = await decryptKeys(config.reserve.walletKey);
                                                const reserveWalletKey = config.reserve.walletKey;
                                                const reserveWalletAddress = config.reserve.walletAddress;
                                                var userHandle = config.reserve.handle;
                                                var wpk = await decryptKeys(reserveWalletKey);
                                                var msg = "Transfer Funds Gift Card";
                                                var destination = studentId+".silamoney.eth";
                                                var destination_wallet = await decryptKeys(item.walletKey);
                                                var destination_address = await decryptKeys(item.walletAddress);
                                                const response = await Sila.transferSila(silamt, userHandle, wpk, destination, destination_wallet, destination_address, msg);
                                                if(response.statusCode == 200) {
                                                    await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : amount, "paymentFrom" : "SLOAN RESERVE ACCOUNT", "bankId" : "NONE", "paymentType" : "giftcard", "loanId" : "NONE", "loanAccount": "NONE", "frequency": "---", "status":"transfer" }, async function (perror, pitem) {
                                                        if (perror) {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['405'],
                                                                statuscode: 405,
                                                                details: error
                                                            });
                                                        } else {
                                                            res.json({
                                                                isError: false,
                                                                message: 'Processing Funds. It will reflect in your Sloan Balance in some time.',
                                                                statuscode: 200,
                                                                details: response.data,
                                                                amount: amount
                                                            });
                                                        }
                                                    })
                                                } else {
                                                    res.json({
                                                        isError: true,
                                                        message: 'Problem Adding Funds',
                                                        statuscode: response.statusCode,
                                                        details: response.data
                                                    });
                                                }
                                            }
                                        })
                                    }
                                })
                            } else {
    	                    	res.json({
    	                            isError: true,
    	                            message: errorMsgJSON['ResponseMsg']['405'],
    	                            statuscode: 200,
    	                            details: "Gift Card Expired"
    	                        });
    	                    }
                        } else if(gitem.type == 'OG' && gitem.studentId.length === 0) {
                            if(gitem.studentId.indexOf(studentId) === -1) {
                                await models.giftcards.update({ 'giftCardId': giftcard }, { $push:{'studentId':studentId} }, async function (error, item) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['500'],
                                            statuscode: 500,
                                            details: error
                                        });
                                    }
                                    else
                                    {
                                        await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                                            if (error) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['405'],
                                                    statuscode: 405,
                                                    details: error
                                                });
                                            }
                                            else {
                                                var wpk = await decryptKeys(config.reserve.walletKey);
                                                const reserveWalletKey = config.reserve.walletKey;
                                                const reserveWalletAddress = config.reserve.walletAddress;
                                                var userHandle = config.reserve.handle;
                                                var wpk = await decryptKeys(reserveWalletKey);
                                                var msg = "Transfer Funds Gift Card";
                                                var destination = studentId+".silamoney.eth";
                                                var destination_wallet = await decryptKeys(item.walletKey);
                                                var destination_address = await decryptKeys(item.walletAddress);
                                                const response = await Sila.transferSila(silamt, userHandle, wpk, destination, destination_wallet, destination_address, msg);
                                                if(response.statusCode == 200) {
                                                    await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : amount, "paymentFrom" : "SLOAN RESERVE ACCOUNT", "bankId" : "NONE", "paymentType" : "giftcard", "loanId" : "NONE", "loanAccount": "NONE", "frequency": "---", "status":"transfer" }, async function (perror, pitem) {
                                                        if (perror) {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['405'],
                                                                statuscode: 405,
                                                                details: error
                                                            });
                                                        } else {
                                                            res.json({
                                                                isError: false,
                                                                message: 'Processing Funds. It will reflect in your Sloan Balance in some time.',
                                                                statuscode: 200,
                                                                details: response.data,
                                                                amount: amount
                                                            });
                                                        }
                                                    })
                                                } else {
                                                    res.json({
                                                        isError: true,
                                                        message: 'Problem Adding Funds',
                                                        statuscode: response.statusCode,
                                                        details: response.data
                                                    });
                                                }
                                            }
                                        })
                                    }
                                })
                            }  else {
    	                    	res.json({
    	                            isError: true,
    	                            message: errorMsgJSON['ResponseMsg']['405'],
    	                            statuscode: 200,
    	                            details: "Gift Card Expired"
    	                        });
    	                    }
                        } 
                    } else {
                        res.json({
                            isError: true,
                            message:  "Gift Card Not Found",
                            statuscode: 201,
                            details:  "Gift Card Not Found"
                        });
                    }
                }
            })                
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },
    getGiftCards: async(req, res, next)=>{
        try{
            models.giftcards.find({}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: "Available Gift Cards",
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },
}

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}