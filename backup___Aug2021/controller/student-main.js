const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs'); 
const moment = require('moment'); 
const path = require('path');
const formidable = require('formidable');
var cron = require('node-cron');
const plaid = require('plaid');
var bodyParser = require('body-parser');
const winston = require('winston');
// var expressWinston = require('express-winston');
// Logger configuration
const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: 'logs/main/main_controller.'+moment().format('YYYY_MM_DD')+'.log'
        })
    ]
};

const logger_main = winston.createLogger(logConfiguration);
//const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
//LIVE PLAID
const plaidClient = new plaid.Client({
  clientID: config.app.PLAID_CLIENT_ID,
  secret: config.app.PLAID_SECRET,
  env: plaid.environments.production
});

const plaidClientSandbox = new plaid.Client({
  clientID: '5d1fc9f84fa1190016b491a3',
  secret: '465fe994f2595d372976e04d544ec5',
  env: plaid.environments.sandbox
});

var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// Initialize WebHooks module.
var WebHooks = require('node-webhooks')

var http = require("https");
var Request = require("request");
// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);

module.exports = {
    getBanks: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            models.student.findOne({ 'studentId': studentId }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: item.bankAccounts
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'No Banks Available'
            });
        }
    },
    addfunds: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide Amount",
                isError: true
            });
            let bankId = req.body.bankId ? req.body.bankId : res.json({
                message: "Please Provide bank Id",
                isError: true
            });
            var msg = "Add Funds";
            const userHandle = studentId+'.silamoney.eth';
            var silamt = parseFloat(amount)*100;
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    var wpk = await decryptKeys(item.walletKey);
                    const response = await Sila.issueSila(
                      silamt,
                      userHandle,
                      wpk,
                      bankId,
                      msg,
                      config.sila.business_uuid
                    );
                    if(response.statusCode == 200) {
                        await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : amount, "paymentFrom" : "bank", "bankId" : bankId, "paymentType" : "FUNDADD", "loanId" : "NONE", "loanAccount": "NONE", "frequency": "---" }, async function (perror, pitem) {
                            if (perror) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['405'],
                                    statuscode: 405,
                                    details: error
                                });
                            } else {
                                res.json({
                                    isError: false,
                                    message: 'Processing Funds. It will reflect in your Sloan Balance in some time.',
                                    statuscode: 200,
                                    details: response.data
                                });
                            }
                        })
                    } else {
                        res.json({
                            isError: true,
                            message: 'Problem Adding Funds',
                            statuscode: response.statusCode,
                            details: response.data
                        });
                    }
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
	deleteBank: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
			let bankId = req.body.bankId ? req.body.bankId : res.json({
                message: "Please Provide Bank ID",
                isError: true
            });
            var request = require("request");
            const userHandle = studentId+'.silamoney.eth';
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    await models.recurring.find({"bankId" : bankId}, async function (rerr, rres) {
                        if (rerr) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: rerr
                            })
                        } else {
                            if(rres.length > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['401'],
                                    statuscode: 401,
                                    details: rres
                                });
                            } else {
                                var agquery = [
                                    { $unwind: "$bankAccounts" },
                                    {
                                      $match: { "bankAccounts.reference": bankId }
                                    }
                                ]
                                var getbankaccountNum = await models.student.aggregate(agquery).exec();
                                if(getbankaccountNum) {
                                    var delbankNumber = getbankaccountNum[0]['bankAccounts']['accountNum'];
                                    var wpk = await decryptKeys(item.walletKey)
                                    var updatedata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "synapseACHId": { "bankacc": bankId }}});      
                                    // console.log(updatedata   );
                                    if(updatedata){
                                        var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "reference": bankId }}});
                                        if(deletednewbankdata)
                                        {
                                            var updateRoundups = await models.student.updateOne({ 'studentId': studentId }, {$pull:{ 'roundUps': {'synpaseNode':bankId} }});
                                            if(updateRoundups)
                                            {
                                                await models.student.findOne({ 'studentId': studentId }, async function (geterror, getitem) {
                                                    if(geterror) {
                                                        throw geterror;
                                                    } else {
                                                        //const delbank = await Sila.deleteAccount(userHandle, delbankNumber, wpk);
                                                        res.json({
                                                            isError: false,
                                                            message: 'Bank Deleted Successfully',
                                                            statuscode: 200,
                                                            details: getitem.bankAccounts
                                                        });  
                                                    }
                                                });
                                            }
                                        }
                                    } 
                                    res.json({
                                        isError: false,
                                        message: 'Bank Deleted Successfully',
                                        statuscode: 200,
                                        details: delbank
                                    }); 
                                }
                                // var updatedata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "synapseACHId": { "bankacc": bankId }}});      
                                // // console.log(updatedata   );
                                // if(updatedata){
                                //     var deletednewbankdata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "bankAccounts": { "reference": bankId }}});
                                //     if(deletednewbankdata)
                                //     {
                                //         var updateRoundups = await models.student.updateOne({ 'studentId': studentId }, {$pull:{ 'roundUps': {'synpaseNode':bankId} }});
                                //         if(updateRoundups)
                                //         {
                                //             await models.student.findOne({ 'studentId': studentId }, async function (geterror, getitem) {
                                //                 if(geterror) {
                                //                     throw geterror;
                                //                 } else {
                                //                     res.json({
                                //                         const delbank = await sila.deleteAccount(userHandle, "account name", "user private key");
                                //                         isError: false,
                                //                         message: 'Bank Deleted Successfully',
                                //                         statuscode: 200,
                                //                         details: getitem.bankAccounts
                                //                     });  
                                //                 }
                                //             });
                                //         }
                                //     }
                                // } 
                            }
                        }
                    })
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    recommendedList: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            models.student.findOne({ 'studentId': studentId }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: 'Success',
                        statuscode: 200,
                        details: [item]
                    });   
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    editProfile: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let actionname = req.body.actionname ? req.body.actionname : res.json({
                message: "Please Provide Which Field to Modify",
                isError: true
            });
            let actionvalue = req.body.actionvalue ? req.body.actionvalue : res.json({
                message: "Please Provide Modified data",
                isError: true
            });
            var modifyset = {};
            modifyset[actionname] = actionvalue;
            models.student.update({"studentId": studentId},{ $set: modifyset }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    models.student.findOne({ "studentId": studentId }, function (errorall, itemall) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: 'Successfully Updated',
                                statuscode: 200,
                                details: itemall
                            });   
                        }
                    })
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    addActivity: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let activities = req.body.activities ? req.body.activities : res.json({
                message: "Please Provide Which Field to Modify",
                isError: true
            });
            models.student.updateOne({"studentId": studentId},{ $set: { "activities": activities } }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    models.student.findOne({ "studentId": studentId }, function (errorall, itemall) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: 'Successfully Updated',
                                statuscode: 200,
                                details: itemall
                            });   
                        }
                    }) 
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    deleteActivity: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let activities = req.body.activities ? req.body.activities : res.json({
                message: "Please Provide Which Field to Modify",
                isError: true
            });
            models.student.update({"studentId": studentId},{ $set: { "activities": activities }}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    models.student.findOne({ "studentId": studentId }, function (errorall, itemall) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: 'Successfully Updated',
                                statuscode: 200,
                                details: itemall
                            });   
                        }
                    }) 
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    uploadPhoto: async(req, res, next)=>{
        try{
            const form = new formidable.IncomingForm(); 
            form.parse(req, function(err, fields, files){ 
          
                var oldPath = files.photo.path; 
                var newPath = path.join(__dirname, '../../sloanapp/uploads') + '/'+files.photo.name ;
                var rawData = fs.readFileSync(oldPath);
                var studentId = fields.studentId;
              
                fs.writeFile(newPath, rawData, function(err){ 
                    var modifiedpath = config.app.SERVER_URL+"/uploads/"+files.photo.name;
                    models.student.updateOne({"studentId": studentId},{ $set: { "profilePic": modifiedpath } }, function (error, item) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            models.student.findOne({ "studentId": studentId }, function (errorall, itemall) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: error
                                    });
                                }
                                else {
                                    res.json({
                                        isError: false,
                                        message: 'Successfully Updated',
                                        statuscode: 200,
                                        details: itemall
                                    });   
                                }
                            })
                        }
                    })
                }) 
            }) 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getDeviceData: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let deviceId = req.body.deviceId ? req.body.deviceId : res.json({
                message: "Please Provide device ID",
                isError: true
            });
            let pushToken = req.body.pushToken ? req.body.pushToken : res.json({
                message: "Please Provide push Token ID",
                isError: true
            });
            let deviceType = req.body.deviceType ? req.body.deviceType : res.json({
                message: "Please Provide device Type",
                isError: true
            });
            models.student.updateOne({ 'studentId': studentId, 'isLoggedIn': true },{ 'deviceId':deviceId, 'pushToken':pushToken, 'deviceType':deviceType }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                } else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    });
                }
                //            else {
                //            	var request = require('request');
                // var options = {
                //   'method': 'POST',
                //   'url': 'https://onesignal.com/api/v1/notifications',
                //   'headers': {
                //     'Authorization': 'Basic NTdmY2Q3ODEtOTBhOS00YjI3LWJmMzYtN2QzZWIzNWIxYjEy',
                //     'Content-Type': 'application/json',
                //     'Cookie': '__cfduid=d67bf80b968068dec011c539bddcffeb41590565308'
                //   },
                //   body: JSON.stringify({"app_id":"f881d3e7-8048-4996-9bb1-4fb23334329b","contents":{"en":"Push Notifications On"},"include_player_ids":[deviceId]})

                // };
                // request(options, function (error, response, body) { 
                // 	if(response)
                // 	 {
                // 	 	res.json({
                //                      isError: false,
                //                      message: 'Success',
                //                      statuscode: 200,
                //                      details: JSON.parse(body)
                //                  });
                // 	}
                // 	else
                // 	{
                // 		res.json({
                //                      isError: true,
                //                      message: 'Error in One Signal API',
                //                      statuscode: 404,
                //                      details: response
                //                  });
                // 	}
                // });
                //            }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    keywordSearch: async(req, res, next)=>{
        try{
            let keyword = req.body.keyword ? req.body.keyword : res.json({
                message: "Please Provide keyword",
                isError: true
            });
            models.student.find({$or:[{"fname":{$regex: ".*" + keyword + ".*"}}, {"lname":{$regex: ".*" + keyword + ".*"}}, {"mobileNo":{$regex: ".*" + keyword + ".*"}}]}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: 'Success',
                        statuscode: 200,
                        details: item
                    });   
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    requestMoney: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide amount",
                isError: true
            });
            let requestedId = req.body.requestedId ? req.body.requestedId : res.json({
                message: "Please Provide requested ID",
                isError: true
            });
            models.student.findOne({ 'studentId': studentId }, function (errorone, itemone) {
            	if (errorone) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: errorone
                    });
                }
                else {
		            models.notifications.create({ '_id': new mongoose.Types.ObjectId(), "studentId": requestedId, "amount": amount, "action": itemone.fname+" has requested $"+amount, "actionDesc": studentId, "type":"money_request" }, function (error, item) {
		                if (error) {
		                    res.json({
		                        isError: true,
		                        message: errorMsgJSON['ResponseMsg']['405'],
		                        statuscode: 405,
		                        details: error
		                    });
		                }
		                else {
		                    models.student.findOne({ 'studentId': requestedId }, function (errorreq, itemreq) {
				                if (error) {
				                    res.json({
				                        isError: true,
				                        message: errorMsgJSON['ResponseMsg']['405'],
				                        statuscode: 405,
				                        details: errorreq
				                    });
				                }
				                else {
				                	var request = require('request');
									var options = {
									  'method': 'POST',
									  'url': 'https://onesignal.com/api/v1/notifications',
									  'headers': {
									    'Authorization': 'Basic NTdmY2Q3ODEtOTBhOS00YjI3LWJmMzYtN2QzZWIzNWIxYjEy',
									    'Content-Type': 'application/json'
									  },
									  body: JSON.stringify({"app_id":"f881d3e7-8048-4996-9bb1-4fb23334329b","contents":{"en":itemreq.fname+" has requested $"+amount },"include_player_ids":[itemreq.deviceId]})

									};
									request(options, function (error, response, body) { 
										console.log(JSON.parse(body));
										if(response)
										 {
										 	res.json({
						                        isError: false,
						                        message: 'Success',
						                        statuscode: 200,
						                        details: JSON.parse(body)
						                    });
										}
										else
										{
											res.json({
						                        isError: true,
						                        message: 'Error in One Signal API',
						                        statuscode: 404,
						                        details: response
						                    });
										}
									});
				                }
				            })
		                }
		            })
				}
			})
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    notificationList: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            models.notifications.find({ "studentId": studentId }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: 'Success',
                        statuscode: 200,
                        details: item
                    });
                }
            }).sort( { 'createdAt': -1 } )
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getStudentData: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            var query = [
                { $match : { "studentId" : studentId } }
            ];
            await models.student.aggregate(query).exec( async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    var agquery = [
                            { $match : { 
                                "$and":[
                                    { "studentId" : studentId },{ "flag": false }
                                ]} 
                            },
                            {
                                $group:
                                {
                                    _id: "$studentId",
                                    totalAmount: { $sum: "$roundup" }
                                }
                            }
                        ];
                        var wad;
                        if(item[0].walletAddress != '') {
                            wad = await decryptKeys(item[0].walletAddress);
                        }
                        balance = await Sila.getSilaBalance(wad);
                        item[0].totalLoan=item[0].loanDetails.length;
                        item[0].allLoans=item[0].loanDetails;
                        item[0].sloanbalance = parseFloat(balance.data.sila_balance)/100;
                        await models.roundups.aggregate(agquery).exec(async (errpay, itempay) => {
                        if(errpay) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        } else {
                            if(itempay.length > 0) {
                                item[0]['totalsaved'] = itempay[0]['totalAmount'];
                            } else {
                                item[0]['totalsaved'] = 0;
                            }
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['201'],
                                statuscode: 200,
                                details: item[0]
                            });
                        }
                    });
                }
            });
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    notificationChangeState: async(req, res, next)=>{
        try{
            let notificationId = req.body.notificationId ? req.body.notificationId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            models.notifications.updateOne({ "_id": mongoose.Types.ObjectId(notificationId) }, {"isRead":true}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: 'Success',
                        statuscode: 200,
                        details: item
                    });
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    paymentHistory: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let loanId = req.body.loanId ? req.body.loanId : res.json({
                message: "Please Provide Loan ID",
                isError: true
            });
            var aggrquery = [
                { $match : { 
                    "$and":[
                        { "studentId" : studentId }, { "loanId":loanId }
                    ]} 
                },
                { $sort : { createdAt : -1 } }
            ];
            models.payment.aggregate(aggrquery).exec(function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: 'Success',
                        statuscode: 200,
                        details: item
                    });
                }
            })
                 
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    userTransactions: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            var userHandle = studentId+'.silamoney.eth';
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    var wpk = await decryptKeys(item.walletKey);
                    var trans = await Sila.getTransactions(userHandle, wpk); 
                    if(trans.statusCode == 200){
                        var alltrans = trans.data.transactions;
                        var newdata = [];
                        alltrans.forEach((element) => {
                            var elem = {};
                            elem['amount'] = (parseFloat(element.sila_amount)/100).toFixed(2);
                            elem['trans_id'] = element.transaction_id;
                            elem['user_id'] = element.user_handle;
                            var bankobj = item['bankAccounts'].find(o => o.accountNum === element.bank_account_name);
                            elem['bank'] = (bankobj == undefined) ? "Loan Payment" : bankobj.bankDispName;
                            elem['created'] = element.created_epoch;
                            elem['status'] = element.status;
                            elem['color'] = "#29DA8B";
                            newdata.push(elem);
                        });
                        res.json({
                            isError: false,
                            message: 'Successfully Received transactions',
                            statuscode: 200,
                            details: newdata
                        });
                    }
                }
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    userAllTransactions: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            var userHandle = studentId+'.silamoney.eth';
            await models.student.findOne({ 'studentId': studentId }, async function (errorstudent, itemstudent) {
                if (errorstudent) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                } else {
		            await models.payment.find({ 
		                "studentId": studentId, 
		                "bankId":{ $exists:true }, 
		                "paymentType": { $ne: "roundup_fees" }
                	}).sort( { "createdAt": -1 } ).exec(async function (error, item) {
		                if (error) {
		                    res.json({
		                        isError: true,
		                        message: errorMsgJSON['ResponseMsg']['405'],
		                        statuscode: 405,
		                        details: error
		                    });
		                }
		                else {
		            		var newdata = [];
		                    await item.forEach((element) => {
		                        var elem = {};
		                        var sign;
		                        if(element.paymentType == 'roundup' || element.paymentType == 'giftcard' || element.paymentType == 'FUNDADD') {
		                        	sign = '+';
		                        } else {
		                        	sign = '-';
		                        }
		                        if(element.amount != undefined) {
		                        	elem['amount'] = sign+'$'+element.amount.toFixed(2);
		                        }
		                        elem['trans_id'] = element.paymentId ? element.paymentId : 'N/A';
		                        elem['user_id'] = element.studentId;
		                        if(element.bankId == 'NONE') {
		                        	elem['bank'] = '';
		                        } else if(element.paymentType == 'roundup'){
		                        	elem['bank'] = element.bankName+' ( '+element.accountNum.slice(element.accountNum.length - 4)+' )';
		                        } else if(element.paymentType == 'loan'){
		                        	elem['bank'] = element.bankName+' ( '+element.accountNum.slice(element.accountNum.length - 4)+' )';
		                        } else if(element.paymentType == 'FUNDADD'){
		                        	elem['bank'] = element.bankName+' ( '+element.accountNum.slice(element.accountNum.length - 4)+' )';
		                        }
		                        var loanobj = itemstudent['loanDetails'].find(o => o.account_number === element.loanAccount);
		                        if(loanobj != undefined) {
		                        	elem['paidfor'] = loanobj.guarantor.trim()+' ( '+loanobj.account.mask+' )';
		                        	console.log(loanobj.guarantor.trim()+' ( '+loanobj.account.mask+' )');
		                        } else if(loanobj === undefined && element.paymentType == 'roundup'){
		                        	elem['paidfor'] = "Round Up";
		                        } else if(loanobj === undefined && element.paymentType == 'giftcard'){
		                        	elem['paidfor'] = "GiftCard";
		                        } else if(loanobj === undefined && element.paymentType == 'FUNDADD'){
		                        	elem['paidfor'] = "Fund Wallet";
		                        } 
		                        var date = new Date(element.createdAt); 
		                        elem['created'] = date.getTime();
		                        if(element.status == 'success') {
		                        	elem['status'] = 'Completed';
		                        	elem['color'] = "#29DA8B";
		                        } else if(element.status == 'redeem' && element.paymentId == null) {
		                        	elem['status'] = 'Completed';
		                        	elem['color'] = "#29DA8B";
		                        } else if(element.status == 'redeem' && element.paymentId != null) {
		                        	elem['status'] = 'Pending';
		                        	elem['color'] = "#da9329";
		                        } else if(element.status == 'pending') {
		                        	elem['status'] = 'Pending';
		                        	elem['color'] = "#da9329";
		                        } else if(element.status == 'transfer') {
		                        	elem['status'] = 'Pending';
		                        	elem['color'] = "#da9329";
		                        } else {
		                        	elem['status'] = 'Payment Failed';
		                        	elem['color'] = "#ff0000";
		                        }
		                        newdata.push(elem);
		                    });
		                    res.json({
		                        isError: false,
		                        message: 'Successfully Received transactions',
		                        statuscode: 200,
		                        details: newdata
		                    });
		                }
		            });
		        }
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    checkManualTransactions: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let wpk = req.body.wpk ? req.body.wpk : res.json({
                message: "Please Provide WPK",
                isError: true
            });
            var userHandle = studentId+'.silamoney.eth';
            var trans = await Sila.getTransactions(userHandle, wpk); ;
            res.json({
                isError: false,
                message: 'Successfully Received transactions',
                statuscode: 200,
                details: trans
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    banktransaction: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let silaamt = req.body.silaamt ? req.body.silaamt : res.json({
                message: "Please Provide Amount",
                isError: true
            });      
            let loanname = req.body.loanname ? req.body.loanname : res.json({
                message: "Please Provide loanname",
                isError: true
            });
            let loanid = req.body.loanid ? req.body.loanid : res.json({
                message: "Please Provide loanid",
                isError: true
            });
            let loanAccount = req.body.loanAccount ? req.body.loanAccount : res.json({
                message: "Please Provide loan Account Number",
                isError: true
            });
            let bank_reference = req.body.bank_reference ? req.body.bank_reference : res.json({
                message: "Please Provide bank Id",
                isError: true
            });
            let bankDispName = req.body.bankDispName ? req.body.bankDispName : res.json({
                message: "Please Provide bank Display Name",
                isError: true
            });            
            let bankName = req.body.bankName ? req.body.bankName : res.json({
                message: "Please Provide bank Name",
                isError: true
            });
            let accountNum = req.body.accountNum ? req.body.accountNum : res.json({
                message: "Please Provide Account Number",
                isError: true
            });
            let accountRouting = req.body.accountRouting ? req.body.accountRouting : res.json({
                message: "Please Provide accountRouting",
                isError: true
            });
            let accountType = req.body.accountType ? req.body.accountType : res.json({
                message: "Please Provide accountType",
                isError: true
            });
            let accountClass = req.body.accountClass ? req.body.accountClass : res.json({
                message: "Please Provide accountClass",
                isError: true
            });
            let frequency = req.body.frequency ? req.body.frequency : res.json({
                message: "Please Provide Frequency",
                isError: true
            });
            let ins = req.body.ins ? req.body.ins : res.json({
                message: "Please Provide instituion id",
                isError: true
            });
            const userHandle = studentId+'.silamoney.eth';
            var amount = (parseFloat(silaamt)*100)+parseFloat(199);
            var msg = "BANK-SILA";
            var d;
            if(frequency == "Weekly") {
                var d = moment().add(7, 'days').format('YYYY-MM-DD');
            } else if(frequency == "Monthly") {
                var d = moment().add(1, 'months').format('YYYY-MM-DD');
            }
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    var wpk = await decryptKeys(item.walletKey);
                    await Sila.issueSila(amount, userHandle, wpk, bankName, msg, config.sila.business_uuid)
                    .then(async (response) => { 
                        await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : silaamt, "paymentFrom" : "bank", "bankId" : bank_reference, "bankName" : bankDispName, "accountNum" : accountNum, "accountRouting" : accountRouting, "accountType" : accountType, "accountClass" : accountClass, "paymentType" : "loan", "loanId" : loanid, "loanAccount": loanAccount, "frequency": frequency, "loan_ins":ins }, async function (error, item) {
                            if(!error) {
                                if(frequency != "One Time") {
                                    await models.recurring.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : silaamt, "bankId" : bank_reference, "bankName" : bankDispName, "accountNum" : accountNum, "accountRouting" : accountRouting, "accountType" : accountType, "accountClass" : accountClass, "loanId" : loanid, "loanname":loanname, "loanAccount": loanAccount, "frequency": frequency, "next_payment_date": d, "loan_ins":ins }, async function (rerror, ritem) {
                                        if(!rerror) {
                                            res.json({
                                                isError: false,
                                                message: 'Loan in Processing Queue',
                                                statuscode: 200,
                                                details: ritem,
                                                data: response
                                            });                                                     
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: 'Database Error',
                                                statuscode: 404,
                                                details: rerror
                                            }); 
                                        }
                                    })
                                } else {
                                    res.json({
                                        isError: false,
                                        message: 'One Time Amount Paid Successfully',
                                        statuscode: 200,
                                        details: item
                                    });  
                                }
                            }
                            else
                            {
                                console.log(error);
                            }
                        })
                    })
                    .catch((err) => { 
                        res.json({
                            isError: true,
                            message: 'Redemtion Failed',
                            statuscode: 404,
                            details: err
                        });
                    });
                }
            })        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getAutoPayments: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : req.json({ 
                message: "Please Provide Student Id", 
                isError: true 
            });
            models.recurring.find({ "studentId": studentId }, function (error, result){
                if(error) {
                    res.json({
                        isError: true,
                        message: 'Database Error',
                        statuscode: 404,
                        details: error
                    }); 
                } else {
                    res.json({
                        isError: false,
                        message: 'Auto Payments Retreived Successfully',
                        statuscode: 200,
                        details: result
                    }); 
                }
            })
        } catch(e) {
            res.json({
                isError: true,
                message: 'Database Error',
                statuscode: 404,
                details: e
            }); 
        }
    },
    addAutoPayment: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : req.json({ 
                message: "Please Provide Student Id", 
                isError: true 
            });
        } catch(e) {
            res.json({
                isError: true,
                message: 'Database Error',
                statuscode: 404,
                details: e
            }); 
        }
    },
    deleteAutoPayment: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : req.json({ 
                message: "Please Provide Student Id", 
                isError: true 
            });
            let id = req.body.id ? req.body.id : req.json({ 
                message: "Please Provide Id", 
                isError: true 
            });
            await models.recurring.find({ "_id": id }, async function (serr, sres){
                if(serr) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: serr
                    });
                } else {
                    if(sres.length > 0) {
                        await models.recurring.remove({ "_id": id }, async function (derr, dres) {
                            if(derr) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: derr
                                });
                            } else {
                                res.json({
                                    isError: false,
                                    message: "Auto Payment Deleted",
                                    statuscode: 200,
                                    details: sres
                                });
                            }
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: "AutoPayment Not Found",
                            statuscode: 404,
                            details: sres
                        });
                    }
                }
            })
        } catch(e) {
            res.json({
                isError: true,
                message: 'Database Error',
                statuscode: 404,
                details: e
            }); 
        }
    },
    editAutoPayment: async(req, res, next)=>{
        try {
            let autoId = req.body.autoId ? req.body.autoId : res.json({
                message: "Please Provide auto Payment ID",
                isError: true
            });
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let amount = req.body.amount ? req.body.amount : res.json({
                message: "Please Provide Amount",
                isError: true
            });
            let achbankid = req.body.bankId ? req.body.bankId : res.json({
                message: "Please Provide bank Id",
                isError: true
            });
            let bankName = req.body.bankName ? req.body.bankName : res.json({
                message: "Please Provide bank Name",
                isError: true
            });
            let bankDispName = req.body.bankDispName ? req.body.bankDispName : res.json({
                message: "Please Provide bank Display Name",
                isError: true
            });   
            let accountNum = req.body.accountNum ? req.body.accountNum : res.json({
                message: "Please Provide Account Number",
                isError: true
            });
            let accountRouting = req.body.accountRouting ? req.body.accountRouting : res.json({
                message: "Please Provide accountRouting",
                isError: true
            });
            let accountType = req.body.accountType ? req.body.accountType : res.json({
                message: "Please Provide accountType",
                isError: true
            });
            let accountClass = req.body.accountClass ? req.body.accountClass : res.json({
                message: "Please Provide accountClass",
                isError: true
            });
            let freq = req.body.frequency ? req.body.frequency : res.json({
                message: "Please Provide Frequency",
                isError: true
            });
            var frequency;
            if(freq == '1') {
                frequency = "One Time";
            } else if(freq == '2') {
                frequency = "Weekly";
            } else if(freq == '3') {
                frequency = "Monthly";
            }
            await models.recurring.find({ "_id": mongoose.Types.ObjectId(autoId) }, async function (err, item){
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: err
                    });
                } else {
                    var d;
                    if(item[0]['frequency'] === frequency ) {
                        d = item[0].next_payment_date;
                    } else {
                        if(frequency == "Weekly") {
                            d = moment().add(7, 'days').format('YYYY-MM-DD');
                        } else if(frequency == "Monthly") {
                            d = moment().add(1, 'months').format('YYYY-MM-DD');
                        }
                    }
                    var data = {
                        "amount" : amount,
                        "bankId" : achbankid,
                        "bankName" : bankDispName,
                        "accountNum" : accountNum,
                        "accountRouting" : accountRouting,
                        "accountType" : accountType,
                        "accountClass" : accountClass,
                        "frequency": frequency,
                        "next_payment_date": d
                    };
                    await models.recurring.update({ "_id": mongoose.Types.ObjectId(autoId) }, { $set: data }, async function (error, result){
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        } else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: result
                            });
                        }
                    })
                }
            })
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            });
        }
    },
    deleteLoan: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let loanId = req.body.loanId ? req.body.loanId : res.json({
                message: "Please Provide Loan ID",
                isError: true
            });
            var request = require("request");
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    console.log("Loan Found > ",item['loanDetails']);
                    if(item['loanDetails'].length > 0) {
                        var deletedloandata = await models.student.updateOne({ "studentId": studentId },{ $pull: { "loanDetails": { "account_id": loanId }}});
                        if(deletedloandata['nModified'] > 0)
                        {
                            await models.recurring.find({ 'loanId': loanId }, async function ( recerror, recitem ){
                                if(recerror){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: recerror
                                    });
                                } else {
                                    if(recitem.length > 0) {
                                        console.log("Recurring Loan FOund");
                                        await models.recurring.remove({ 'loanId': loanId }, async function ( delerror, delitem ){
                                            if(delerror) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['405'],
                                                    statuscode: 405,
                                                    details: delerror
                                                });
                                            } else {
                                                await models.student.findOne({ 'studentId': studentId }, async function (newerror, newitem) {
                                                    if(newerror) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['405'],
                                                            statuscode: 405,
                                                            details: newerror
                                                        });
                                                    } else {                                                        
                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                            statuscode: 200,
                                                            details: newitem['loanDetails']
                                                        });
                                                    }
                                                })
                                            }
                                        });
                                    } else {
                                        console.log('Recurring Not found for this loan');
                                        await models.student.findOne({ 'studentId': studentId }, async function (newerror, newitem) {
                                            if(newerror) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['405'],
                                                    statuscode: 405,
                                                    details: newerror
                                                });
                                            } else {                                                        
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: (newitem['loanDetails'])?newitem['loanDetails']:[]
                                                });
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },  
    wallettransaction: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let silaamt = req.body.silaamt ? req.body.silaamt : res.json({
                message: "Please Provide Amount",
                isError: true
            });      
            let loanname = req.body.loanname ? req.body.loanname : res.json({
                message: "Please Provide loanname",
                isError: true
            });
            let loanid = req.body.loanid ? req.body.loanid : res.json({
                message: "Please Provide loanid",
                isError: true
            });
            let loanAccount = req.body.loanAccount ? req.body.loanAccount : res.json({
                message: "Please Provide loan Account Number",
                isError: true
            });            
            let ins = req.body.ins ? req.body.ins : res.json({
                message: "Please Provide instituion id",
                isError: true
            });
            var request = require("request");
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const fboWalletKey = config.fboaccount.walletKey;
                    const fboWalletAddress = config.fboaccount.walletAddress;
                    var amount = parseFloat(silaamt)*100;
                    var userHandle = studentId+".silamoney.eth";
                    var wpk = await decryptKeys(item.walletKey);
                    var msg = "Transfer Funds";
                    var destination = config.fboaccount.handle;
                    var destination_wallet = await decryptKeys(fboWalletKey);
                    var destination_address = await decryptKeys(fboWalletAddress);
                    await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                    .then(async (response) => {
                        var roundedamount = (Math.ceil(parseFloat(silaamt)*10)/10).toFixed(2);
                        var adjustedamount = roundedamount - parseFloat(amount);
                        console.log(response);
                        await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : silaamt, "fullAmount" : roundedamount, "paymentFrom" : "wallet", "bankId" : "NA", "bankName" : 'Sloan Wallet', "accountNum" : 'NA', "accountRouting" : 'NA', "accountType" : 'Sloan Wallet', "accountClass" : 'Sloan', "paymentType" : "loan", "loanId" : loanid, "loanAccount": loanAccount, "frequency": "One Time", "status": "transfer", "loan_ins":ins }, function (perror, pitem) {
                            if(!perror) {
                                res.json({
                                    isError: false,
                                    message: 'Loan Payment Processing',
                                    statuscode: 200,
                                    details: pitem
                                }); 
                            }
                            else
                            {
                                res.json({
                                    isError: true,
                                    message: 'Database Error',
                                    statuscode: 404,
                                    details: perror
                                }); 
                            }
                        })
                    })
                    .catch((err) => { 
                        console.log(err);
                    });
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getPlaidData: async(request, response, next) => {
        try {
            var query = [
                {"$match": {"roundUps.0": { "$gt": {} } } },
                { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
            ];
            await models.student.aggregate(query).exec(async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else 
                {
                    var col = [];
                    var onecol = {};
                    for (var i = 0; i < item.length; i++) {
                        var ac = null;
                        if(item[i].hasOwnProperty("roundUps")) {
                            if(item[i].roundUps.length > 0) {
                                const now = moment();
                                const today = now.format('YYYY-MM-DD');
                                const thirtyDaysAgo = now.subtract(30, 'days').format('YYYY-MM-DD');
                                var dto = {};
                                var ac = item[i].roundUps[0].access_token;
                                await plaidClient.getTransactions(ac, thirtyDaysAgo, today, async(err, res) => {
                                    if(!err) {
                                        if(res.hasOwnProperty('transactions')) {
                                            if(res.transactions.length > 0) {
                                                for(var j = 0; j < res.transactions.length; j++) {
                                                    await models.roundups.find({ "transaction_id" : res.transactions[j].transaction_id }, async function (checkerr, checkresp) {
                                                        if(checkerr) {
                                                            console.log(checkerr);
                                                        } else {
                                                            if(checkresp.length > 0) {
                                                                // console.log();
                                                            } else {
                                                                dto["_id"] = new mongoose.Types.ObjectId();
                                                                dto["flag"] = false;
                                                                dto["studentId"] = item[i].studentId;
                                                                dto["account_id"] = res.transactions[j].account_id;
                                                                dto["accountnum"] = res.transactions[j].account_number;
                                                                dto["account_owner"] =res.transactions[j].account_owner;
                                                                dto["amount"] = res.transactions[j].amount;
                                                                dto["roundedupAmount"] = Math.ceil(res.transactions[j].amount);
                                                                dto["roundup"] = (Math.ceil(res.transactions[j].amount) - res.transactions[j].amount).toFixed(2);
                                                                dto["authorized_date"] = res.transactions[j].authorized_date;
                                                                dto["category"] =res.transactions[j].category;
                                                                dto["category_id"] = res.transactions[j].category_id;
                                                                dto["date"] = res.transactions[j].date;
                                                                dto["iso_currency_code"] = res.transactions[j].iso_currency_code;
                                                                dto["location"] = res.transactions[j].location;
                                                                dto["name"] = res.transactions[j].name;
                                                                dto["payment_channel"] = res.transactions[j].payment_channel;
                                                                dto["payment_meta"] = res.transactions[j].payment_meta;
                                                                dto["pending"] = res.transactions[j].pending;
                                                                dto["pending_transaction_id"] = res.transactions[j].pending_transaction_id;
                                                                dto["transaction_code"] = res.transactions[j].transaction_code;
                                                                dto["transaction_id"] = res.transactions[j].transaction_id;
                                                                dto["transaction_type"] = res.transactions[j].transaction_type;
                                                                dto["unofficial_currency_code"] = res.transactions[j].unofficial_currency_code;
                                                                await models.roundups.create(dto, async function (cerror, cresponse) {
                                                                    if(cerror) {
                                                                       await console.log(cerror);
                                                                    } else {
                                                                        //await console.log(cresponse.studentId);
                                                                    }
                                                                });
                                                            }
                                                        }                                                            
                                                    })                                                    
                                                }
                                            } 
                                        }
                                    }                                    
                                });
                                var agquery = [
                                    { $match : { 
                                        "$and":[
                                            { "studentId" : item[i].studentId },{ "flag": false }
                                        ]} 
                                    },
                                    {
                                        $group:
                                        {
                                            _id: "$studentId",
                                            totalAmount: { $sum: "$roundup" }
                                        }
                                    }
                                ];
                                await models.roundups.aggregate(agquery).exec(async function (terr, tresp) {
                                    if(terr) {
                                       await console.log(terr);
                                    } else {
                                        //await console.log(tresp[0].totalAmount);
                                        if(tresp[0].totalAmount >= 10) {
                                            await roundUpAch(item[i].studentId, item[i].walletKey, item[i].roundUps[0].bankName, tresp[0].roundup, item[i].roundUps[0].accountnum);
                                            //await console.log(item[i].synpaseId, item[i].roundUps[0].synpaseNode, item[i].synpaseNodeId, tresp[0].totalAmount)
                                        } else {
                                            console.log('Less tha $10');
                                        }
                                    }
                                })
                                // console.log(item[i].studentId);
                            }
                        }
                        await new Promise(resolve => setTimeout(resolve, 2000));
                    }
                }
            });
        } catch (error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });

        }
    },

    //TESTING ANGIE TRANSACTIONS
    getPlaidDataTEST: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student Id",
                isError: true
            });
            var query = [
                {"$match": {"studentId": studentId } },
                { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
            ];
            await models.student.aggregate(query).exec(async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else 
                {
                    var col = [];
                    var onecol = {};
                    for (itemsingle of item) {
                        var ac = null;
                        if(itemsingle.hasOwnProperty("roundUps")) {
                            if(itemsingle.roundUps.length > 0) {
                                const now = moment();
                                const today = now.format('YYYY-MM-DD');
                                const thirtyDaysAgo = now.subtract(30, 'days').format('YYYY-MM-DD');
                                var dto = {};
                                var ac = itemsingle.roundUps[0].access_token;
                                await plaidClient.getTransactions(ac, thirtyDaysAgo, today, async(err, res) => {
                                    if(!err) {
                                        if(res.hasOwnProperty('transactions')) {
                                            if(res.transactions.length > 0) {
                                                for(transaction of res.transactions) {
                                                    await console.log(transaction.transaction_id);
                                                    await models.roundups.find({ "transaction_id" : transaction.transaction_id }, async function (checkerr, checkresp) {
                                                        if(checkerr) {
                                                            console.log(checkerr);
                                                        } else {
                                                            if(checkresp.length > 0) {
                                                                // console.log();
                                                            } else {
                                                                dto["_id"] = new mongoose.Types.ObjectId();
                                                                dto["flag"] = false;
                                                                dto["studentId"] = itemsingle.studentId;
                                                                dto["account_id"] = transaction.account_id;
                                                                dto["account_owner"] =transaction.account_owner;
                                                                dto["amount"] = transaction.amount;
                                                                dto["roundedupAmount"] = Math.ceil(transaction.amount);
                                                                dto["roundup"] = (Math.ceil(transaction.amount) - transaction.amount).toFixed(2);
                                                                dto["authorized_date"] = transaction.authorized_date;
                                                                dto["category"] =transaction.category;
                                                                dto["category_id"] = transaction.category_id;
                                                                dto["date"] = transaction.date;
                                                                dto["iso_currency_code"] = transaction.iso_currency_code;
                                                                dto["location"] = transaction.location;
                                                                dto["name"] = transaction.name;
                                                                dto["payment_channel"] = transaction.payment_channel;
                                                                dto["payment_meta"] = transaction.payment_meta;
                                                                dto["pending"] = transaction.pending;
                                                                dto["pending_transaction_id"] = transaction.pending_transaction_id;//pending_transaction_id
                                                                dto["transaction_code"] = transaction.transaction_code;
                                                                dto["transaction_id"] = transaction.transaction_id;
                                                                dto["transaction_type"] = transaction.transaction_type;
                                                                dto["unofficial_currency_code"] = transaction.unofficial_currency_code;
                                                                await models.roundups.create(dto, async function (cerror, cresponse) {
                                                                    if(cerror) {
                                                                       await console.log(cerror);
                                                                    } else {
                                                                        //await console.log(cresponse.studentId);
                                                                    }
                                                                });
                                                            }
                                                        }                                                            
                                                    })                                                   
                                                }
                                            } 
                                        }
                                    }                                    
                                });
                                // console.log(item[i].studentId);
                            }
                        }
                        await new Promise(resolve => setTimeout(resolve, 2000));
                    }
                }
            });
        } catch (error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });

        }
    },




    createLinkToken: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClient.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['liabilities'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'loans',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createLinkTokenSandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClientSandbox.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['liabilities'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'loans',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    
    createLinkTokenBank: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClient.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['auth'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'default',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },

    createLinkTokenBankSandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClientSandbox.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['auth'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'default',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
     createLinkTokenBankVerify: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let access_token = request.body.access_token ? request.body.access_token : response.json({
                message: "Please Provide access_token",
                isError: true
            });
            const linkTokenResponse = await plaidClient.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'default',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
                access_token: access_token
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createLinkTokenBankVerifySandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let access_token = request.body.access_token ? request.body.access_token : response.json({
                message: "Please Provide access_token",
                isError: true
            });
            const linkTokenResponse = await plaidClientSandbox.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'default',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
                access_token: access_token
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createLinkTokenMicro: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClient.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['auth'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'microdeposits',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createLinkTokenMicroSandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            const linkTokenResponse = await plaidClientSandbox.createLinkToken({
                user: {
                  client_user_id: studentId,
                },
                client_name: 'Sloan',
                products: ['auth'],
                country_codes: ['US'],
                language: 'en',
                link_customization_name: 'microdeposits',
                webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
            }, function(error, linkTokenResponse) {
                if(error) {
                    response.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 404,
                        details: error
                    });
                } else {
                    response.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: linkTokenResponse.link_token
                    });
                }
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createMicroDepositToken: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let publicToken = request.body.publicToken ? request.body.publicToken : response.json({
                message: "Please Provide Public Token",
                isError: true
            });
            let bankName = request.body.bankName ? request.body.bankName : response.json({
                message: "Please Provide Bank Name",
                isError: true
            });
            // Obtain an access_token from the Link public_token
            const linkTokenResponse = await plaidClient.exchangePublicToken(publicToken);
            linkTokenResponse['bankDispName'] = bankName;
            linkTokenResponse['type'] = 'Same Day Micro-Deposit';
            console.log(linkTokenResponse);
            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':linkTokenResponse }}); 
            if(updatebankdata) {
                response.json({
                    isError: false,
                    message: "Bank Account Added, Verification Pending",
                    statuscode: 200,
                    details: linkTokenResponse
                });
            }
        }  catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createMicroDepositTokenSandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let publicToken = request.body.publicToken ? request.body.publicToken : response.json({
                message: "Please Provide Public Token",
                isError: true
            });
            let bankName = request.body.bankName ? request.body.bankName : response.json({
                message: "Please Provide Bank Name",
                isError: true
            });
            // Obtain an access_token from the Link public_token
            const linkTokenResponse = await plaidClientSandbox.exchangePublicToken(publicToken);
            linkTokenResponse['bankDispName'] = bankName;
            linkTokenResponse['type'] = 'Same Day Micro-Deposit';
            console.log(linkTokenResponse);
            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':linkTokenResponse }}); 
            if(updatebankdata) {
                response.json({
                    isError: false,
                    message: "Bank Account Added, Verification Pending",
                    statuscode: 200,
                    details: linkTokenResponse
                });
            }
        }  catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createAutoMicroDepositToken: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let publicToken = request.body.publicToken ? request.body.publicToken : response.json({
                message: "Please Provide Public Token",
                isError: true
            });
            let bankName = request.body.bankName ? request.body.bankName : response.json({
                message: "Please Provide Bank Name",
                isError: true
            });
            // Obtain an access_token from the Link public_token
            const linkTokenResponse = await plaidClient.exchangePublicToken(publicToken);
            linkTokenResponse['bankDispName'] = bankName;
            linkTokenResponse['type'] = 'Automated Micro-Deposit';
            console.log(linkTokenResponse);
            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':linkTokenResponse }}); 
            if(updatebankdata) {
                response.json({
                    isError: false,
                    message: "Bank Account Added, Verification Pending",
                    statuscode: 200,
                    details: linkTokenResponse
                });
            }
        }  catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    createAutoMicroDepositTokenSandbox: async(request, response, next) => {
        try {
            let studentId = request.body.studentId ? request.body.studentId : response.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let publicToken = request.body.publicToken ? request.body.publicToken : response.json({
                message: "Please Provide Public Token",
                isError: true
            });
            let bankName = request.body.bankName ? request.body.bankName : response.json({
                message: "Please Provide Bank Name",
                isError: true
            });
            // Obtain an access_token from the Link public_token
            const linkTokenResponse = await plaidClientSandbox.exchangePublicToken(publicToken);
            linkTokenResponse['bankDispName'] = bankName;
            linkTokenResponse['type'] = 'Automated Micro-Deposit';
            console.log(linkTokenResponse);
            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':linkTokenResponse }}); 
            if(updatebankdata) {
                response.json({
                    isError: false,
                    message: "Bank Account Added, Verification Pending",
                    statuscode: 200,
                    details: linkTokenResponse
                });
            }
        }  catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    getLiabilities: async(request, response, next) => {
        try {
            console.log('===================LOAN UPDATES========================');
            let access_token = request.body.access_token ? request.body.access_token : response.json({
                message: "Please Provide Access Token",
                isError: true
            });
            let account_id = request.body.account_id ? request.body.account_id : response.json({
                message: "Please Provide Account Id",
                isError: true
            });
            await updateLoans(access_token, account_id);
            // const loans = await plaidClient.getLiabilities(access_token, async function(err, res) {
            //     console.log(res.accounts);
            // });

        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    getItemDetails: async(request, response, next) => {
        try {
            let access_token = request.body.access_token ? request.body.access_token : response.json({
                message: "Please Provide Access Token",
                isError: true
            });
            let item_id = request.body.item_id ? request.body.item_id : response.json({
                message: "Please Provide Account Id",
                isError: true
            });
            // const loans = await plaidClient.getLiabilities(access_token, async function(err, res) {
            //     console.log(res.accounts);
            // });
            await plaidClient.getItem(access_token, async (err, res) => {
                response.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: res
                });
            });
        } catch(error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
    },
    // manualFees: async(request,response, next) => {
    //     try {
    //         console.log('===================FEES========================');
    //         const walletKey = config.feeaccount.walletKey;
    //         const walletAddress = config.feeaccount.walletAddress;
    //         var userHandle = config.feeaccount.handle;
    //         var wad = await decryptKeys(walletAddress);
    //         var wpk = await decryptKeys(walletKey);
    //         var msg = "Redeem Fees";
    //         var account_name = "Fees_Bank";
    //         await Sila.getSilaBalance(wad)
    //         .then(async (response) => {
    //             console.log(response);
    //             if(response.statusCode == 200 && response.data.sila_balance > 0) {
    //                 var amount = response.data.sila_balance;
    //                 console.log(amount);
    //                 // await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
    //                 // .then(async (response_redeem) => {
    //                 //     console.log(response_redeem);
    //                 // })
    //             }
    //         })
    //         .catch((err) => { 
    //             console.log(err);
    //         });
    //     } catch (error) {
    //         console.log(error);
    //     }
    // },


    //TEST ROUND UP FEES
    // testRoundUps: async(request, response, next) => {
    //     try {
    //         var query = [
    //             {"$match": {"roundUps.0": { "$gt": {} } } },
    //             { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
    //         ];
    //         await models.student.aggregate(query).exec(async function (error, item) {
    //             if (error) {
    //                 res.json({
    //                     isError: true,
    //                     message: errorMsgJSON['ResponseMsg']['405'],
    //                     statuscode: 405,
    //                     details: error
    //                 });
    //             }
    //             else 
    //             {
    //                 var col = [];
    //                 var onecol = {};
    //                 for (var i = 0; i < item.length; i++) {
    //                     var amount = 0.5;
    //                     var silaamt = 50;
    //                     var userHandle = item[i].studentId+'.silamoney.eth';
    //                     var wpk = await decryptKeys(item[i].walletKey);
    //                     var msg = "Round Up Fees";
    //                     var bankName = item[i].roundUps[0].bankName;
    //                     var bankDispName = item[i].roundUps[0].bankDispName;
    //                     console.log("==================ROUND UP FEES===================");
    //                     console.log("Amount:",silaamt);
    //                     console.log("User:",userHandle);
    //                     console.log("Wallet Key:",wpk);
    //                     console.log("Bank Name:",bankName);
    //                     console.log("Message:",msg);
    //                     console.log("================END ROUND UP FEES=================");
    //                     await Sila.issueSila(silaamt, userHandle, wpk, bankName, msg)
    //                     .then(async (issue_res) => { 
    //                         console.log(issue_res);
    //                         //if(issue_res.statuscode == 200) {
    //                             await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : item[i].studentId, "accountNum":bankName, "paymentId" : issue_res.data.transaction_id, "amount" : amount, "paymentFrom" : "bank", "bankId" : bankName, "bankName":bankDispName, "paymentType" : "roundup_fees", "loanId" : null,"loan_ins":null }, async function (error1, item1) {
    //                                 if(error1) {
    //                                     console.log('Error');
    //                                 } else {
    //                                     console.log("ROUND UP FEES CREATED >>>>>>>>>>>>>");
    //                                     console.log(item1);
    //                                 }
    //                             })
    //                         //}
    //                     })
    //                     await new Promise(resolve => setTimeout(resolve, 800));
    //                 }
    //             }
    //         });
    //     } catch (error) {
    //         response.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });

    //     }
    // },
    //END TEST
    //TEST ROUND UPS
    // testRoundUps_BACKEDUP: async(request, response, next) => {
    //     try {
    //         var query = [
    //             {"$match": {"roundUps.0": { "$gt": {} } } },
    //             { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
    //         ];
    //         await models.student.aggregate(query).exec(async function (error, item) {
    //             if (error) {
    //                 res.json({
    //                     isError: true,
    //                     message: errorMsgJSON['ResponseMsg']['405'],
    //                     statuscode: 405,
    //                     details: error
    //                 });
    //             }
    //             else 
    //             {
    //                 var col = [];
    //                 var onecol = {};
    //                 for (var i = 0; i < item.length; i++) {
    //                     var ac = null;
    //                     if(item[i].hasOwnProperty("roundUps")) {
    //                         if(item[i].roundUps.length > 0) {
    //                             const now = moment();
    //                             const today = now.format('YYYY-MM-DD');
    //                             const thirtyDaysAgo = now.subtract(30, 'days').format('YYYY-MM-DD');
    //                             var dto = {};
    //                             var ac = item[i].roundUps[0].access_token;
    //                             await plaidClient.getTransactions(ac, thirtyDaysAgo, today, async(err, res) => {
    //                                 if(!err) {
    //                                     if(res.hasOwnProperty('transactions')) {
    //                                         if(res.transactions.length > 0) {
    //                                             for(var j = 0; j < res.transactions.length; j++) {
    //                                                 await models.roundups.find({ "transaction_id" : res.transactions[j].transaction_id }, async function (checkerr, checkresp) {
    //                                                     if(checkerr) {
    //                                                         console.log(checkerr);
    //                                                     } else {
    //                                                         if(checkresp.length > 0) {
    //                                                             // console.log();
    //                                                         } else {
    //                                                             dto["_id"] = new mongoose.Types.ObjectId();
    //                                                             dto["flag"] = false;
    //                                                             dto["studentId"] = item[i].studentId;
    //                                                             dto["account_id"] = res.transactions[j].account_id;
    //                                                             dto["account_owner"] =res.transactions[j].account_owner;
    //                                                             dto["amount"] = res.transactions[j].amount;
    //                                                             dto["roundedupAmount"] = Math.ceil(res.transactions[j].amount);
    //                                                             dto["roundup"] = (Math.ceil(res.transactions[j].amount) - res.transactions[j].amount).toFixed(2);
    //                                                             dto["authorized_date"] = res.transactions[j].authorized_date;
    //                                                             dto["category"] =res.transactions[j].category;
    //                                                             dto["category_id"] = res.transactions[j].category_id;
    //                                                             dto["date"] = res.transactions[j].date;
    //                                                             dto["iso_currency_code"] = res.transactions[j].iso_currency_code;
    //                                                             dto["location"] = res.transactions[j].location;
    //                                                             dto["name"] = res.transactions[j].name;
    //                                                             dto["payment_channel"] = res.transactions[j].payment_channel;
    //                                                             dto["payment_meta"] = res.transactions[j].payment_meta;
    //                                                             dto["pending"] = res.transactions[j].pending;
    //                                                             dto["pending_transaction_id"] = res.transactions[j].pending_transaction_id;
    //                                                             dto["transaction_code"] = res.transactions[j].transaction_code;
    //                                                             dto["transaction_id"] = res.transactions[j].transaction_id;
    //                                                             dto["transaction_type"] = res.transactions[j].transaction_type;
    //                                                             dto["unofficial_currency_code"] = res.transactions[j].unofficial_currency_code;
    //                                                             await models.roundups.create(dto, async function (cerror, cresponse) {
    //                                                                 if(cerror) {
    //                                                                    await console.log(cerror);
    //                                                                 } else {
    //                                                                     //await console.log(cresponse.studentId);
    //                                                                 }
    //                                                             });
    //                                                         }
    //                                                     }                                                            
    //                                                 })                                                    
    //                                             }
    //                                         } 
    //                                     }
    //                                 }                                    
    //                             });
    //                             var agquery = [
    //                                 { $match : { 
    //                                     "$and":[
    //                                         { "studentId" : item[i].studentId },{ "flag": false }
    //                                     ]} 
    //                                 },
    //                                 {
    //                                     $group:
    //                                     {
    //                                         _id: "$studentId",
    //                                         totalAmount: { $sum: "$roundup" }
    //                                     }
    //                                 }
    //                             ];
    //                             await models.roundups.aggregate(agquery).exec(async function (terr, tresp) {
    //                                 if(terr) {
    //                                    await console.log(terr);
    //                                 } else {
    //                                     await console.log(tresp);
    //                                     if(tresp.length > 0) {
    //                                         if(tresp[0].totalAmount >= 10) {
    //                                             await roundUpAch(item[i].studentId, item[i].walletKey, item[i].roundUps[0].bankName, tresp[0].totalAmount, item[i].roundUps[0].accountNum);
    //                                             //await console.log(item[i].synpaseId, item[i].roundUps[0].synpaseNode, item[i].synpaseNodeId, tresp[0].totalAmount)                                            
    //                                             var per = (parseFloat(tresp[0].totalAmount)/10)/100;
    //                                             global.io.emit('rounduppercentage', { 'percent': per});
    //                                         } else {
    //                                             console.log('Less tha $10');
    //                                             var per = (parseFloat(tresp[0].totalAmount)/10)/100;
    //                                             global.io.emit('rounduppercentage', { 'percent': per});
    //                                         }
    //                                     }
    //                                 }
    //                             })
    //                             // console.log(item[i].studentId);
    //                         }
    //                     }
    //                     await new Promise(resolve => setTimeout(resolve, 2000));
    //                 }
    //             }
    //         });
    //     } catch (error) {
    //         response.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });

    //     }
    // }
    //END TEST
}


// ROUND UPS
cron.schedule('0 */24 * * *', async() => {
    try {
        var query = [
            {"$match": {"roundUps.0": { "$gt": {} } } },
            { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
        ];
        await models.student.aggregate(query).exec(async function (error, item) {
            if (error) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['405'],
                    statuscode: 405,
                    details: error
                });
            }
            else 
            {
                var col = [];
                var onecol = {};
                for (itemsingle of item) {
                    var ac = null;
                    if(itemsingle.hasOwnProperty("roundUps")) {
                        if(itemsingle.roundUps.length > 0) {
                            const now = moment();
                            const today = now.format('YYYY-MM-DD');
                            const thirtyDaysAgo = now.subtract(2, 'days').format('YYYY-MM-DD');
                            var dto = {};
                            var ac = itemsingle.roundUps[0].access_token;
                            await plaidClient.getTransactions(ac, thirtyDaysAgo, today, async(err, res) => {
                                if(!err) {
                                    if(res.hasOwnProperty('transactions')) {
                                        if(res.transactions.length > 0) {
                                            for(transaction of res.transactions) {
                                                await models.roundups.find({ "transaction_id" : transaction.transaction_id }, async function (checkerr, checkresp) {
                                                    if(checkerr) {
                                                        console.log(checkerr);
                                                    } else {
                                                        if(checkresp.length > 0) {
                                                            // console.log();
                                                        } else {
                                                            dto["_id"] = new mongoose.Types.ObjectId();
                                                            dto["flag"] = false;
                                                            dto["studentId"] = itemsingle.studentId;
                                                            dto["account_id"] = transaction.account_id;
                                                            dto["account_owner"] =transaction.account_owner;
                                                            dto["amount"] = transaction.amount;
                                                            dto["roundedupAmount"] = Math.ceil(transaction.amount);
                                                            dto["roundup"] = (Math.ceil(transaction.amount) - transaction.amount).toFixed(2);
                                                            dto["authorized_date"] = transaction.authorized_date;
                                                            dto["category"] =transaction.category;
                                                            dto["category_id"] = transaction.category_id;
                                                            dto["date"] = transaction.date;
                                                            dto["iso_currency_code"] = transaction.iso_currency_code;
                                                            dto["location"] = transaction.location;
                                                            dto["name"] = transaction.name;
                                                            dto["payment_channel"] = transaction.payment_channel;
                                                            dto["payment_meta"] = transaction.payment_meta;
                                                            dto["pending"] = transaction.pending;
                                                            dto["pending_transaction_id"] = transaction.pending_transaction_id;//pending_transaction_id
                                                            dto["transaction_code"] = transaction.transaction_code;
                                                            dto["transaction_id"] = transaction.transaction_id;
                                                            dto["transaction_type"] = transaction.transaction_type;
                                                            dto["unofficial_currency_code"] = transaction.unofficial_currency_code;
                                                            await models.roundups.create(dto, async function (cerror, cresponse) {
                                                                if(cerror) {
                                                                   await console.log(cerror);
                                                                } else {
                                                                    //await console.log(cresponse.studentId);
                                                                }
                                                            });
                                                        }
                                                    }                                                            
                                                })                                                    
                                            }
                                        } 
                                    }
                                }                                    
                            });
                            var agquery = [
                                { $match : { 
                                    "$and":[
                                        { "studentId" : itemsingle.studentId },{ "flag": false }
                                    ]} 
                                },
                                {
                                    $group:
                                    {
                                        _id: "$studentId",
                                        totalAmount: { $sum: "$roundup" }
                                    }
                                }
                            ];
                            await models.roundups.aggregate(agquery).exec(async function (terr, tresp) {
                                if(terr) {
                                   await console.log(terr);
                                } else {
                                    await console.log(tresp);
                                    if(tresp.length > 0) {
                                        if(tresp[0].totalAmount >= 5) {
                                            await roundUpAch(itemsingle.roundUps[0].access_token, itemsingle.studentId, itemsingle.walletKey, itemsingle.roundUps[0].bankDispName, tresp[0].totalAmount, itemsingle.roundUps[0].accountNum);
                                            //await console.log(itemsingle.synpaseId, item[i].roundUps[0].synpaseNode, item[i].synpaseNodeId, tresp[0].totalAmount)                                            
                                            var per = (parseFloat(tresp[0].totalAmount)/5)/100;
                                            global.io.emit('rounduppercentage', { 'percent': per});
                                        } else {
                                            console.log('Less tha $5');
                                            var per = (parseFloat(tresp[0].totalAmount)/5)/100;
                                            global.io.emit('rounduppercentage', { 'percent': per});
                                        }
                                    }
                                }
                            })
                            // console.log(item[i].studentId);
                        }
                    }
                    await new Promise(resolve => setTimeout(resolve, 2000));
                }
            }
        });
    } catch (error) {
        response.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['404'],
            statuscode: 404,
            details: null
        });

    }
});

cron.schedule('0 3 * * *', async() => {
    try {
        var d = moment().format('YYYY-MM-DD');
        await models.recurring.find({}, async function (error, item) {
            if (error) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['405'],
                    statuscode: 405,
                    details: error
                });
            }
            else 
            {
                for(var i = 0; i < item.length; i++) {
                    var id = item[i]["_id"];
                    var amt = item[i]["amount"];
                    var achid = item[i]["bankId"];
                    var bankName = item[i]["bankName"];
                    var accountNum = item[i]["accountNum"];
                    var accountRouting = item[i]["accountRouting"];
                    var accountType = item[i]["accountType"];
                    var accountClass = item[i]["accountClass"];
                    var loanId = item[i]["loanId"];
                    var loanname = item[i]["loanname"];
                    var loanAccount = item[i]["loanAccount"];
                    var frequency = item[i]["frequency"];
                    var ins = item[i]["loan_ins"];
                    var studentId = item[i]["studentId"];
                    if(item[i]["next_payment_date"] == d) {
                        await autoLoanPay(id, studentId, achid, amt, bankName, accountNum, accountRouting, accountType, accountClass, frequency, loanId, loanname, loanAccount, ins );
                    } else {
                        console.log("Wait for It....! Today is : ", d);
                    }
                    await new Promise(resolve => setTimeout(resolve, 1000));    
                }
            }
        });
    } catch (error) {
        response.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['404'],
            statuscode: 404,
            details: null
        });

    }
});

cron.schedule('0 0 * * FRI', async() => {
    try {
        console.log('===================FEES========================');
        const walletKey = config.feeaccount.walletKey;
        const walletAddress = config.feeaccount.walletAddress;
        var userHandle = config.feeaccount.handle;
        var wad = await decryptKeys(walletAddress);
        var wpk = await decryptKeys(walletKey);
        var msg = "Redeem Fees";
        var account_name = "Fees_Bank";
        await Sila.getSilaBalance(wad)
        .then(async (response) => {
            console.log(response);
            if(response.statusCode == 200 && response.data.sila_balance > 0) {
                var amount = response.data.sila_balance;
                await Sila.redeemSila(amount, userHandle, wpk, account_name, msg, config.sila.business_uuid)
                .then(async (response_redeem) => {
                    console.log(response_redeem);
                })
            }
        })
        .catch((err) => { 
            console.log(err);
        });
    } catch (error) {
        console.log(error);
    }
});


// cron.schedule('* * * * *', async() => {
//     try {
//         logger_main.info('Hello, Testing 1 2 3 4 5 6 7 8 9!');
//     } catch (error) {
//         console.log(error);
//     }
// });


// LOAN UPDATES

cron.schedule('0 13 * * *', async() => {
    try {
        console.log('===================LOAN UPDATES========================');
        var query = [
            { $project : {
                "studentId" : 1 ,
                "loanDetails" : 1
            }},
            { $unwind : "$loanDetails" }
        ];
        await models.student.aggregate(query).exec(async function (error, item) {
            if(error) {
                response.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            } else {
                for (var i = 0; i < item.length - 1; i++) {
                    // console.log(item[i]['loanDetails']['access_token'])
                    if(item[i]['loanDetails'].hasOwnProperty("access_token")){
                        console.log(i);
                        console.log(">>>>>>>>>>>>>>");
                        var access_token = item[i]['loanDetails']['access_token'];
                        var account_id = item[i]['loanDetails']['account_id'];

                        await updateLoans(access_token, account_id);
                    }
                }
            }
        });
    } catch (error) {
        console.log(error);
    }
});

//ROUND UP FEES
// cron.schedule('0 0 * * FRI', async() => {
//     try {
//         var query = [
//             {"$match": {"roundUps.0": { "$gt": {} } } },
//             { "$project": {"studentId":1, "roundUps": 1, "synpaseId": 1, "synpaseNodeId": 1, "walletKey": 1}}
//         ];
//         await models.student.aggregate(query).exec(async function (error, item) {
//             if (error) {
//                 res.json({
//                     isError: true,
//                     message: errorMsgJSON['ResponseMsg']['405'],
//                     statuscode: 405,
//                     details: error
//                 });
//             }
//             else 
//             {
//                 var col = [];
//                 var onecol = {};
//                 for (var i = 0; i < item.length; i++) {
//                     var amount = 0.5;
//                     var silaamt = 50;
//                     var userHandle = item[i].studentId+'.silamoney.eth';
//                     var wpk = await decryptKeys(item[i].walletKey);
//                     var msg = "Round Up Fees";
//                     var bankName = item[i].roundUps[0].bankName;
//                     var bankDispName = item[i].roundUps[0].bankDispName;
//                     console.log("==================ROUND UP FEES===================");
//                     console.log("Amount:",silaamt);
//                     console.log("User:",userHandle);
//                     console.log("Wallet Key:",wpk);
//                     console.log("Bank Name:",bankName);
//                     console.log("Message:",msg);
//                     console.log("================END ROUND UP FEES=================");
//                     await Sila.issueSila(silaamt, userHandle, wpk, bankName, msg)
//                     .then(async (issue_res) => { 
//                         console.log(issue_res);
//                         if(issue_res.statusCode == 200) {
//                             await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : item[i].studentId, "accountNum":bankName, "paymentId" : issue_res.data.transaction_id, "amount" : amount, "paymentFrom" : "bank", "bankId" : bankName, "bankName":bankDispName, "paymentType" : "roundup_fees", "loanId" : null,"loan_ins":null }, async function (error1, item1) {
//                                 if(error1) {
//                                     console.log('Error');
//                                 } else {
//                                     console.log("ROUND UP FEES CREATED >>>>>>>>>>>>>");
//                                     console.log(item1);
//                                 }
//                             })
//                         }
//                     })
//                     await new Promise(resolve => setTimeout(resolve, 800));
//                 }
//             }
//         });
//     } catch (error) {
//         response.json({
//             isError: true,
//             message: errorMsgJSON['ResponseMsg']['404'],
//             statuscode: 404,
//             details: null
//         });

//     }
// })

// cron.schedule('* * * * *', async() => {
//     try {
//         var per = Math.floor(Math.random() * 100);
//         console.log(per);
//         global.io.emit('rounduppercentage', { 'percent': per});
//     } catch (error) {
//         console.log(error);
//     }
// });

async function roundUpAch(access_token, studentId, destWalletKey, bankName, amt, acnum) {
    var wpk = await decryptKeys(destWalletKey);
    var amount = (parseFloat(amt)*100);
    var userHandle = studentId+".silamoney.eth";
    var msg = "Round Ups";
    var msg1 = "Round Up Fees";
    const response = await plaidClient.getBalance(access_token).catch((error_balance) => {
        if(error_balance) {
            console.log(error_balance);
        }
    });
    const available_money = response.accounts[0].balances.available;
    var money_check = pasrseFloat(amt) + ( (parseFloat(amt) * 50) / 100 );
    if(available_money > money_check) {
        await Sila.issueSila(amount, userHandle, wpk, acnum, msg, config.sila.business_uuid)
        .then(async (responseiss) => { 
            await models.roundups.update({"studentId": studentId, "flag": false},{ $set: { "flag": true } }, { multi: true }, async function (error, item) {
                if(error) {
                    console.log('Error');
                } else {
                    console.log(responseiss);
                    await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "accountNum":acnum, "paymentId" : responseiss.data.transaction_id, "amount" : amt, "paymentFrom" : "bank", "bankId" : acnum, "bankName":bankName, "accountnum":acnum, "paymentType" : "roundup", "loanId" : null,"loan_ins":null }, async function (error1, item1) {
                        if(error1) {
                            console.log('Error');
                        } else {
                            await Sila.issueSila(50, userHandle, wpk, acnum, msg1, config.sila.business_uuid)
                            .then(async (issue_res) => { 
                                console.log(issue_res);
                                if(issue_res.statusCode == 200) {
                                    await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "accountNum":acnum, "paymentId" : issue_res.data.transaction_id, "amount" : '0.50', "paymentFrom" : "bank", "bankId" : acnum, "bankName":bankName, "paymentType" : "roundup_fees", "loanId" : null,"loan_ins":null }, async function (error2, item2) {
                                        if(error2) {
                                            console.log('Error');
                                        } else {
                                            console.log("ROUND UP FEES CREATED >>>>>>>>>>>>>");
                                            console.log(item2);
                                            logger_main.info('ROUND UP FEES >>>>>>>>> '+item2);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }).catch((err) => { 
            console.log(err);
        });
    } else {
        await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "accountNum":acnum, "paymentId" : responseiss.data.transaction_id, "amount" : amt, "paymentFrom" : "bank", "bankId" : acnum, "bankName":bankName, "accountnum":acnum, "paymentType" : "roundup_FAIL", "loanId" : null,"loan_ins":null }, async function (error1, item1) {
            if(error1) {
                console.log('Error');
            } else {
                console.log('Round Ups has Failed because of low balance in your account');
            }
        })
    }
}


async function autoLoanPay(id, achid, studentId, amt, bankName, accountNum, accountRouting, accountType, accountClass, frequency, loanId, loanname, loanAccount, ins  ) {
    const userHandle = studentId+'.silamoney.eth';
    var amount = parseFloat(amt)*100;
    var msg = "BANK-SILA";
    var d;
    if(frequency == "Weekly") {
        var d = moment().add(7, 'days').format('YYYY-MM-DD');
    } else if(frequency == "Monthly") {
        var d = moment().add(1, 'months').format('YYYY-MM-DD');
    }
    await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
        if (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['405'],
                statuscode: 405,
                details: error
            });
        }
        else {
            var wpk = await decryptKeys(item.walletKey);
            await Sila.issueSila(amount, userHandle, wpk, bankName, msg, config.sila.business_uuid)
            .then(async (response) => { 
                await models.payment.create({ "_id" : new mongoose.Types.ObjectId(), "studentId" : studentId, "paymentId" : response.data.transaction_id, "amount" : amount, "paymentFrom" : "bank", "bankId" : bank_reference, "bankName" : bankName, "accountNum" : accountNum, "accountRouting" : accountRouting, "accountType" : accountType, "accountClass" : accountClass, "paymentType" : "loan", "loanId" : loanid, "loanAccount": loanAccount, "frequency": frequency, "loan_ins":ins }, async function (error, item) {
                    if(!error) {
                        if(frequency != "One Time") {
                            await models.recurring.updateOne({ "_id" :id }, {$set: {"next_payment_date": d }}, async function (rerror, ritem) {
                                if(!rerror) {
                                    console.log("paid");
                                } else {
                                    console.log("error");
                                }
                            })
                        }
                    }
                    else
                    {
                        console.log("db error");
                    }
                })
            })
            .catch((err) => { 
                res.json({
                    isError: true,
                    message: 'Redemtion Failed',
                    statuscode: 404,
                    details: err
                });
            });
        }
    })
}


function emi_calculator(p, r, t) 
{ 
    var emi; 
  
    // one month interest
    r = r / (12 * 100); 
      
    // one month period
    t = t * 12;  
      
    emi = (p * r * Math.pow(1 + r, t)) / (Math.pow(1 + r, t) - 1); 
  
    return (emi); 
} 

// webHooks = new WebHooks({
//     db: {"checkEvents": ["13.56.117.240:3000/webhook"]}, // just an example
// })

// var emitter = webHooks.getEmitter()
 
// emitter.on('*.success', function (shortname, statusCode, body) {
//     console.log('Success on trigger webHook' + shortname + 'with status code', statusCode, 'and body', body)
// })

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}

async function feeAccount(data) {

}


async function updateLoans(access_token, account_id) {
    await plaidClient.getAccounts(access_token, {
      account_ids: [account_id]
    }, async (err, res) => {
        if(res !== undefined) {
            console.log(res.accounts[0].balances);
            await models.student.update({ "loanDetails.account_id": account_id }, { "$set": { "loanDetails.$.account.balances": res.accounts[0].balances } }, async function (updte_error, updte_item) {
                if(updte_error) {
                    console.log(updte_error);
                } else {
                    console.log(updte_item);
                }
            })
        }
    });
}