const mongoose = require('mongoose');
function toLower (v) {
    return v.toLowerCase();
}
var paymentSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    studentId : String,
    paymentId : String,
    amount: {type: Number,default:""},
    fullAmount: {type: Number,default:""},
    paymentFrom:{type: String, default:""},// bank or sloan wallet
    bankId : { type: String, default: ""},
    bankName : { type: String, default: ""},
    accountNum : { type: String, default: ""},
    accountRouting : { type: String, default: ""},
    accountType : { type: String, default: ""},
    accountClass : { type: String, default: ""},
    loanId:{type:String,default:""},
    loanAccount:{type:String,default:""},
    paymentType:{type: String, default:""}, // Loan or Friends
    paymentDetails: [],
    sloanbalance:{type:String,default:""},
    frequency:{type: String,default:""},
    status:{type: String,default:"pending"},
    loan_ins: {type: String,default:""}
},
{
    timestamps: true 
})


const payment = mongoose.model("payment", paymentSchema)

module.exports = payment;