const mongoose = require('mongoose');
function toLower (v) {
    return v.toLowerCase();
}
var studentSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    studentId : String,
    fname : {type:String,default:""},
    lname : {type:String,default:""},
    EmailId:{type: String, set: toLower},
    deviceId:{type: String, default:""},
    pushToken:{type: String, default:""},
    deviceType:{type: String, default:""},
    address:{
        address1:{type:String,default:""},
        address2:{type:String,default:""},
        city:{type:String,default:""},
        state:{type:String,default:""},
        zip:{type:String,default:""},
    },
    Status :{ 
        signedUpVia:String,
        isVerified :{type:Boolean,default:false},
        isNotifyOn:{type:Boolean,default:false},
        isOnline :{type:Boolean,default:false},
        fcmTocken:{type:String,default:""},
        authTocken :{type:String,default:""},
        socialId:{type:String,default:""},
        isKYCVerified:{type:Boolean,default:false}      
    },
    salt:{type:String,default:""},
    password:String,
    dob:{type:String,default:""},
    profilePic :{type:String,default:"http://54.176.67.107/default-avatar.png"},
    mobileNo : {type:String,default:""},
    lastSendOtp:String,
    memberSince:String,
    completionStatus:{type:String,default:"0"},
    walletKey:{type:String,default:""},
    walletAddress:{type:String,default:""},
    walletKeySecond:{type:String,default:""},
    walletAddressSecond:{type:String,default:""},
	customToken:{type:String,default:""},
    stripe_customer_ID: {type:String,default:""},
    stripe_subscription_ID: {type:String,default:""},
    payitoff_borrow_ID: {type:String,default:""},
    payitoff_borrow_UUID: {type:String,default:""},
    loanDetails:[

    ],
    bankAccounts:[

    ],
    roundUps:[

    ],
    school:{type:String,default:""},
    gradschool:{type:String,default:""},
    activities:[
    
    ],
    about:{type:String,default:""},
    synapseACHId:[],
    synapseACHdetails:[],
    sloanbalance:{type:String,default:""},
    roundUpACHlast:{type: String},
    isLoggedIn:{type: Boolean}
},
{
    timestamps: true 
})

studentSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.salt;
    delete obj.password;
    return obj;
}


const student = mongoose.model("student", studentSchema)

module.exports = student;