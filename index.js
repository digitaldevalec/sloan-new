const express = require('express');
const app = express();
var expressWinston = require('express-winston');
const winston = require('winston');
const appRoutes =  require('./routes');
const models = require('./model/common');
const fs = require('fs')
const morgan = require('morgan')

const moment = require('moment');
//morgan.token('robust', function (req, res) { return res.headers })
const path = require('path')
const bodyParser =  require('body-parser');
const dbConnection = require('./model/db-connection');
const config =  require('./configaration/environment');

const server = require('http').createServer(app);
const io = require('socket.io')(server);
global.io = io;
// The event will be called when a client is connected.
// io.on('connection', (socket) => {
//   global.io.emit('testdata', { 'test':'This is a test' });
// });

// // create a write stream (in append mode)
// var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
// // log only 4xx and 5xx responses to console
// app.use(morgan('combined', {
//   skip: function (req, res) { return res.statusCode < 400 }
// }))
// // setup the logger
// app.use(morgan('robust', { stream: accessLogStream }))



expressWinston.requestWhitelist.push('body');
expressWinston.responseWhitelist.push('body');
app.use(expressWinston.logger({
  transports: [
    new winston.transports.File({ filename: 'access.'+moment().format('YYYY_MM_DD')+'.log' })
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  )
}));




app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Content-Type, Authorization');


    if(req.method === 'OPTIONS'){
        res.header("Access-Control-Allow-Methods" , 'PUT, POST, PATCH, DELETE, GET');     
        return res.status(200).json({});   
    }
    next();
});

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', appRoutes);


// express-winston errorLogger makes sense AFTER the router.
app.use(expressWinston.errorLogger({
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' })
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  )
}));




app.use((err, req, res, next)=>{
   console.log(res)
    res.json({
        isError : true,
        message : 'Server Faault',
        statuscode : '500',
        details : res

    });
});

server.listen(config.app.PORT, (req, res)=>{
    console.log('Server is running at port ' + config.app.PORT);
});



const exitHandler = () => {
  if (server) {
    server.close(() => {
      console.log('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  console.log(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  console.log('SIGTERM received');
  if (server) {
    server.close();
  }
});