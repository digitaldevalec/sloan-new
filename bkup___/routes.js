const studentOnboardingRoutes =  require('./endpoint/student-onboarding');
const studentArcusRoutes =  require('./endpoint/student-arcus');
const studentPlaidRoutes =  require('./endpoint/student-plaid');
const studentMainRoutes =  require('./endpoint/student-main');

const express =  require('express')
const appRoutes = express.Router();
appRoutes.use('/onboarding', studentOnboardingRoutes);
appRoutes.use('/arcus', studentArcusRoutes);
appRoutes.use('/plaid', studentPlaidRoutes);
appRoutes.use('/main', studentMainRoutes);
module.exports = appRoutes;