
const studentArcusCtrl = require('../controller/student-arcus');
const express =  require('express');
const checkAuth = require('../middleware/check-auth');
const studentArcusRoutes =  express.Router();
studentArcusRoutes.post('/save-list-institute',  studentArcusCtrl.saveListInstitute);
studentArcusRoutes.post('/get-dummy-institute',  studentArcusCtrl.getDummyInstitute);
studentArcusRoutes.post('/get-institute',studentArcusCtrl.getInstitute);
studentArcusRoutes.post('/save-institute',  studentArcusCtrl.saveInstitute);
studentArcusRoutes.get('/get-realdata',  studentArcusCtrl.getRealData);
studentArcusRoutes.get('/search-institute',  studentArcusCtrl.searchInstitute);
studentArcusRoutes.post('/do-login', studentArcusCtrl.doLogin);
studentArcusRoutes.post('/get-loanBy-userId', studentArcusCtrl.getLoanByUserId);
studentArcusRoutes.post('/getloansratelowest', studentArcusCtrl.getLoanByUserIdLowestRate);
studentArcusRoutes.post('/getloansamounthighest', studentArcusCtrl.getLoanByUserIdHighestAmount);
studentArcusRoutes.post('/getloansamountlowest', studentArcusCtrl.getLoanByUserIdLowestAmount);
studentArcusRoutes.post('/get-loanBy-loanId', studentArcusCtrl.getLoanByLoanId);
studentArcusRoutes.post('/createtransaction', studentArcusCtrl.createNewTransaction);


module.exports = studentArcusRoutes;