
const studentPlaidCtrl = require('../controller/student-plaid');
const express =  require('express');
const checkAuth = require('../middleware/check-auth');
const studentPlaidRoutes =  express.Router();
/**************************************************************
 * Student Plaid Related routes
***************************************************************/
studentPlaidRoutes.post('/save-plaid-data', studentPlaidCtrl.savePlaidData);
studentPlaidRoutes.post('/get_plaid_institutions', studentPlaidCtrl.getInstitutions);
studentPlaidRoutes.post('/get_first_institutions', studentPlaidCtrl.getFirstInstitutions);
studentPlaidRoutes.post('/isRoundUpsOn', studentPlaidCtrl.isRoundUpsOn);
studentPlaidRoutes.post('/isRoundUpsOff', studentPlaidCtrl.isRoundUpsOff);
//studentPlaidRoutes.post('/createbanklogin',studentPlaidCtrl.createBankLogin);
studentPlaidRoutes.post('/saveloandata',studentPlaidCtrl.saveLoanData);

module.exports = studentPlaidRoutes;