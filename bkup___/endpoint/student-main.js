const studentMainCtrl = require('../controller/student-main');
const studentSilaCtrl = require('../controller/student-sila');
const studentGCCtrl = require('../controller/student-giftcard');
const express =  require('express');
const studentMainRoutes =  express.Router();
const checkAuth = require('../middleware/check-auth');
/**************************************************************
 * Student Main Related routes
***************************************************************/
studentMainRoutes.post('/getbanks',studentMainCtrl.getBanks);
studentMainRoutes.post('/addfunds',studentMainCtrl.addfunds);
studentMainRoutes.post('/deletebank',studentMainCtrl.deleteBank);
studentMainRoutes.post('/recommendedlist',studentMainCtrl.recommendedList);
studentMainRoutes.post('/editprofile',studentMainCtrl.editProfile);
studentMainRoutes.post('/addactivity',studentMainCtrl.addActivity);
studentMainRoutes.post('/deleteactivity',studentMainCtrl.deleteActivity);
studentMainRoutes.post('/uploadphoto',studentMainCtrl.uploadPhoto);
studentMainRoutes.post('/getdevicedata',studentMainCtrl.getDeviceData);
studentMainRoutes.post('/keywordsearch',studentMainCtrl.keywordSearch);
studentMainRoutes.post('/requestmoney',studentMainCtrl.requestMoney);
studentMainRoutes.post('/notificationlist',studentMainCtrl.notificationList);
studentMainRoutes.post('/getstudentdata',studentMainCtrl.getStudentData);
studentMainRoutes.post('/notificationchange',studentMainCtrl.notificationChangeState);
studentMainRoutes.post('/paymenthistory',studentMainCtrl.paymentHistory);
studentMainRoutes.post('/userTransactions',studentMainCtrl.userTransactions);
studentMainRoutes.post('/banktransaction',studentMainCtrl.banktransaction);
studentMainRoutes.post('/getAutoPayments',studentMainCtrl.getAutoPayments);
studentMainRoutes.post('/addAutoPayment',studentMainCtrl.addAutoPayment);
studentMainRoutes.post('/deleteAutoPayment',studentMainCtrl.deleteAutoPayment);
studentMainRoutes.post('/editAutoPayment',studentMainCtrl.editAutoPayment);
studentMainRoutes.post('/deleteLoan',studentMainCtrl.deleteLoan);
studentMainRoutes.post('/wallettransaction',studentMainCtrl.wallettransaction);
studentMainRoutes.post('/getplaiddata',studentMainCtrl.getPlaidData);


studentMainRoutes.post('/createLinkToken',studentMainCtrl.createLinkToken);



//Sila Endpoints
studentMainRoutes.post('/webhook_sila',studentSilaCtrl.webHook);


//GIFT CARD ENDPOINTS
studentMainRoutes.get('/getGiftCards',studentGCCtrl.getGiftCards);

module.exports = studentMainRoutes;