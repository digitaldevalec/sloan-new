const express = require('express');
const app = express();

const appRoutes =  require('./routes');
const models = require('./model/common');
const morgan =  require('morgan');
const bodyParser =  require('body-parser');
const dbConnection = require('./model/db-connection');
const config =  require('./configaration/environment');

const server = require('http').createServer(app);
const io = require('socket.io')(server);
global.io = io;
// The event will be called when a client is connected.
io.on('connection', (socket) => {
  console.log('A client just joined on');
});

app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Content-Type, Authorization');


    if(req.method === 'OPTIONS'){
        res.header("Access-Control-Allow-Methods" , 'PUT, POST, PATCH, DELETE, GET');     
        return res.status(200).json({});   
    }
    next();
});

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', appRoutes);

app.use((err, req, res, next)=>{
   console.log(res)
    res.json({
        isError : true,
        message : 'Server Faault',
        statuscode : '500',
        details : res

    });
});
server.listen(config.app.PORT, (req, res)=>{
    console.log('Server is running at port ' + config.app.PORT);
    io.emit('data', { server: 'Arindam' });
});
