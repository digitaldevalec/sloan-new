const mongoose = require('mongoose');
const models = require('../model/common');
const bcrypt = require("bcrypt");
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
//var Request = require("request");
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};

Sila.configure(silaconfig);
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);


const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// var nearest = require('nearest-date')
module.exports = {
     /**
     * @abstract 
     * Signup Via Social
     * According to signedup type collect details of student
     * check if student exists or not in database using email 
     * if exist update student document value with social details other wise insert.
    */
    signupViaSocial:(req, res, next)=>{
        try{
    		let type = "sloanId";
    		//var gloabalvarobj = {};
    		let autouserid;
    		var dx = new Date();
    		var nx = dx.getTime();
    		var customToken = nx;
    		console.log(customToken);
    		AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
    			autouserid = data;
    		})
    		let d = new Date();
    		let timeAnddate = d.toISOString();
    		let signedUpObj = req.body.signedUpObj ? req.body.signedUpObj : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['100'] + " - Social Details " });
    		if (!req.body.signedUpObj) { return; }
    		console.log(signedUpObj);
            // gloabalvarobj.firstnameFromSocial = signedUpObj && signedUpObj.user.familyName ? signedUpObj.user.familyName : '';
            // gloabalvarobj.lastnameFromSocial = signedUpObj && signedUpObj.user.givenName ? signedUpObj.user.givenName : '';
            // gloabalvarobj.emailFromSocial = signedUpObj && signedUpObj.user.email ? signedUpObj.user.email : '';
            // gloabalvarobj.photo = signedUpObj && signedUpObj.user.photo ? signedUpObj.user.photo : '';
            // gloabalvarobj.googleProvidedId = signedUpObj && signedUpObj.user.id ? signedUpObj.user.id : '';
    		var gloabalvarobj = {
    			"firstnameFromSocial":signedUpObj.familyName ? signedUpObj.familyName : signedUpObj.fullName.familyName,
    			"lastnameFromSocial":signedUpObj.givenName ? signedUpObj.givenName : signedUpObj.fullName.givenName,
    			"emailFromSocial":signedUpObj.email ? signedUpObj.email : signedUpObj.email,
    			"photo" : signedUpObj.photo ? signedUpObj.photo : '',
    			"googleProvidedId": signedUpObj.id ? signedUpObj.id : signedUpObj.user,
    			"via": signedUpObj.id ? "GOOGLE" : "APPLE"
    		};
    		console.log(gloabalvarobj);
            let insertquery = {
                '_id': new mongoose.Types.ObjectId(),'studentId':autouserid,'EmailId':gloabalvarobj.emailFromSocial,
                'fname':gloabalvarobj.firstnameFromSocial,'lname':gloabalvarobj.lastnameFromSocial,
                'Status.signedUpVia':gloabalvarobj.via,'Status.socialId':gloabalvarobj.googleProvidedId,
                'profilePic':gloabalvarobj.photo,'memberSince':timeAnddate, 'salt':customToken, 'isLoggedIn': true
            };
            // models.student.findOne({'EmailId':gloabalvarobj.emailFromSocial}, function (error, result) {
            models.student.aggregate([ { $match: { $or: [ { 'EmailId':gloabalvarobj.emailFromSocial }, { 'Status.socialId':gloabalvarobj.googleProvidedId } ] } } ], function (error, result) {
                if(error){
                    res.json({
                        isError: true,
                        message:  "404-1",
                        statuscode:  404,
                        details: error
                    });
                }
                else{
                    console.log(result);
                    if(result.length == 0){
                        models.student.create(insertquery, function (insertedError, insertedResult) {
                            if(insertedError){
                                res.json({
                                    isError: true,
                                    message:  insertedError+"404-2",
                                    statuscode:  404,
                                    details: insertedError
                                });
                            }
                            else{
                                // bcrypt.hash(password, insertedResult.salt, (error, hashedPass) => {
                                    // if(error){
                                        // res.json({
                                            // isError: true,
                                            // message: "404-3",
                                            // statuscode: 404,
                                            // details: error
                                        // });
                                   // }
                                   // else{
                                       // if(insertedResult.password==hashedPass){
                                            const token = token_gen({
                                                'studentId':insertedResult.studentId,
                                                'userType': 'STUDENT'
                                            });
                                            insertedResult.Status.authTocken = token;
                                            insertedResult.totalLoan=insertedResult.loanDetails.length;
                                            let minDue=0;
                                            if(insertedResult.loanDetails.length!==0){
                                                insertedResult.loanDetails.map(a=>{
                                                    minDue=minDue+a.balance;
                                                })
                                            }
                                            insertedResult.minDue=minDue;
                                            let datesArry=[];
                                            insertedResult.loanDetails.map(a=>{
                                                a.payments.map(b=>{
                                                    datesArry.push(b.date) 
                                                })
                                            })
                                            // var target = new Date()
                                            // var index = nearest(datesArry, target.toISOString().split('T')[0])
                                            // console.log(target.toISOString().split('T')[0])
                                           
                                            
                                            // item[0].index=datesArry[index];
                                            // console.log(datesArry)
        
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: insertedResult
                                            });
                                       // }
                                       // else{
                                            // res.json({
                                                // isError: false,
                                                // message: errorMsgJSON['ResponseMsg']['101'],
                                                // statuscode: 101,
                                                // details: null
                                            // });
                                       // }
                                   // }
                                // })
                            }
                        })
                    }
                    else{
    					console.log(result);
                        // bcrypt.hash(password, result.salt, (error, hashedPass) => {
                            // if(error){
                                // res.json({
                                    // isError: true,
                                    // message: "404-4",
                                    // statuscode: 404,
                                    // details: null
                                // });
                           // }
                           // else{
                               // if(result.password==hashedPass){
                                    // const token = token_gen({
                                        // 'studentId':result.studentId,
                                        // 'userType': 'STUDENT'
                                    // });
                                    // result.Status.authTocken = token;
                                    let minDue=0;
    								if ('loanDetails' in result) {
    									if(result.loanDetails.length!==0){
    										result.totalLoan=result.loanDetails.length;
    										result.loanDetails.map(a=>{
    											minDue=minDue+a.balance;
    										})
    									}
    								}
                                    result.minDue=minDue;
                                    let datesArry=[];
    								if ('loanDetails' in result) {
    									result.loanDetails.map(a=>{
    										a.payments.map(b=>{
    											datesArry.push(b.date) 
    										})
    									})
    								}
                                    // var target = new Date()
                                    // var index = nearest(datesArry, target.toISOString().split('T')[0])
                                    // console.log(target.toISOString().split('T')[0])
                                   
                                    
                                    // item[0].index=datesArry[index];
                                    // console.log(datesArry)

                                    res.json({
                                        isError: false,
                                        message: 200,
                                        statuscode: 200,
                                        details: result[0],
                                        subhasistes1:"testing"
                                    });
                               // }
                               // else{
                                    // res.json({
                                        // isError: false,
                                        // message: errorMsgJSON['ResponseMsg']['101'],
                                        // statuscode: 101,
                                        // details: null
                                    // });
                               // }
                           // }
                        // })

                    }
                }
            })
        }
        catch (error) {
            //console.log(error)
            
            res.json({
                isError: true,
                message: "404-catch",
                statuscode: 404,
                details: error
            });
        }
    },
    /**
     * @abstract 
     * Manual Signup
     * check if student exists or not in database using email 
     * if exist show error message else insert new student record.
    */
    manualSignup:(req, res, next)=>{
        try{
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - password",
                isError: true,
            });
			var dx = new Date();
			var nx = dx.getTime();
			var customToken = nx;
			console.log(emailIdTakenFromUser,':',password,':',nx);
            if (!req.body.EmailId || !req.body.password) { return; }
            let emailId=emailIdTakenFromUser.toLowerCase().trim();
            let type = "sloanId";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            let d = new Date();
            let timeAnddate = d.toISOString();
            let findQuery={'EmailId':emailId};
            models.student.findOne(findQuery, function (error, result) {
                if(error){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    console.log(result);
                    if(result == null ){
                        bcrypt.genSalt(10, (err, salt) => {
                            bcrypt.hash(password, salt, (err, hashedPass) => {
                                let insertedValues={
                                    '_id': new mongoose.Types.ObjectId(),'studentId':autouserid,'EmailId':emailId,
                                    'Status.signedUpVia':'MANUAL','salt':salt,'memberSince':timeAnddate,'password':hashedPass,
									'customToken':customToken, 'isLoggedIn': true
                                }
                                 models.student.create(insertedValues, function (insertedError, insertedResult) {
                                    if(insertedError){
                                        res.json({
                                            isError: true,
                                            message:  errorMsgJSON['ResponseMsg']['404'],
                                            statuscode:  404,
                                            details: "INSERT ERRROR"
                                        });
                                    }
                                    else{
                                        bcrypt.hash(password, insertedResult.salt, (error, hashedPass) => {
                                            if(error){
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                           }
                                           else{
                                               if(insertedResult.password==hashedPass){
                                                    const token = token_gen({
                                                        'studentId':insertedResult.studentId,
                                                        'userType': 'STUDENT'
                                                    });
                                                    insertedResult.Status.authTocken = token;
                                                    insertedResult.totalLoan=insertedResult.loanDetails.length;
                                                    let minDue=0;
                                                    if(insertedResult.loanDetails.length!==0){
                                                        insertedResult.loanDetails.map(a=>{
                                                            minDue=minDue+a.balance;
                                                        })
                                                    }
                                                    insertedResult.minDue=minDue;
                                                    let datesArry=[];
                                                    insertedResult.loanDetails.map(a=>{
                                                        a.payments.map(b=>{
                                                            datesArry.push(b.date) 
                                                        })
                                                    });
                                                    
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: insertedResult
                                                    });
                                               }
                                               else{
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['101'],
                                                        statuscode: 101,
                                                        details: null
                                                    });
                                               }
                                           }
                                        })
                                    }
                                })
                            })
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['202'],
                            statuscode: 202,
                            details: null
                        }) 
                    }
                }  
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
   /**
     * @abstract 
     * SIGNIN
     * check if student exists or not in database using email 
     * If exist convert given password to hash,And match it with pre stored hashed password during 
     * Registration. 
    */
   signin: async(req, res, next)=>{
        try{
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - password",
                isError: true,
            });
            if (!req.body.EmailId || !req.body.password) { return; }
            let emailId=emailIdTakenFromUser.toLowerCase().trim();
            let aggreQuery = [{
                $match: { 'EmailId': emailId }
            }];
            await models.student.aggregate(aggreQuery).exec(async (err, item) => {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
               }
               else{
                   if(item.length==0){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['102'],
                            statuscode: 102,
                            details: null
                        });
                   }
                   else{
                        await bcrypt.hash(password, item[0].salt, async (error, hashedPass) => {
                            if(error){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                           }
                           else{
                               if(item[0].password==hashedPass){
                                    const token = token_gen({
                                        'studentId':item[0].studentId,
                                        'userType': 'STUDENT'
                                    });
                                    item[0].Status.authTocken = token;
                                    item[0].totalLoan=item[0].loanDetails.length;
                                    let minDue=0;
                                    item[0].sloanbalance = item[0].sloanbalance;
                                    // if(item[0].loanDetails.length!==0){
                                    //     item[0].loanDetails.map(a=>{
                                    //         minDue=minDue+a.balance;
                                    //     })
                                    // }
                                    item[0].minDue=minDue;
                                    item[0].isLoggedIn=true;
                                    let agquery = [
                                        { $match : { 
                                            "$and":[
                                                { "studentId" : item[0].studentId }, { "paymentType": "roundup" }
                                            ]} 
                                        },
                                        {
                                            $group:
                                            {
                                                _id: "$studentId",
                                                totalAmount: { $sum: "$amount" }
                                            }
                                        }
                                    ];
                                    await models.payment.aggregate(agquery).exec(async (errpay, itempay) => {
                                        if(errpay) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        } else {
                                            if(itempay.length > 0) {
                                                item[0].totalsaved = itempay[0]['totalAmount'];
                                            } else {
                                                item[0].totalsaved = 0;
                                            }
                                            await models.student.updateOne({ 'EmailId': emailId }, { $set: { 'isLoggedIn': true }}, function (err, result){
                                                if(err) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['103'],
                                                        statuscode: 103,
                                                        details: err
                                                    });
                                                } else {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['201'],
                                                        statuscode: 200,
                                                        details: item[0]
                                                    });
                                                }
                                            })
                                        }
                                    });
                                    // let datesArry=[];
                                    // item[0].loanDetails.map(a=>{
                                    //     a.payments.map(b=>{
                                    //         datesArry.push(b.date) 
                                    //     })
                                    // })
                                    // var target = new Date()
                                    // var index = nearest(datesArry, target.toISOString().split('T')[0])
                                    // console.log(target.toISOString().split('T')[0])
                                   
                                    
                                    // item[0].index=datesArry[index];
                                    // console.log(datesArry)
                                    
                                    // res.json({
                                    //     isError: false,
                                    //     message: errorMsgJSON['ResponseMsg']['201'],
                                    //     statuscode: 200,
                                    //     details: item[0]
                                    // });
                               }
                               else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['101'],
                                        statuscode: 101,
                                        details: null
                                    });
                               }
                           }
                        })
                   }
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   },

    logout: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - studentId",
                isError: true,
            });
            models.student.updateOne({'studentId': studentId}, {$set:{ 'isLoggedIn': false }}, async function (err, result){
                if(err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['103'],
                        statuscode: 103,
                        details: err
                    });
                } else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: null
                    });
                }
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
   
   /**
     * @abstract 
     * SIGNIN
     * check if student exists or not in database using email 
     * If exist convert given password to hash,And match it with pre stored hashed password during 
     * Registration. 
    */
   internalLogin: async (req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - studentId",
                isError: true,
            });
            let token = req.body.token ? req.body.token : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - token",
                isError: true,
            });
            if (!req.body.studentId || !req.body.token) { return; }
            var balance;
            //let emailId=emailIdTakenFromUser.toLowerCase().trim();
            let aggreQuery = [{
                $match: { 'studentId': studentId, 'salt': token  }
            }];
            await models.student.aggregate(aggreQuery).exec(async (err, item) => {
               if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
               }
               else{
                   if(item.length==0){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['102'],
                            statuscode: 102,
                            details: null
                        });
                   }
                   else{
						item[0].customToken = token;
						item[0].totalLoan=item[0].loanDetails.length;
                        var wad;
                        if(item[0].walletAddress != '') {
                            wad = await decryptKeys(item[0].walletAddress);
                        }
                        balance = await Sila.getSilaBalance(wad);
						let minDue=0;
						// if(item[0].loanDetails.length!==0){
						//     item[0].loanDetails.map(a=>{
						//         minDue=minDue+a.balance;
						//     })
						// }
                        item[0].minDue = minDue;
						item[0].sloanbalance = parseFloat(balance.data.sila_balance)/100;
                        let agquery = [
                            { $match : { 
                                "$and":[
                                    { "studentId" : studentId }, { "paymentType": "roundup" }
                                ]} 
                            },
                            {
                                $group:
                                {
                                    _id: "$studentId",
                                    totalAmount: { $sum: "$amount" }
                                }
                            }
                        ];
                        await models.payment.aggregate(agquery).exec(async (errpay, itempay) => {
                            if(errpay) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            } else {
                                if(itempay.length > 0) {
                                    item[0].totalsaved = itempay[0]['totalAmount'];
                                } else {
                                    item[0].totalsaved = 0;
                                }
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['201'],
                                    statuscode: 200,
                                    details: item[0]
                                });
                            }
                        });
						// let datesArry=[];
						// item[0].loanDetails.map(a=>{
						//     a.payments.map(b=>{
						//         datesArry.push(b.date) 
						//     })
						// })
						// var target = new Date()
						// var index = nearest(datesArry, target.toISOString().split('T')[0])
						// console.log(target.toISOString().split('T')[0])
					   
						
						// item[0].index=datesArry[index];
						// console.log(datesArry)
                   }
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   },
    /**
     * @abstract 
     * Save Basic Information
     * gather basic information from user and userid. Update information in collection which match
     * the user given id
    */
   saveBasicInfo:(req, res, next)=>{
        try{
          
            let fname = req.body.fname ? req.body.fname : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - First Name",
                isError: true,
            });
            let lname = req.body.lname ? req.body.lname : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Last Name",
                isError: true,
            });
            let dob = req.body.dob ? req.body.dob : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Date of Birth",
                isError: true,
            });

            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
                isError: true,
            });

            console.log(studentId);
            if ( !req.body.fname ||!req.body.lname||!req.body.dob ) { return; } 

            // var studentId = req.body.studentId; 

            models.student.updateOne({ 'studentId': studentId }, { 'fname': fname,'lname':lname,'dob':dob,'completionStatus':"1" }, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                    console.log(updatederror);
                }
                else{
                    if(updatedItem.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['203'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['405'],
                            statuscode: 405,
                            details: null
                        }); 
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
   },
   /**
     * @abstract 
     * Save Student Address
     * gather information of address from user and userid. Update information in collection which match
     * the user given id
    */
    // saveAddress: async(req, res, next)=>{
    //     try{
    //         let address1 = req.body.address1 ? req.body.address1 : res.json({
    //             message: errorMsgJSON['ResponseMsg']['100'] + " -Address One",
    //             isError: true,
    //         });
    //         let address2 = req.body.address2 ? req.body.address2 :""; 
    //         let city = req.body.city ? req.body.city : res.json({
    //             message: errorMsgJSON['ResponseMsg']['100'] + " -City",
    //             isError: true,
    //         }); 
    //         let state = req.body.state ? req.body.state : res.json({
    //             message: errorMsgJSON['ResponseMsg']['100'] + " -State",
    //             isError: true,
    //         }); 
    //         let zip = req.body.zip ? req.body.zip : res.json({
    //             message: errorMsgJSON['ResponseMsg']['100'] + " -Zip",
    //             isError: true,
    //         }); 
    //         let ssn = req.body.ssn ? req.body.ssn : res.json({
    //             message: errorMsgJSON['ResponseMsg']['100'] + " - SSN",
    //             isError: true,
    //         }); 
    //         if ( !req.body.address1 ||!req.body.city||!req.body.state ||!req.body.zip) { return; } 
    //         var studentId = req.body.studentId; 
    //         var getdata = await models.student.findOne({'studentId': studentId});
            
    //         var dob = getdata.dob;
    //         //SILA REGISTER START
    //         var userhandle = studentId+'.silamoney.eth';
    //         const wallet = await Sila.generateWallet();
    //         var wpk = wallet.privateKey;
    //         var wad = wallet.address;
    //         var silauserresponse;
    //         var reqKycResponse;
    //         var checkKycResponse;
    //         var dbresponse;
    //         if(wallet.address) {                
    //             var generatedWallet = {
    //                 address: wallet.address,
    //                 privateKey: wpk,
    //             };
    //             const newuser = new Sila.User();
    //             newuser.handle = userhandle;
    //             newuser.firstName = getdata.fname;
    //             newuser.lastName = getdata.lname;
    //             newuser.address = address1+' '+address2;
    //             newuser.city = city;
    //             newuser.state = state;
    //             newuser.zip = zip;
    //             newuser.phone = getdata.mobileNo;
    //             newuser.email = getdata.EmailId;
    //             newuser.dateOfBirth = dob;
    //             newuser.ssn = ssn;
    //             newuser.cryptoAddress = wallet.address;
    //             silauserresponse = await Sila.register(newuser).then(async () =>{
    //                 console.log(userhandle, wpk);
    //                 reqKycResponse = await Sila.requestKYC(userhandle, wpk);
    //                 console.log(reqKycResponse);
    //             }).then(async () => {
    //                 checkKycResponse = await Sila.checkKYC(userhandle, wpk);
    //                 console.log(checkKycResponse);
    //             }).then(async () => {
    //                 dbresponse = models.student.updateOne({ 'studentId': studentId }, { 'address.address1': address1,'address.address2':address2,'address.city':city,'address.state':state,'address.zip':zip,'completionStatus':"3",'synpaseId':wpk,'synpaseNodeId':wad });
    //             }).catch((err) => {
    //                 console.log(err);
    //             });
    //         }
    //         res.json({
    //             isError: false,
    //             message: 'Checking',
    //             statuscode: 200,
    //             wallet: wallet,
    //             kycrequest: reqKycResponse,
    //             checkkycres: checkKycResponse,
    //             dbresponse: dbresponse
    //         });
    //         // END SILA REGISTER
    //     } catch (error) {
    //         res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });
    //     }
    // },
    saveAddress: async (req, res, next) => {
        try {
            let address1 = req.body.address1 ? req.body.address1 : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Address One",
                isError: true,
            });
            let address2 = req.body.address2 ? req.body.address2 :""; 
            let city = req.body.city ? req.body.city : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -City",
                isError: true,
            }); 
            let state = req.body.state ? req.body.state : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -State",
                isError: true,
            }); 
            let zip = req.body.zip ? req.body.zip : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Zip",
                isError: true,
            });
            if ( !req.body.address1 ||!req.body.city||!req.body.state ||!req.body.zip) { return; } 
            var studentId = req.body.studentId; 
            const wallet = await Sila.generateWallet();
            var wpk1 = wallet.privateKey;
            var wad1 = wallet.address;
            /* Encrypt the data. */
            
            var wpk = await encryptKeys(wpk1,'private_key');
            console.log(wpk);
            var wad = await encryptKeys(wad1,'wallet_address');
            console.log(wad1);

            var getdata = await models.student.findOne({'studentId': studentId});
            
            var dob = getdata.dob;
            
            await models.student.updateOne({ 'studentId': studentId }, { 'address.address1': address1,'address.address2':address2,'address.city':city,'address.state':state,'address.zip':zip,'completionStatus':"4", 'walletKey':wpk,'walletAddress':wad }, function (err, item) {
                if(err) {
                    res.json({
                        isError: true,
                        message: 'Database Update Failed',
                        statuscode: 401,
                        details: err
                    });
                } else {
                    console.log(item);
                    if(item.nModified == 1) {
                        res.json({
                            isError: false,
                            message: 'Database Updated Successfully',
                            statuscode: 200,
                            details: item
                        });
                    } else {
                        res.json({
                            isError: true,
                            message: 'DB update failed',
                            statuscode: 500,
                            details: item
                        });
                    }
                }
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    verifySsn: async (req, res, next) => {
        try {
            let ssn = req.body.ssn ? req.body.ssn : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - SSN",
                isError: true,
            }); 
            if ( !req.body.ssn) { return false; } 
            var studentId = req.body.studentId; 
            var getdata = await models.student.findOne({'studentId': studentId});
            //console.log(getdata);
            var dob = getdata.dob;

            var wpk = await decryptKeys(getdata.walletKey);
            console.log(wpk);
            var wad = await decryptKeys(getdata.walletAddress);
            console.log(wad);

            // var wad = getdata.walletAddress;
            var userhandle = studentId+'.silamoney.eth';
            var silauserresponse;
            var reqKycResponse;
            var checkKycResponse;

            var phone;
            if(getdata.mobileNo.includes('+91')) {
                phone = getdata.mobileNo.substring(3);
            } else if(getdata.mobileNo.includes('+1')) {
                phone = getdata.mobileNo.substring(2);
            }
            console.log(phone);
            const user = new Sila.User();
            user.handle = userhandle;
            user.firstName = getdata.fname;
            user.lastName = getdata.lname;
            user.address = getdata.address.address1;
            user.city = getdata.address.city;
            user.state = getdata.address.state;
            user.zip = getdata.address.zip;
            user.phone = phone;
            user.email = getdata.EmailId;
            user.dateOfBirth = dob;
            user.ssn = ssn;
            user.cryptoAddress = wad;
            console.log(user);
            if(user != undefined) {
                var generatedWallet = {
                    address: wad,
                    privateKey: wpk,
                };
                console.log(generatedWallet);
                await Sila.register(user).then(async () =>{
                    reqKycResponse = await Sila.requestKYC(userhandle, wpk);
                    console.log(reqKycResponse);
                    if(reqKycResponse.statusCode != 200) {
                        res.json({
                            isError: true,
                            message: 'Request KYC Failure',
                            statuscode: 401,
                            details: reqKycResponse
                        });
                    }
                }).then(async () => {
                    checkKycResponse = await Sila.checkKYC(userhandle, wpk);
                    console.log(checkKycResponse);
                    if(checkKycResponse.statusCode != 200) {
                        res.json({
                            isError: true,
                            message: 'Check KYC Failure',
                            statuscode: 401,
                            details: checkKycResponse
                        });
                    }
                }).then(async () => {
                    await models.student.updateOne({ 'studentId': studentId }, { 'completionStatus':"5" }, function (err, item) {
                        if(err) {
                            res.json({
                                isError: true,
                                message: 'Database Update Failed',
                                statuscode: 401,
                                details: err
                            });
                        } else {
                            console.log(item);
                            if(item.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: 'KYC Submitted Successfully',
                                    statuscode: 200,
                                    details: item
                                });
                            } else {
                                res.json({
                                    isError: true,
                                    message: 'DB update failed',
                                    statuscode: 500,
                                    details: item
                                });
                            }
                        }
                    });
                })
                .catch((err) => {
                    console.log(err);
                });
            }
        }
        catch (error) {
            console.log(error);
            res.json({
                isError: true,
                message: 'Verification Error',
                statuscode: 404,
                details: error
            });
        }
    },
    saveAddressBusiness: async(req, res, next)=>{
        try{
            let address1 = req.body.address1 ? req.body.address1 : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Address One",
                isError: true,
            });
            let address2 = req.body.address2 ? req.body.address2 :""; 
            let city = req.body.city ? req.body.city : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -City",
                isError: true,
            }); 
            let state = req.body.state ? req.body.state : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -State",
                isError: true,
            }); 
            let zip = req.body.zip ? req.body.zip : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Zip",
                isError: true,
            }); 
            if ( !req.body.address1 ||!req.body.city||!req.body.state ||!req.body.zip) { return; } 
            var studentId = req.body.studentId; 
            var getdata = await models.student.findOne({'studentId': studentId});
            var dob = getdata.dob;
            var splitdate = dob.split("-");
            var day = splitdate[2];
            var month = splitdate[1];
            var year = splitdate[0];
            var request = require("request");
            var options = {
              'method': 'POST',
              'url': 'https://uat-api.synapsefi.com/v3.1/address-verification',
              'headers': {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({"address_street":address1,"address_city":address2,"address_subdivision":state,"address_country_code":"US","address_postal_code":zip})
            };
            request(options, function (error, response,getbody) { 
                // if(getbody.deliverability == "usps_deliverable") {
                    var options = { 
                        method: 'POST',
                        url: 'https://uat-api.synapsefi.com/v3.1/users',
                        headers: {
                        'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
                        'X-SP-USER-IP': '127.0.0.1',
                        'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
                        'Content-Type': 'application/json'
                        },
                        body: 
                        {
                          "logins": [
                            {
                              "email": getdata.EmailId
                            }
                          ],
                          "phone_numbers": [
                            getdata.mobileNo,
                            getdata.EmailId
                          ],
                          "legal_names": [
                            getdata.fname+' '+getdata.lname    
                          ],
                          "documents":[{
                                "email":getdata.EmailId,
                                "phone_number":getdata.mobileNo,
                                "ip":"13.56.117.240",
                                "name":getdata.fname+' '+getdata.lname,
                                "alias":"",
                                "entity_type":"LLC",
                                "entity_scope":"Not Known",
                                "day":parseInt(day),
                                "month":parseInt(month),
                                "year":parseInt(year),
                                "address_street":address1+' '+address2,
                                "address_city":city,
                                "address_subdivision":state,
                                "address_postal_code":zip,
                                "address_country_code":"US",
                                "virtual_docs":[],
                                "physical_docs":[],
                                "social_docs":[]
                            }],
                            "extra": {
                                "supp_id": studentId,
                                "cip_tag": 1,
                                "is_business": true
                            }
                        },
                        json: true };

                        request(options, function (error, response, body) {
                            if (error){ throw new Error(error); }
                            else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['201'],
                                    statuscode: 200,
                                    details: body
                                });
                                var docid = body.documents[0].id;
                                var options = { method: 'POST',
                                  url: 'https://uat-api.synapsefi.com/v3.1/oauth/'+body._id,
                                  headers: {
                                    'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
                                    'X-SP-USER-IP': '127.0.0.1',
                                    'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
                                    'Content-Type': 'application/json'
                                  },
                                  body: 
                                   {
                                    "refresh_token":body.refresh_token,
                                    "scope":[
                                        "USER|PATCH",
                                        "USER|GET",
                                        "NODES|POST",
                                        "NODES|GET",
                                        "NODE|GET",
                                        "NODE|PATCH",
                                        "NODE|DELETE",
                                        "TRANS|POST",
                                        "TRANS|GET",
                                        "TRAN|GET",
                                        "TRAN|PATCH",
                                        "TRAN|DELETE",
                                        "SUBNETS|GET",
                                        "SUBNETS|POST",
                                        "SUBNET|GET",
                                        "SUBNET|PATCH",
                                        "STATEMENTS|GET",
                                        "STATEMENT|GET"
                                    ]
                                   },
                                  json: true 
                                };
                                request(options, function (error, response, body) {
                                    if (error){ throw new Error(error); }
                                    else { 
                                        var oauth = body.oauth_key;
                                        var options = {
                                          'method': 'POST',
                                          'url': 'https://uat-api.synapsefi.com/v3.1/users/'+body.user_id+'/nodes',
                                          'headers': {
                                            'X-SP-USER-IP': '127.0.0.1',
                                            'X-SP-USER': oauth+'|10a4113e4fab640b2568c70bc9b79d9d',
                                            'Content-Type': 'application/json'
                                          },
                                          body: JSON.stringify({"type":"CUSTODY-US","info":{"nickname":"Custody Account", "document_id":docid}})

                                        }
                                        request(options, function (error, response, body) { 
                                            if (error) throw new Error(error);
                                            else{
                                                var newbody = JSON.parse(body);
                                                // console.log(newbody);
                                                var sid = newbody.nodes[0].user_id;
                                                var nid = newbody.nodes[0]._id;
                                                models.student.updateOne({ 'studentId': studentId }, { 'address.address1': address1,'address.address2':address2,'address.city':city,'address.state':state,'address.zip':zip,'completionStatus':"3",'synpaseId':sid,'synpaseNodeId':nid }, function (updatederror, updatedItem) {
                                                    if(updatederror) {
                                                        console.log(updatederror);
                                                        res.json({
                                                            isError: true,
                                                            message: 'ERROR',
                                                            statuscode: 405,
                                                            details: response
                                                        });
                                                    }
                                                    else{
                                                        if(updatedItem.nModified == 1) {
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['204'],
                                                                statuscode: 200,
                                                                node_id: newbody.nodes[0]._id,
                                                                synapse_id: newbody.nodes[0].user_id,
                                                                details:  JSON.parse(body)
                                                            })
                                                        }
                                                        else{
                                                            console.log('.....2',updatederror)
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['405'],
                                                                statuscode: 405,
                                                                details: response
                                                            }); 
                                                        }
                                                    }
                                                })
                                            }
                                        });
                                    }
                                }); 
                            }
                        }); 
                    })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    isNotifyOn: async(req, res, next)=>{
        try{
          
            let status = req.body.status ? req.body.status : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Notification Status",
                isError: true,
            });
            if ( !req.body.status) { return; }
            console.log(status);
            let notificationStatus;
            if(status == "true"){
                notificationStatus=true;
            }
            else{
                notificationStatus=false;
            }
            var studentId = req.body.studentId;
            await models.student.updateOne({ 'studentId': studentId }, { 'Status.isNotifyOn': notificationStatus}, async function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else{
                    if(updatedItem.nModified == 1) {
                        await models.student.updateOne({ 'studentId': studentId }, { "completionStatus":"7" }, async function (complete_error, complete_item) {
                            if(complete_error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['405'],
                                    statuscode: 405,
                                    details: null
                                });
                            } else {
                                let message=notificationStatus?errorMsgJSON['ResponseMsg']['205']:errorMsgJSON['ResponseMsg']['103']
                                let code=notificationStatus?200:103;
                                res.json({
                                    isError: false,
                                    message: message,
                                    statuscode: code,
                                    details: null
                                });
                            }
                        });
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['405'],
                            statuscode: 405,
                            details: null
                        }); 
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Send Otp
     * collect studentId and mobileNo from user
     * prepare otp code
     * send otp to user given mobile no using twilio
     * after success save otp and studentId in timeToLive collection.
    */
    sendOtp:(req, res, next)=>{
       
        let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -Mobile Number",
            isError: true,
        });
        if (!req.body.mobileNo) { return; } 
        var studentId = req.body.studentId;
        let code = Math.floor((Math.random() * 999999) + 111111);
        let finalcode = code.toString().substring(0, 4)
        sendsmscontroller.Sendsms(mobileNo, finalcode, (callError, callResponse) => {
            if (callError) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['406'],
                    statuscode: 406,
                    details: null
                })
            }
            else{
                models.timeTOLive.findOne({ 'studentId': studentId }, function (searchError, searchResult) {
                if(searchError){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                }
                else{
                    if(searchResult==null){
                            console.log('No Record found in time to live.Its time to insert')
                            let insertedValues={
                                '_id': new mongoose.Types.ObjectId(),'studentId':studentId,
                                'mobileNo':mobileNo,'otp':finalcode
                            }
                            models.timeTOLive.create(insertedValues, function (insertedError, insertedResult) {
                                if(insertedError){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['407'],
                                        statuscode: 407,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['206'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                            })
                    }
                    else{
                            console.log('Record found in time to live.Its time to update')
                            models.timeTOLive.updateOne({ 'studentId': studentId }, { 'otp': finalcode,'mobileNo':mobileNo}, function (updatederror, updatedItem) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: null
                                    });
                                }
                                else{
                                    if(updatedItem.nModified == 1) {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['206'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['405'],
                                            statuscode: 405,
                                            details: null
                                        }); 
                                    }
                                }
                            }) 
                    }
                }
                })
            
            }
        })
    },
   /**
     * @abstract 
     * Verify Otp
     * collect otp,mobileno,studentId from user
     * check if given otp and  studentId exists  in timeToLive collection or not
     * if matched save mobile number in student db and set completionStatus=2 in student db,Else give error message('otp does not match or otp expired')
    */
    verifyOtp:(req, res, next)=>{
       try{
           
            let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Mobile Number",
                isError: true,
            });
            let otp = req.body.otp ? req.body.otp : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Otp",
                isError: true,
            });
            if ( !req.body.mobileNo || !req.body.otp ) { return; } 
            var studentId = req.body.studentId;

            models.timeTOLive.findOne({'studentId':studentId}, function (error, result) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(result==null){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['104'],
                            statuscode: 104,
                            details: null
                        }); 
                    }
                    else{
                        if(result.otp==otp){
                            models.student.updateOne({ 'studentId': studentId }, { 'mobileNo': mobileNo,'completionStatus':3}, function (updatederror, updatedItem) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['405'],
                                        statuscode: 405,
                                        details: null
                                    });
                                }
                                else{
                                    if(updatedItem.nModified == 1){
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['207'],
                                            statuscode: 200,
                                            details: null
                                        });
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['405'],
                                            statuscode: 405,
                                            details: null
                                        });
                                    }
                                }
                            })
                        }
                        else{
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['105'],
                                statuscode: 105,
                                details: null
                            });   
                        }
                    }
                }
            })
       }
       catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    verifyOtpEmail:(req, res, next)=>{
       try{
           
            let email = req.body.email ? req.body.email : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Email ID",
                isError: true,
            });
            let otp = req.body.otp ? req.body.otp : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Otp",
                isError: true,
            });
            if ( !req.body.email || !req.body.otp ) { return; } 
            var studentId = req.body.studentId;

            models.tempOtpContainer.findOne({ 'EmailId': email }, function (error, result) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(result==null){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['104'],
                            statuscode: 104,
                            details: null
                        }); 
                    }
                    else{
                        console.log(result);
                        if(result.otp==otp){
                            res.json({
                                isError: false,
                                message: "Email Is Verified",
                                statuscode: 200,
                                details: null
                            });
                        }
                        else{
                            res.json({
                                isError: true,
                                message: 'Wrong OTP',
                                statuscode: 501,
                                details: null
                            });   
                        }
                    }
                }
            })
       }
       catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Reset Password
     * collect user given studentId and password.convert new password as haspassword and get salt .
     * update old sald and hashed password with new one against given studentId
    */
    resetPassword:(req, res, next)=>{
        try{
            let email = req.body.email ? req.body.email : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Student Id",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Password",
                isError: true,
            });
            if (!req.body.email || !req.body.password) { return; } 
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(password, salt, (err, hashedPass) => {
                    models.student.updateOne({ 'EmailId': email }, { 'password': hashedPass,'salt':salt}, function (updatederror, updatedItem) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: null
                            });
                        }
                        else{
                            if(updatedItem.nModified == 1){
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['208'],
                                    statuscode: 200,
                                    details: null
                                });
                            }
                            else{
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['405'],
                                    statuscode: 405,
                                    details: null
                                });  
                            }
                        }
                    })
                })
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    forgetPassword: async (req, res, next) => {
        try {
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            if (!req.body.EmailId) { return; }
            let emailId = emailIdTakenFromUser.toLowerCase().trim();
            let aggreQuery = [{
                $match: { 'EmailId': emailId }
            }];
            await models.student.aggregate(aggreQuery).exec(async (err, item) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item.length == 0) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['102'],
                            statuscode: 102,
                            details: null
                        });
                    }
                    else {
                        let code = Math.floor((Math.random() * 999999) + 111111);
                        let finalcode = code.toString().substring(0, 4)
                        // simplemailercontroller.viaGmail({ receiver: emailId, subject: '----Otp For Forgot Password----', msg: finalcode }, (mailerErr, nodeMailerResponse) => {
                    	var msg = {
                            to: emailIdTakenFromUser,
                            from: 'app@sloanapp.com',
                            subject: 'OTP for Forget Password',
                            text: finalcode
                        };
                        await sgMail.send(msg).then(async () => {
                            await models.tempOtpContainer.findOne({ 'EmailId': emailId }, async function (error, item) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (item == null) {
                                        console.log('email address does not exist. time to insert')
                                        let insertquery = {
                                            '_id': new mongoose.Types.ObjectId(),
                                            'EmailId': emailId,
                                            'otp': finalcode
                                        }
                                        await models.tempOtpContainer.create(insertquery, async function (err, data) {
                                            if (err) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                    statuscode: 200,
                                                    details: data

                                                })
                                            }
                                        })
                                    }
                                    else {
                                        console.log(' email address exists. time to update')
                                        let querywhere = { 'EmailId': emailId }
                                        let querywith = {
                                            'otp': finalcode
                                        }
                                        await models.tempOtpContainer.updateOne(querywhere, querywith, async function (err, response) {
                                            if (err) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else {
                                                if (response.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: response
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }).catch((e) => {
					        res.json({
                                isError: true,
                                message: 'MAIL ERROR',
                                statuscode: 408,
                                details: e
                            });
					    })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    changePassword: (req, res, next) => {
        try{
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let otp = req.body.otp ? req.body.otp : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Otp",
                isError: true,
            });
            let password = req.body.password ? req.body.password : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Password",
                isError: true,
            });
            if (!req.body.EmailId || !req.body.otp || !req.body.password ) { return; }
            let emailId = emailIdTakenFromUser.toLowerCase().trim();
            models.tempOtpContainer.findOne({ 'EmailId': emailId }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(item==null){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['104'],
                            statuscode: 104,
                            details: null
                        });  
                    }
                    else{
                        if(item.otp==otp){
                            bcrypt.genSalt(10, (err, salt) => {
                                bcrypt.hash(password, salt, (err, hashedPass) => {
                                    models.student.updateOne({ 'EmailId': emailId }, { 'password': hashedPass,'salt':salt}, function (updatederror, updatedItem) {
                                        if (updatederror) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['405'],
                                                statuscode: 405,
                                                details: null
                                            });
                                        }
                                        else{
                                            if(updatedItem.nModified == 1){
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['208'],
                                                    statuscode: 200,
                                                    details: null
                                                });
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['405'],
                                                    statuscode: 405,
                                                    details: null
                                                });  
                                            }
                                        }
                                    })
                                })
                            })
                        }
                        else{
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['105'],
                                statuscode: 105,
                                details: null
                            });  
                        }
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    gotoDashboard: async (req, res, next) => {
        try {
            let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - studentId",
                isError: true,
            });

            if (!req.body.EmailId) { return; }
            //studentId =  req.userData.studentId;
            await models.student.updateOne({ 'EmailId': emailIdTakenFromUser }, { 'completionStatus': "7" }, async function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else {
                    if (updatedItem.nModified == 1) {
                        let emailId = emailIdTakenFromUser.toLowerCase().trim();
                        let aggreQuery = [{
                            $match: { 'EmailId': emailId }
                        }];
                        await models.student.aggregate(aggreQuery).exec( async(err, item) => {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else {
                                if (item.length == 0) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['102'],
                                        statuscode: 102,
                                        details: null
                                    });
                                }
                                else {
                                    const token = token_gen({
                                        'studentId': item[0].studentId,
                                        'userType': 'STUDENT'
                                    });
                                    var wad = await decryptKeys(item[0].walletAddress);
                                    balance = await Sila.getSilaBalance(wad);
                                    item[0].sloanbalance = parseFloat(balance.data.sila_balance)/100;
                                    item[0].Status.authTocken = token;
                                    item[0].totalLoan=item[0].loanDetails.length;
                                    item[0].minDue=0;
                                    let agquery = [
                                        { $match : { 
                                            "$and":[
                                                { "studentId" : studentId }, { "paymentType": "roundup" }
                                            ]} 
                                        },
                                        {
                                            $group:
                                            {
                                                _id: "$studentId",
                                                totalAmount: { $sum: "$amount" }
                                            }
                                        }
                                    ];
                                    await models.payment.aggregate(agquery).exec(async (errpay, itempay) => {
                                        if(errpay) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        } else {
                                            if(itempay.length > 0) {
                                                item[0].totalsaved = itempay[0]['totalAmount'];
                                            } else {
                                                item[0].totalsaved = 0;
                                            }
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['201'],
                                                statuscode: 200,
                                                details: item[0]
                                            });
                                        }
                                    });
                                }
                            }
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['405'],
                            statuscode: 405,
                            details: null
                        });
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
       
    },

    dashboardPage: async(req, res, next) => {
        try {
          /*  let emailIdTakenFromUser = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - Email Address",
                isError: true,
            });
            if (!req.body.EmailId) { return; }*/
            
            var balance = '';
            studentId =  req.body.studentId;
            await models.student.findOne({ 'studentId': studentId }, { loanDetails: { $slice: 3 } }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else {
                    
                    var itemData = item.toJSON();
                    //console.log(item);
                    const token = token_gen({
                        'studentId': item.studentId,
                        'userType': 'STUDENT'
                    });
                    let getbalancearr = '';
                    
                    await models.notifications.find({"studentId": studentId, "isRead": false }, async function (errornoti, itemnoti) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: errornoti
                            });
                        }
                        else {
                            var wad = await decryptKeys(item.walletAddress);
                            balance = await Sila.getSilaBalance(wad);
                            console.log(balance);
                            itemData['activities'] = item.activities;
                            itemData['school'] = item.school;
                            itemData['gradschool'] = item.gradschool;
                            itemData['about'] = item.about;
                            itemData['sloanbalance'] = (balance.statusCode == 200)?(parseFloat(balance.data.sila_balance)/100):'--';
                            itemData['loanbalance'] = 0;
                            itemData['salt'] = item.salt;
                            let totalLoanCount =item.loanDetails.length;
                            let minDue=0;
                            if(itemnoti.length !== 0 ) {
                                itemData['noOfunreadMsg'] = itemnoti.length;
                            } else {
                                itemData['noOfunreadMsg'] = 0;
                            }
                            const loanBillingDates = [];
                            
                            itemData['totalLoan'] = totalLoanCount;
                            if(item.loanDetails.length!==0){
                                item.loanDetails.map(a=>{
                                    minDue=minDue+a.balance;
                                    loanBillingDates.push(a.due_date);
                                })
                                
                                //console.log(loanBillingDates);
                                const now = new Date();

                                let closest = Infinity;

                                loanBillingDates.forEach(function(d) {
                                    const date = new Date(d);

                                    if (date >= now && (date < new Date(closest) || date < closest)) {
                                        closest = d;
                                    }
                                });
                                itemData['minDue'] = minDue;

                                itemData['billingDate'] = closest;
          
                            }else{
                                itemData['minDue'] = 0;

                                itemData['billingDate'] = null;
                            }
                            //itemData['Status']['authTocken'] = token;
                            console.log(itemData);
                            let agquery = [
                                { $match : { 
                                    "$and":[
                                        { "studentId" : studentId }, { "paymentType": "roundup" }
                                    ]} 
                                },
                                {
                                    $group:
                                    {
                                        _id: "$studentId",
                                        totalAmount: { $sum: "$amount" }
                                    }
                                }
                            ];
                            await models.payment.aggregate(agquery).exec(async (errpay, itempay) => {
                                if(errpay) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    if(itempay.length > 0) {
                                        itemData['totalsaved'] = itempay[0]['totalAmount'];
                                    } else {
                                        itemData['totalsaved'] = 0;
                                    }
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['201'],
                                        statuscode: 200,
                                        details: itemData
                                    });
                                }
                            });
                        }
                    })
                }                
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
       
    },
	/**
     * @abstract 
     * Update Student KYC
     * gather information of address from user and userid. Update information in collection which match
     * the user given id
    */
	updateKyc: async(req, res, next)=>{
		try{
			let address1 = req.body.address1 ? req.body.address1 : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -Address One",
				isError: true,
			});
			let address2 = req.body.address2 ? req.body.address2 :""; 
			let city = req.body.city ? req.body.city : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -City",
				isError: true,
			}); 
			let state = req.body.state ? req.body.state : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -State",
				isError: true,
			}); 
			let zip = req.body.zip ? req.body.zip : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -Zip",
				isError: true,
			}); 
			let email = req.body.email ? req.body.email : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " - email",
				isError: true,
			}); 
			let name = req.body.name ? req.body.name : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " - name",
				isError: true,
			}); 
			let phone = req.body.phone ? req.body.phone : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " - phone",
				isError: true,
			}); 
			//if ( !req.body.address1 ||!req.body.city||!req.body.state ||!req.body.zip) { return; } 
			var studentId = req.body.studentId; 
			var getdata = await models.student.findOne({'studentId': studentId});
			var dob = getdata.dob;
			var splitdate = dob.split("-");
			var day = splitdate[2];
			var month = splitdate[1];
			var year = splitdate[0];
			var request = require("request");
			var options = {
			  'method': 'POST',
			  'url': 'https://uat-api.synapsefi.com/v3.1/address-verification',
			  'headers': {
				'Content-Type': 'application/json'
			  },
			  body: JSON.stringify({"address_street":address1,"address_city":address2,"address_subdivision":state,"address_country_code":"US","address_postal_code":zip})
			};
			request(options, function (error, response,getbody) { 
				// if(getbody.deliverability == "usps_deliverable") {
				var options = {
					'method': 'GET',
					'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId+'?full_dehydrate=yes',
					'headers': {
						'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
						'X-SP-USER-IP': '127.0.0.1',
						'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
						'Content-Type': 'application/json'
					}
				};
				request(options, function (error, response, body1) {
					var newbody = JSON.parse(body1);
					var docid = newbody.documents[0].id;
					var rt = newbody.refresh_token;
					if (error) {
						throw new Error(error);
					}
					else
					{
						var options = { method: 'POST',
							url: 'https://uat-api.synapsefi.com/v3.1/oauth/'+getdata.synpaseId,
							headers: {
								'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
								'X-SP-USER-IP': '127.0.0.1',
								'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
								'Content-Type': 'application/json'
							},
							body: 
							{
								"refresh_token":rt,
								"scope":[
									"USER|PATCH",
									"USER|GET",
									"NODES|POST",
									"NODES|GET",
									"NODE|GET",
									"NODE|PATCH",
									"NODE|DELETE",
									"TRANS|POST",
									"TRANS|GET",
									"TRAN|GET",
									"TRAN|PATCH",
									"TRAN|DELETE",
									"SUBNETS|GET",
									"SUBNETS|POST",
									"SUBNET|GET",
									"SUBNET|PATCH",
									"STATEMENTS|GET",
									"STATEMENT|GET"
								]
							},
							json: true 
						}
						//MAIN TRANSACTION
						request(options, function (error, response, body2) {
							var oauth = body2.oauth_key;
							if (error) {
								throw new Error(error);
							}
							else
							{
								var options = {
									'method': 'PATCH',
									'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId,
									'headers': {
										'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
										'X-SP-USER-IP': '127.0.0.1',
										'X-SP-USER': oauth+'|10a4113e4fab640b2568c70bc9b79d9d',
										'Content-Type': 'application/json'
									},
									body: JSON.stringify(
										{
										"update":{
											"login":{
												"email":email
											},
											"remove_login":{
												"email":getdata.EmailId
											},
											"phone_number":phone,
											"remove_phone_number":getdata.mobileNo,
											"legal_name":name,
											"remove_legal_name":getdata.fname+' '+getdata.lname,
											"is_hidden":false
										},
										"documents":[
												{
													"id":docid,
													"email":email,
													"name": name,
													"entity_scope": "Not Known",
													"entity_type": "M",
													"phone_number":phone,
													"address_street": address1+' '+address2 ,
													"address_city": city,
													"address_subdivision": state,
													"address_country_code": "US",
													"address_postal_code": zip
												}
											]
										}
									)
								};
								request(options, function (error, response) { 
									if (error) throw new Error(error);
									res.json({
										isError: false,
										message: "Updated Successfully",
										statuscode: 200,
										details: JSON.parse(response.body)
									});
								});
							}
						})
					}
				})						
            })
        // console.log(body.nodes[0]._id);
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
	},
	deleteKyc: async(req, res, next)=>{
		try{
			let studentId = req.body.studentId ? req.body.studentId : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
				isError: true,
			});  
			var getdata = await models.student.findOne({'studentId': studentId});
			var dob = getdata.dob;
			var splitdate = dob.split("-");
			var day = splitdate[2];
			var month = splitdate[1];
			var year = splitdate[0];
			var request = require("request");
			
			var options = {
				'method': 'GET',
				'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId+'?full_dehydrate=yes',
				'headers': {
					'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
					'X-SP-USER-IP': '127.0.0.1',
					'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
					'Content-Type': 'application/json'
				}
			};
			request(options, function (error, response, body1) {
				var newbody = JSON.parse(body1);
				var docid = newbody.documents[0].id;
				var rt = newbody.refresh_token;
				if (error) {
					throw new Error(error);
				}
				else
				{
					var options = { method: 'POST',
						url: 'https://uat-api.synapsefi.com/v3.1/oauth/'+getdata.synpaseId,
						headers: {
							'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
							'X-SP-USER-IP': '127.0.0.1',
							'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
							'Content-Type': 'application/json'
						},
						body: 
						{
							"refresh_token":rt,
							"scope":[
								"USER|PATCH",
								"USER|GET",
								"NODES|POST",
								"NODES|GET",
								"NODE|GET",
								"NODE|PATCH",
								"NODE|DELETE",
								"TRANS|POST",
								"TRANS|GET",
								"TRAN|GET",
								"TRAN|PATCH",
								"TRAN|DELETE",
								"SUBNETS|GET",
								"SUBNETS|POST",
								"SUBNET|GET",
								"SUBNET|PATCH",
								"STATEMENTS|GET",
								"STATEMENT|GET"
							]
						},
						json: true 
					}
					//MAIN TRANSACTION
					request(options, function (error, response, body2) {
						var oauth = body2.oauth_key;
						if (error) {
							throw new Error(error);
						}
						else
						{
							var options = {
								'method': 'PATCH',
								'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId,
								'headers': {
									'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
									'X-SP-USER-IP': '127.0.0.1',
									'X-SP-USER': oauth+'|10a4113e4fab640b2568c70bc9b79d9d',
									'Content-Type': 'application/json'
								},
								body: JSON.stringify(
									{
									  "documents":[{
										"id":docid,
										"permission_scope":"DELETE_DOCUMENT"
									  }]
									}
								)
							};
							request(options, function (error, response) { 
								if (error) throw new Error(error);
								res.json({
									isError: false,
									message: "Deleted Successfully",
									statuscode: 200,
									details: JSON.parse(response.body)
								});
							});
						}
					})
				}
			})
		}
		catch(error) {
			res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });		   
		}
	},
	updateUserPermission: async(req, res, next)=>{
		try{
			let studentId = req.body.studentId ? req.body.studentId : res.json({
				message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
				isError: true,
			});  
			var getdata = await models.student.findOne({'studentId': studentId});
			var dob = getdata.dob;
			var splitdate = dob.split("-");
			var day = splitdate[2];
			var month = splitdate[1];
			var year = splitdate[0];
			var request = require("request");
			
			var options = {
				'method': 'GET',
				'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId+'?full_dehydrate=yes',
				'headers': {
					'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
					'X-SP-USER-IP': '127.0.0.1',
					'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
					'Content-Type': 'application/json'
				}
			};
			request(options, function (error, response, body1) {
				var newbody = JSON.parse(body1);
				var docid = newbody.documents[0].id;
				var rt = newbody.refresh_token;
				if (error) {
					throw new Error(error);
				}
				else
				{
					var options = { method: 'POST',
						url: 'https://uat-api.synapsefi.com/v3.1/oauth/'+getdata.synpaseId,
						headers: {
							'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
							'X-SP-USER-IP': '127.0.0.1',
							'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
							'Content-Type': 'application/json'
						},
						body: 
						{
							"refresh_token":rt,
							"scope":[
								"USER|PATCH",
								"USER|GET",
								"NODES|POST",
								"NODES|GET",
								"NODE|GET",
								"NODE|PATCH",
								"NODE|DELETE",
								"TRANS|POST",
								"TRANS|GET",
								"TRAN|GET",
								"TRAN|PATCH",
								"TRAN|DELETE",
								"SUBNETS|GET",
								"SUBNETS|POST",
								"SUBNET|GET",
								"SUBNET|PATCH",
								"STATEMENTS|GET",
								"STATEMENT|GET"
							]
						},
						json: true 
					}
					//MAIN TRANSACTION
					request(options, function (error, response, body2) {
						var oauth = body2.oauth_key;
						if (error) {
							throw new Error(error);
						}
						else
						{
							var options = {
								'method': 'PATCH',
								'url': 'https://uat-api.synapsefi.com/v3.1/users/'+getdata.synpaseId,
								'headers': {
									'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
									'X-SP-USER-IP': '127.0.0.1',
									'X-SP-USER': oauth+'|10a4113e4fab640b2568c70bc9b79d9d',
									'Content-Type': 'application/json'
								},
								body: JSON.stringify(
									{
										"permission": "LOCKED",
										"permission_code":"DUPLICATE_ACCOUNT"
									}
								)
							};
							request(options, function (error, response) { 
								if (error) throw new Error(error);
								res.json({
									isError: false,
									message: "Permission Updated Successfully",
									statuscode: 200,
									details: JSON.parse(response.body)
								});
							});
						}
					})
				}
			})
		}
		catch(error) {
			res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });		   
		}
	},
    getPlaidData: async(request, response, next) => {
        try {
            var query = [
                 { "$project": {"studentId":1, "roundUps": 1}}
            ];
            await models.student.aggregate(query).exec(async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else 
                {
                    var col = [];
                    var onecol = {};
                    for (var i = 0; i < item.length; i++) {
                        var ac = null;
                        if(item[i].hasOwnProperty("roundUps")) {
                            if(item[i].roundUps.length > 0) {
                                const now = moment();
                                const today = now.format('YYYY-MM-DD');
                                const thirtyDaysAgo = now.subtract(30, 'days').format('YYYY-MM-DD');
                                var dto = {};
                                var ac = item[i].roundUps[0].access_token;
                                await plaidClient.getTransactions(ac, thirtyDaysAgo, today, async(err, res) => {
                                    if(!err) {
                                        if(res.hasOwnProperty('transactions')) {
                                            if(res.transactions.length > 0) {
                                                for(var j = 0; j < res.transactions.length; j++) {
                                                    dto["flag"] = false;
                                                    dto["studentId"] = item[i].studentId;
                                                    dto["_id"] = new mongoose.Types.ObjectId();
                                                    dto["account_id"] = res.transactions[j].account_id;
                                                    dto["account_owner"] =res.transactions[j].account_owner;
                                                    dto["amount"] = res.transactions[j].amount;
                                                    dto["roundedupAmount"] = Math.ceil(res.transactions[j].amount);
                                                    dto["roundup"] = (Math.ceil(res.transactions[j].amount) - res.transactions[j].amount).toFixed(2);
                                                    dto["authorized_date"] = res.transactions[j].authorized_date;
                                                    dto["category"] =res.transactions[j].category;
                                                    dto["category_id"] = res.transactions[j].category_id;
                                                    dto["date"] = res.transactions[j].date;
                                                    dto["iso_currency_code"] = res.transactions[j].iso_currency_code;
                                                    dto["location"] = res.transactions[j].location;
                                                    dto["name"] = res.transactions[j].name;
                                                    dto["payment_channel"] = res.transactions[j].payment_channel;
                                                    dto["payment_meta"] = res.transactions[j].payment_meta;
                                                    dto["pending"] = res.transactions[j].pending;
                                                    dto["pending_transaction_id"] = res.transactions[j].pending_transaction_id;
                                                    dto["transaction_code"] = res.transactions[j].transaction_code;
                                                    dto["transaction_id"] = res.transactions[j].transaction_id;
                                                    dto["transaction_type"] = res.transactions[j].transaction_type;
                                                    dto["unofficial_currency_code"] = res.transactions[j].unofficial_currency_code;
                                                    await models.roundups.create(dto, async function (cerror, cresponse) {
                                                        if(cerror) {
                                                           await console.log(cerror);
                                                        } else {
                                                            //await console.log(cresponse.studentId);
                                                            var agquery = [
                                                                {
                                                                    $group:
                                                                    {
                                                                        _id: "$id",
                                                                        totalAmount: { $sum: "$roundup" }
                                                                    }
                                                                }
                                                            ];
                                                            await models.roundups.aggregate(agquery).exec(async function (terr, tresp) {
                                                                if(terr) {
                                                                   await console.log(terr);
                                                                } else {
                                                                    await console.log(tresp);
                                                                }
                                                            })
                                                        }
                                                    });
                                                }
                                            } 
                                        }
                                    }                                    
                                });
                            }
                        }
                        await new Promise(resolve => setTimeout(resolve, 2000));
                    }
                }
            });
        } catch (error) {
            response.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });

        }
    },
    checkhandle: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
                isError: true,
            });
            const userHandle = studentId+'.silamoney.eth';
            const resss = await Sila.checkHandle(userHandle);
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss.data
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    getWallet: async(req, res, next)=>{
        try {
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
                isError: true,
            });
            let wpk = req.body.wpk ? req.body.wpk : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -wpk",
                isError: true,
            });
            const userHandle = studentId+'.silamoney.eth';
            const resss = await Sila.getWallet(userHandle, wpk);
            res.json({
                isError: false,
                message: 'Get Wallet',
                statuscode: 200,
                details: resss.data
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    getSilaBalance: async(req, res, next)=>{
        try {
            let wad = req.body.wad ? req.body.wad : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -wad",
                isError: true,
            });
            const resss = await Sila.getSilaBalance(wad);
            res.json({
                isError: false,
                message: 'Get Wallet',
                statuscode: 200,
                details: resss.data
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    registerwithkycsila: async(req, res, next)=>{
        try {
            const wallet = await Sila.generateWallet();
            var resss;
            if(wallet.address) {
                const user = new Sila.User();
                user.handle = 'feesaccountnew.silamoney.eth';
                user.firstName = 'Fees';
                user.lastName = 'Account';
                user.address = '123 Main St';
                user.city = 'Anytown';
                user.state = 'NY';
                user.zip = '12345';
                user.phone = '1234527890';
                user.email = 'feesaccount@digitalindustry.co';
                user.dateOfBirth = '1990-01-01';
                user.ssn = '123456222';
                user.cryptoAddress = wallet.address;
                resss = await Sila.register(user);
            }
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss,
                wallet:wallet
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    registerBusiness: async(req, res, next)=>{
        try {
            const wallet = await Sila.generateWallet();
            var resss;
            if(wallet.address) {
                const user = new Sila.User();
                user.handle = 'arcus.silamoney.eth';
                user.entity_name  = 'Arcus Financial Intelligence, Inc';
                user.address = '214 W 29th street, #1007';
                user.city = 'New York';
                user.state = 'NY';
                user.zip = '10001';
                user.phone = '1-800-917-7198';
                user.ein = '46-2992392';
                user.email = 'contact@arcusfi.com';
                user.business_type = 'llc';
                user.business_website = 'https://www.arcusfi.com';
                user.doing_business_as = 'Arcus Financial Intelligence, Inc';
                user.naics_code = 5419;
                user.cryptoAddress = wallet.address;
                resss = await Sila.register(user);
            }
            res.json({
                isError: false,
                message: 'Business Reg Done',
                statuscode: 200,
                details: resss,
                wallet: wallet
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    getnaics: async(req, res, next)=>{
        try {
            const resss = await Sila.getNacisCategories();
            res.json({
                isError: false,
                message: 'NAICS',
                statuscode: 200,
                details: resss
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    requestkyc: async(req, res, next)=>{
        try {
            let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
                isError: true,
            });
            let userhandle = req.body.userhandle ? req.body.userhandle : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - userhandle",
                isError: true,
            });
            const wpk = walletKey;
            const resss = await Sila.requestKYC(userhandle, wpk);
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss.data
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    checkkyc: async(req, res, next)=>{
        try {
            let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
                isError: true,
            });
            let userhandle = req.body.userhandle ? req.body.userhandle : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - userhandle",
                isError: true,
            });
            const wpk = walletKey;
            const resss = await Sila.checkKYC(userhandle, wpk);
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss.data
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    getEntities: async(req, res, next)=>{
        try {
            const resss = await Sila.getEntities();
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    getEntity: async(req, res, next)=>{
        try {
            let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
                isError: true,
            });
            let userhandle = req.body.userhandle ? req.body.userhandle : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - userhandle",
                isError: true,
            });
            const wpk = walletKey;
            const resss = await Sila.getEntity(userhandle, wpk);
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: resss
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    linkbanksila :async(req, res, next) => {
        let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
            isError: true,
        });
        let studentId = req.body.studentId ? req.body.studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
            isError: true,
        });
        const userHandle = studentId+'.silamoney.eth';
        const wpk = walletKey;
        // Direct account-linking flow (restricted by use-case, contact Sila for approval)
        const response = await Sila.linkAccountDirect(
          userHandle,
          wpk,
          '1234567891',
          '134567892',
          studentId+'_Bank',
        ); // Account Type and Account Name parameters are not required
        res.json({
            isError: false,
            message: 'Checking',
            statuscode: 200,
            details: response
        });
    },
    getbanksSila :async(req, res, next) => {
        let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
            isError: true,
        });
        let studentId = req.body.studentId ? req.body.studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
            isError: true,
        });
        const userHandle = studentId+'.silamoney.eth';
        const wpk = walletKey;
        // Direct account-linking flow (restricted by use-case, contact Sila for approval)
        const response = await Sila.getAccounts(userHandle, walletKey);
        res.json({
            isError: false,
            message: 'Checking',
            statuscode: 200,
            details: response.data
        });
    },
    issueSila :async(req, res, next) => {
        let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
            isError: true,
        });
        let amount = req.body.amount ? req.body.amount : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - Amount",
            isError: true,
        });
        let studentId = req.body.studentId ? req.body.studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
            isError: true,
        });
        let account_name = req.body.account_name ? req.body.account_name : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -account_name",
            isError: true,
        });
        const userHandle = studentId+'.silamoney.eth';
        const wpk = walletKey;
        
        const response = await Sila.issueSila(
          amount,
          userHandle,
          wpk,
          account_name
        );
        res.json({
            isError: false,
            message: 'Checking',
            statuscode: 200,
            details: response.data
        });
    },
    transferSila :async(req, res, next) => {
        let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
            isError: true,
        });
        let amount = req.body.amount ? req.body.amount : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - Amount",
            isError: true,
        });
        let studentId = req.body.studentId ? req.body.studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
            isError: true,
        });
        let destination_wallet = req.body.destination_wallet ? req.body.destination_wallet : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -destination_wallet",
            isError: true,
        });
        let destination_address = req.body.destination_address ? req.body.destination_address : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -destination_address",
            isError: true,
        });
        let descriptor = req.body.descriptor ? req.body.descriptor : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -descriptor",
            isError: true,
        });
        let destination_studentId = req.body.destination_studentId ? req.body.destination_studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -destination_studentId",
            isError: true,
        });
        const userHandle = studentId+'.silamoney.eth';
        const destination = destination_studentId+'.silamoney.eth';
        const wpk = walletKey;
        
        const response = await Sila.transferSila(
          amount,
          userHandle,
          wpk,
          destination,
          destination_wallet,
          destination_address,
          descriptor
        );
        res.json({
            isError: false,
            message: 'Checking',
            statuscode: 200,
            details: response.data
        });
    },
    redeemSila :async(req, res, next) => {
        let walletKey = req.body.walletKey ? req.body.walletKey : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - walletKey",
            isError: true,
        });
        let amount = req.body.amount ? req.body.amount : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " - Amount",
            isError: true,
        });
        let studentId = req.body.studentId ? req.body.studentId : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -studentId",
            isError: true,
        });
        let account_name = req.body.account_name ? req.body.account_name : res.json({
            message: errorMsgJSON['ResponseMsg']['100'] + " -account_name",
            isError: true,
        });
        const userHandle = studentId+'.silamoney.eth';
        const wpk = walletKey;
        const message = "ACH-PAY-LOAN";
        
        await Sila.redeemSila(amount, userHandle, wpk, account_name, message)
        .then((response) => { 
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                details: response.data
            });
        })
        .catch((err) => { 
            res.json({
                isError: true,
                message: 'Redemtion Failed',
                statuscode: 404,
                details: err
            });
        });
    },
    encryptData :async(req, res, next) => {
        try {
            let data = req.body.data ? req.body.data : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - data",
                isError: true,
            });
            const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
            const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
            const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
            const context = {
                stage: 'staging',
                purpose: 'sloan',
                origin: 'us-west-1'
            };
            const { result } = await encrypt(keyring, data, { context });
            res.json({
                isError: false,
                message: 'ENCRYPTED',
                statuscode: 200,
                details: Buffer.from(result).toString("base64"),
            }); 
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
    decryptData :async(req, res, next) => {
        try {
            let data = req.body.data ? req.body.data : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - data",
                isError: true,
            });

            /* Decrypt the data. */
            var decryption_first = await decryptKeys(data);
            console.log(decryption_first);

            /* Grab the encryption context so you can verify it. */
            // const { encryptionContext } = messageHeader;

            /* Verify the encryption context.
            * If you use an algorithm suite with signing,
            * the Encryption SDK adds a name-value pair to the encryption context that contains the public key.
            * Because the encryption context might contain additional key-value pairs,
            * do not add a test that requires that all key-value pairs match.
            * Instead, verify that the key-value pairs you expect match.
            */

            // var encrypted_data_base64 = Buffer.from(result).toString("base64");
            // var decrypted_data_raw = Buffer.from(encrypted_data_base64,"base64");
            //console.log(plaintext);
            // res.json({
            //     isError: false,
            //     message: 'Checking',
            //     statuscode: 200,
            //     plaintext: Buffer.from(plaintext).toString("ascii"),
            //     result: Buffer.from(result).toString("base64"),
            //     original: result,
            //     decrypted: decrypted_data_raw,
            //     cleartext: data,
            //     messageHeader: messageHeader
            // });
            res.json({
                isError: false,
                message: 'Checking',
                statuscode: 200,
                decryption: decryption_first
            });
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },    
    sendMailSendgrid: async(req, res, next)=>{
        try {
            let e_name = req.body.e_name ? req.body.e_name : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " - e_name",
                isError: true,
            });
            var msg = {
                to: 'subhashis.routh@pkweb.in',
                from: 'dev@sloanapp.com',
                templateId: 'd-b6cc5041a1514475a2d4c7f323118818',
                dynamicTemplateData: {
                    name: e_name,
                },
            };
            sgMail.send(msg);
        } catch(e) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: e
            }); 
        }
    },
}


function authcrypto(data){
    var private_key = 'f4d0028da26cc3eb4071a89b453419af113871e53812f0636c57e981f044a2d2';
    // Stringify the message. (The string that gets hashed should be
    // guaranteed to be the same as what is sent in the request.)
    // NOTE: if testing the example strings, you can just declare them as
    // strings, e.g. var message = 'Sila';
    var message = data

    // Generate the message hash using the Keccak 256 algorithm.
    var msg_hash = EthCrypto.hash.keccak256(message);

    // // Create a signature using your private key and the hashed message.
    var signature = EthCrypto.sign(private_key, msg_hash);

    // // The EthCrypto library adds a leading '0x' which should be removed 
    // // from the signature.
    signature = signature.substring(2);

    // The raw message should then be sent in an HTTP request body, and the signature
    // should be sent in a header.
    
    return data;
}

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'production',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}