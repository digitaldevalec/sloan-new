const mongoose = require('mongoose');
const models = require('../model/common');
const bcrypt = require("bcrypt");
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const dummyDataJSON = require('../service/dummyData.json');
var http = require("https");
var Request = require("request");
var hmacSha1 = require("hmac_sha1")
var Base64 = require('js-base64').Base64;
var md5 = require('md5');
var crypto = require('crypto');

module.exports = {
    saveInstitute: async(req, res, next)=>{
        try{
            let instituteDetails = req.body.instituteDetails ? req.body.instituteDetails : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Institute Details",
                isError: true
            });
            let aggrQuery = [
                { '$match': { $or: [
                    {'rpps_biller_id':instituteDetails.rpps_biller_id}
                ]}}
            ]
            models.institute.aggregate(aggrQuery).exec(function(searchErr, searchedItem){
               if(searchErr){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
               }
               else{
                  if(searchedItem.length==0) {
                    models.institute.insertMany(instituteDetails,{ordered: false} , function (err, insertedValue) {
                        if(err){
                            res.json({
                                isError: true,
                                message:  errorMsgJSON['ResponseMsg']['404'],
                                statuscode:  404,
                                details: null
                            });
                        }
                        else{
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: null
                            })
                        }
                    })
                  }
                  else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['209'],
                        statuscode: 209,
                        details: null
                    })
                  }
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    searchInstitute: (req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            let aggrQuery = [
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    '$match': { 'name': { "$regex": serachText, "$options": "i" } }
                },
                
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.institute.aggregate(aggrQuery).exec(function(searchErr, searchedItem){
                if(searchErr){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchedItem[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getInstitute: async(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let aggrQuery = [
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.institute.aggregate(aggrQuery).exec(function(searchErr, searchedItem){
                if(searchErr){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false, //Wed, 01 Aug 2018 17:26:52 GMT
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchedItem[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }

    },
    getRealData: (req, res, next)=>{
        var page = req.query;
        var today = new Date();
        let str = today.toUTCString();
        var name = req.query.name;


        var dateObj=new Date(Math.round((new Date()).getTime()))
        var dateString = dateObj.toGMTString()
        hmac_Sha1 = new hmacSha1('base64');
        // var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`,`application/json,,/billers,${dateString}`)
        var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`,`application/json,,/biller_directory?q[name_cont]=${name},${dateString}`)
        // var base64EncodeString = Base64.encode(sha1EncodedHash)
        // let headers = {
        //     'Authorization': `APIAuth f10ef5e3b31026cda6e199e3e1b6fd21:${sha1EncodedHash}`, 
        //     'Content-MD5': '', //${base64EncodeString}  ${sha1EncodedHash}
        //     'Date': `${dateString}`,
        //     'Content-Type': 'application/json',
        //     'Accept': 'application/vnd.regalii.v3.2+json',
        //     'Sec-Fetch-Mode': 'cors'
        // };
        let headers = {
            'Authorization': `APIAuth c24da1160d5f4563190e5cb6e0103094:${sha1EncodedHash}`, 
            'Content-MD5': '', //${base64EncodeString}  ${sha1EncodedHash}
            'Date': `${dateString}`,
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.regalii.v3.2+json',
            'Sec-Fetch-Mode': 'cors'
        };

        console.log('------------------------------------');
        console.log(dateObj);
        console.log(dateString);
        console.log(sha1EncodedHash);
        // console.log(dateObj);
        console.log('------------------------------------');
       

        Request.get({
            "headers": headers,
            "url": 'https://apix.arcusapi.com/biller_directory?q[name_cont]='+name,
           
        }, (error, response, body) => {
        // Request.get({
        //     "headers": headers,
        //     "url": 'https://apix.staging.arcusapi.com/billers',
           
        // }, (error, response, body) => {
            if (error) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
            if (response) {
                res.json({
                   
                     body:JSON.parse(body)

                })
            }
        })
    },
    saveListInstitute: async(req, res, next)=>{
       
        Request.post({
            "headers": { "content-type": "application/json" },
            "url": `${config.app.FORMATTED_URL}/api/arcus/get-dummy-institute`,
            "body": JSON.stringify({})
        },  (error, response, body) => {
            if(error) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                })
            }
           
          let instituteDb=JSON.parse(body)
         
        models.institute.find({},async function (err, data) {
            var onlyInA = data.filter(comparer(instituteDb.instituteData.billers));
            var onlyInB = instituteDb.instituteData.billers.filter(comparer(data));
            var result = onlyInA.concat(onlyInB);
            if(result.length==0){
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: data
                })
            }
            let newJobArr = await result.map(tt => {
                return {
                    "_id": new mongoose.Types.ObjectId(),
                    "name":tt['name'],
                    "uuid": tt['uuid'],
                    "type": tt['type'],
                    "id":tt['id'],
                    "country": tt['country'],
                    "currency": tt['currency'],
                    "biller_type":  tt['biller_type'],
                    "bill_type": tt['bill_type'],
                    "required_parameters":  tt['required_parameters'],
                    "returned_parameters":  tt['returned_parameters'],
                    "can_migrate": tt['can_migrate'],
                    "has_xdata": tt['has_xdata'],
                    "login_category": null,
                    "charge_user": tt['charge_user']
    
                };
            })
           
                models.institute.insertMany(newJobArr,{ordered: false} , function (err, insertedValue) {
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                
                models.institute.find({},async function (err, searchdata) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchdata
                    })
                })
                
            })
        })
    });
    },
    getLoanByUserId: async (req, res, next)=>{
        try{
            studentId = req.body.studentId ;
            let aggrQuery = [
                {
                    $match:{
                        'studentId' : studentId
                    }
                },
                {
                    $project : {
                    '_id' : 0,
                    "loanDetails":1
                    }
                },
                { $unwind: '$loanDetails' },
                { $sort: { "loanDetails.interest_rate_percentage": -1 } },
                { $group: { "_id":"Rate Percentage Highest", "loanDetails": { $push: "$loanDetails" } } }
            ]
            await models.student.aggregate(aggrQuery).exec(async function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }); 
                }
               else{
                console.log(item[0]);
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item[0]
                })
               }

            })


        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }

    },
    getLoanByUserIdLowestRate:(req, res, next)=>{
        try{
            studentId = req.body.studentId ;
            let aggrQuery = [
                {
                    $match:{
                        'studentId' : studentId
                    }
                },
                {
                    $project : {
                    '_id' : 0,
                    "loanDetails":1
                    }
                },
                { $unwind: '$loanDetails' },
                { $sort: { "loanDetails.interest_rate_percentage": 1 } },
                { $group: { "_id":"Rate Percentage Lowest", "loanDetails": { $push: "$loanDetails" } } }
            ]
            models.student.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }); 
                }
               else{
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item[0]
                })
               }

            })


        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }

    },
    getLoanByUserIdHighestAmount:(req, res, next)=>{
        try{
            studentId = req.body.studentId ;
            let aggrQuery = [
                {
                    $match:{
                        'studentId' : studentId
                    }
                },
                {
                    $project : {
                    '_id' : 0,
                    "loanDetails":1
                    }
                },
                { $unwind: '$loanDetails' },
                { $sort: { "loanDetails.origination_principal_amount": -1 } },
                { $group: { "_id":"Amount Highest", "loanDetails": { $push: "$loanDetails" } } }
            ]
            models.student.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }); 
                }
               else{
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item[0]
                })
               }

            })


        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }

    },
    getLoanByUserIdLowestAmount:(req, res, next)=>{
        try{
            studentId = req.body.studentId ;
            let aggrQuery = [
                {
                    $match:{
                        'studentId' : studentId
                    }
                },
                {
                    $project : {
                    '_id' : 0,
                    "loanDetails":1
                    }
                },
                { $unwind: '$loanDetails' },
                { $sort: { "loanDetails.origination_principal_amount": 1 } },
                { $group: { "_id":"Amount Lowest", "loanDetails": { $push: "$loanDetails" } } }
            ]
            models.student.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }); 
                }
               else{
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item[0]
                })
               }

            })


        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }

    },
    getLoanByLoanId:(req, res, next)=>{
        try{
      
            let biller_uuid = req.body.biller_uuid ? req.body.biller_uuid : res.json({
                message: errorMsgJSON['ResponseMsg']['100'] + " -Biller UUID",
                isError: true,
            });
            
            if ( !req.body.biller_uuid) { return; }
            studentId = req.userData.studentId ;
            let aggrQuery = [
                {
                    $match:{
                        'studentId' : studentId
                    }
                },
                {
                    $project : {
                    '_id' : 0,
                    "loanDetails":1
                    }
                },
                { $unwind: {path:"$loanDetails",preserveNullAndEmptyArrays:false}},
                { '$match':{'loanDetails.biller_uuid':biller_uuid}},
            ]
            models.student.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }); 
                }
               else{
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item[0]
                })
               }

            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            }) //Wed, 01 Aug 2018 17:26:52 GMT
        }
    },
    getDummyInstitute:(req, res, next)=>{
       res.json({
           instituteData:dummyDataJSON['dummyData']
       })
    },
    doLogin: (req, res, next) => {
        var dateObj = new Date(Math.round((new Date()).getTime()))
        var dateString = dateObj.toGMTString()
        hmac_Sha1 = new hmacSha1('base64');
        var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`, `application/json,,/bills,${dateString}`)
        // var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`, `application/json,,/bills,${dateString}`)
        // console.log('------------------------------------');
        // console.log(dateObj);
        // console.log(dateString);
        // console.log(sha1EncodedHash);
        // console.log('------------------------------------');
        var body = {
            login: req.body.login,
            password: req.body.password,
            biller_id: req.body.biller_id,
            external_user_id: req.body.external_user_id
        }
        var url = 'https://apix.staging.arcusapi.com/bills'
        // var url = 'https://apix.arcusapi.com/bills'
        // console.log('------------------------------------');
        //console.log(req.body);
        // console.log(options);
        // console.log('------------------------------------');
        var a = JSON.stringify(body);
        let hash = crypto.createHash('md5').update(a, 'utf8').digest('base64');
        //console.log(sha1EncodedHash)
        let headers1;
        headers1 = {
            'Authorization': `APIAuth f10ef5e3b31026cda6e199e3e1b6fd21:${sha1EncodedHash}`,
            'X-Date': `${dateString}`,
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.regalii.v3.2+json',
            'Sec-Fetch-Mode': 'cors'
        };
            // headers1 = {
            //     'Authorization': `APIAuth c24da1160d5f4563190e5cb6e0103094:${sha1EncodedHash}`,
            //     'X-Date': `${dateString}`,
            //     'Content-Type': 'application/json',
            //     'Accept': 'application/vnd.regalii.v3.2+json',
            //     'Sec-Fetch-Mode': 'cors'
            // };
        var options = {
            json: true,
            url: url,
            method: 'POST',
            headers: headers1,
            body: body,
        }
        //console.log('........', headers1)
        //console.log(options)
        Request(options, (error, response, body) => {
            // res.json({body});
            //console.log('------------------------------------');
            // console.log(error);
            // console.log(response);
            // console.log(body);
           // console.log('------------------------------------');
            if (error) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: body
                });
            }
            if (body) {
                console.log("id",body.id)
                console.log( "type",body.payments)
                console.log(typeof(body.biller_id))

                if(typeof(body.error_code) == null){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['409'],
                        statuscode: 409,
                        details: body
                    });
                }else{
                    models.student.find({ "loanDetails.biller_uuid": body.biller_uuid,"studentId":req.body.external_user_id }, function (err, searchdata) {
                        if (err) {
                            console.log(1)
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: err
                            }); 
                        }
                        else {
                            if (searchdata.length == 0) {
                                
                                let insertedValue = {
                                    "biller_uuid": body.biller_uuid,
                                    "due_date": body.due_date,
                                    "balance": body.balance,
                                    "id": body.id,
                                    "type":body.type,
                                    "biller_id":body.biller_id,
                                    "account_number": body.account_number,
                                    "name_on_account":  body.name_on_account,
                                    "balance_currency": body.balance_currency,
                                    "balance_updated_at":body.balance_updated_at,
                                    "status": body.status,
                                    "payments":body.payments,
                                    "payment_method":body.payment_method
                                }
                                let querywhere = {
                                    "studentId": req.body.external_user_id
                                }
                                models.student.updateOne(querywhere, { $push: { 'loanDetails': insertedValue } }, function (error, response) {
                                    if (error) {
                                        console.log(2)
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: error
                                        });
                                    }
                                    else {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: body
                                        })
                                    }
                                })
                            }
                            else {
                                console.log("Already Exist")
                                let updateWith={
                                    'loanDetails.$.due_date': body.due_date,
                                    'loanDetails.$.balance': body.balance,
                                    'loanDetails.$.id':body.id,
                                    'loanDetails.$.type':body.type,
                                    'loanDetails.$.biller_id':body.biller_id,
                                    'loanDetails.$.account_number':body.account_number,
                                    'loanDetails.$.name_on_account':body.name_on_account,
                                    'loanDetails.$.balance_currency':body.balance_currency,
                                    'loanDetails.$.balance_updated_at':body.balance_updated_at,
                                    'loanDetails.$.status':body.status,
                                    'loanDetails.$.payments':body.payments,
                                    'loanDetails.$.payment_method':body.payment_method,
                                    
                                }
                                console.log("updateWith",updateWith)
                                models.student.updateOne({ "studentId": req.body.external_user_id, "loanDetails.biller_uuid": req.body.biller_id }, { $set: updateWith }, function (error, response) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: error
                                        });
                                    }
                                    else {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: body
                                        })
    
                                    }
                                })
                            }
                        }
                    })
                }

    
            }
        })
    },
    createNewTransaction: (req, res, next)=>{
        var dateObj = new Date(Math.round((new Date()).getTime()))
        var dateString = dateObj.toGMTString()
        hmac_Sha1 = new hmacSha1('base64');
        var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`, `application/json,,/transactions,${dateString}`);
        // var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`, `application/json,,/transactions,${dateString}`)
        var body = {
            login: req.body.login,
            password: req.body.password,
            biller_id: req.body.biller_id,
            external_user_id: req.body.external_user_id
        }
        var url = 'https://apix.staging.arcusapi.com/transactions';
        var a = JSON.stringify(body);
        let hash = crypto.createHash('md5').update(a, 'utf8').digest('base64');
        let headers1;
        headers1 = {
            'Authorization': `APIAuth f10ef5e3b31026cda6e199e3e1b6fd21:${sha1EncodedHash}`,
            'X-Date': `${dateString}`,
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.regalii.v3.2+json',
            'Sec-Fetch-Mode': 'cors'
        };
        var body = {"rpps_biller_id":"5fd1171c-ec7b-42b1-99c5-d07d2d30f6a6","account_number":"1234567890","phone_number":"(714) 202-5081","amount":"2000.0","currency":"USD","payer_born_on":"1983-09-03","first_name":"John","last_name":"Doe","address":{"street":"351 W 14th St Apt L","zip_code":"10014","city":"New York","state":"NY"}};
        var options = {
            json: true,
            url: url,
            method: 'POST',
            headers: headers1,
            body: body,
        }
        
        Request(options, function (error, response, body) { 
            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: response
            })
        })
    },
}
function comparer(otherArray){
    return function(current){
      return otherArray.filter(function(other){
        return other.id == current.id && other.uuid == current.uuid
      }).length == 0;
    }
  }