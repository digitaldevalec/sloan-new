const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs'); 
const moment = require('moment'); 
const path = require('path');
const formidable = require('formidable');
var cron = require('node-cron');
var hmacSha1 = require("hmac_sha1")
var Base64 = require('js-base64').Base64;
var md5 = require('md5');
var crypto = require('crypto');
const plaid = require('plaid');
var bodyParser = require('body-parser');
// const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
const plaidClient = new plaid.Client({
  clientID: config.app.PLAID_CLIENT_ID,
  secret: config.app.PLAID_SECRET,
  env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
// Initialize WebHooks module.
var WebHooks = require('node-webhooks')
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
var http = require("https");
var Request = require("request");
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// const io = require('socket.io')(3111);
// io.on('connection', (socket) => {
//   console.log('A client just joined on');
//   io.emit('data', { some: 'data' });
// });
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);


module.exports = {
    webHook: async(req, res, next)=>{
        try{
            console.log(req.body);
            var eventtype = req.body.event_type;
            var status = req.body.event_details.outcome; 
            var getid = req.body.event_details.entity; 
            var student = getid.replace("id", "Id");
            console.log(student);
            if(eventtype == 'kyc'){
                if( status == 'pending' ) {
                    global.io.emit('kycpending', { 'kyc':req.body });
                } else if(status == 'passed') {
                    global.io.emit('kycverification', { 'kyc':req.body });
                    await models.student.updateOne({ 'studentId': student }, { $set: { "Status.isKYCVerified": true } }, async function (kycerr,kycitem) {
                        if(kycerr) {
                            console.log(kycerr);
                        } else {
                            await models.student.findOne({ 'studentId': student }, async function (studenterror, studentitem) {
                                if (studenterror) {
                                    console.log(studenterror);
                                }
                                else {
                                    // if(studentitem.deviceId != undefined) {
                                    //     var request = require('request');
                                    //     var options = {
                                    //       'method': 'POST',
                                    //       'url': 'https://onesignal.com/api/v1/notifications',
                                    //       'headers': {
                                    //         'Authorization': 'Basic NTdmY2Q3ODEtOTBhOS00YjI3LWJmMzYtN2QzZWIzNWIxYjEy',
                                    //         'Content-Type': 'application/json',
                                    //         'Cookie': '__cfduid=d67bf80b968068dec011c539bddcffeb41590565308'
                                    //       },
                                    //       body: JSON.stringify({"app_id":"f881d3e7-8048-4996-9bb1-4fb23334329b","template_id":"0a130e18-5aca-4886-a523-19570391702e","include_player_ids":[studentitem.deviceId]})

                                    //     };
                                    //     request(options, function (error, response, body) { 
                                    //         if(response)
                                    //          {
                                    //             res.json({
                                    //                 isError: false,
                                    //                 message: 'Success',
                                    //                 statuscode: 200,
                                    //                 details: JSON.parse(body)
                                    //             });
                                    //         }
                                    //         else
                                    //         {
                                    //             res.json({
                                    //                 isError: true,
                                    //                 message: 'Error in One Signal API',
                                    //                 statuscode: 404,
                                    //                 details: response
                                    //             });
                                    //         }
                                    //     });
                                    // }
                                    console.log(studentitem.EmailId);
                                    if(studentitem.EmailId != undefined) {
                                        var msg = {
                                            to: studentitem.EmailId,
                                            from: 'app@sloanapp.com',
                                            templateId: 'd-b6cc5041a1514475a2d4c7f323118818',//'d-363f56ae224746a9833717df4f3c865c'
                                            dynamicTemplateData: {
                                                name: studentitem.fname+' '+studentitem.lname,
                                            },
                                        };
                                        sgMail.send(msg);
                                    }
                                }
                            })
                            console.log(kycitem);
                        }
                    });
                }
            }       
            if(eventtype == "transaction" && status == "success") {
                var studentId = req.body.event_details.entity;
                var tid = req.body.event_details.transaction;
                await models.payment.findOne({ 'paymentId': tid }, async function (error, item) {
                    if(error) {
                        console.log(error);
                    } else {
                        console.log(item.status);
                        if(item.status == "pending") {
                            await models.payment.updateOne({ 'paymentId': tid, "status": "pending" }, { $set: { "status": "transfer" } }, async function (update_error, update_item) {
                                if(update_error) {
                                    console.log(update_error);
                                } else {
                                    await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                        if(get_error) {
                                            console.log(get_error);
                                        } else {
                                            var amount = parseFloat(item.amount)*100;
                                            var userHandle = item.studentId+".silamoney.eth";
                                            var wpk = get_item.walletKey;
                                            var msg = "Transfer Funds";
                                            var destination = "alecfbonew.silamoney.eth";
                                			var destination_wallet = "0xe688caf486fc734151753451aaf6362ee4d3cb0d7b6ca6c33b8619c52cbcd952"; // 
                                			var destination_address = "0x1Cd5FeB95A95951481d571E68EB7D179e29c44cd"; // 
                                            var id = item._id;
                                            await Sila.transferSila(amount, userHandle, wpk, destination, destination_wallet, destination_address, msg)
                                            .then(async (response) => {
                                                await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                    if(trans_error) {
                                                        console.log(trans_error);
                                                    } else {
                                                        console.log(trans_item);
                                                    }
                                                });
                                            })
                                            .catch((err) => { 
                                                console.log(err);
                                            });
                                        }
                                    });
                                }
                            });
                        } 
                        if(item.status == "transfer") {
                            await models.payment.updateOne({ 'paymentId': tid, "status": "transfer" }, { $set: { "status": "redeem" } }, async function (update_error, update_item) {
                                if(update_error) {
                                    console.log(update_error);
                                } else {
                                    await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                        if(get_error) {
                                            console.log(get_error);
                                        } else {
                                            var amount = parseFloat(item.amount)*100;
                                            var userHandle = "alecfbonew.silamoney.eth";
                                            var wpk = "0xe688caf486fc734151753451aaf6362ee4d3cb0d7b6ca6c33b8619c52cbcd952";
                                            var msg = "Redeem Funds";
                                            var account_name = "alecfbonew_Bank";
                                            var id = item._id;
                                            await Sila.redeemSila(amount, userHandle, wpk, account_name, msg)
                                            .then(async (response) => {
                                                await models.payment.updateOne({ '_id': id }, { $set: { "paymentId": response.data.transaction_id } }, async function (trans_error, trans_item) {
                                                    if(trans_error) {
                                                        console.log(trans_error);
                                                    } else {
                                                        console.log(trans_item);
                                                    }
                                                });
                                            })
                                            .catch((err) => { 
                                                console.log(err);
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        if(item.status == "redeem") {
                            await models.payment.updateOne({ 'paymentId': tid, "status": "redeem" }, { $set: { "status": "success" } }, async function (update_error, update_item) {
                                if(update_error) {
                                    console.log(update_error);
                                } else {
                                    await models.student.findOne({ 'studentId': item.studentId }, async function (get_error, get_item) {
                                        await models.institute.findOne({ 'ins_id': item.loan_ins }, async function (get_errorx, get_itemx) {
                                            var dateObj = new Date(Math.round((new Date()).getTime()))
                                            var dateString = dateObj.toGMTString()
                                            hmac_Sha1 = new hmacSha1('base64');
                                            var sha1EncodedHash = hmac_Sha1.digest(`lC2oSx/+/8qJXvep1Qi2K1wEXLjQgF8/8nmU1QVm3a6ygCYcknZmbnRiT6UivjpC4xe1KoUmaxxWc93rXj0vLQ==`, `application/json,,/transactions,${dateString}`);
                                            // var sha1EncodedHash = hmac_Sha1.digest(`GvtN2vXQs6e5L1J1iUYZCaS3eeIlI6gjKkWAy/8B2c0luAzz39AQnjkPDPxqgQFFWtmoi5FGSp+d6xMau6Yjuw==`, `application/json,,/transactions,${dateString}`)
                                            var body = {
                                                login: req.body.login,
                                                password: req.body.password,
                                                biller_id: req.body.biller_id,
                                                external_user_id: req.body.external_user_id
                                            }
                                            var url = 'https://apix.staging.arcusapi.com/transactions';
                                            var a = JSON.stringify(body);
                                            let hash = crypto.createHash('md5').update(a, 'utf8').digest('base64');
                                            let headers1;
                                            headers1 = {
                                                'Authorization': `APIAuth f10ef5e3b31026cda6e199e3e1b6fd21:${sha1EncodedHash}`,
                                                'X-Date': `${dateString}`,
                                                'Content-Type': 'application/json',
                                                'Accept': 'application/vnd.regalii.v3.2+json',
                                                'Sec-Fetch-Mode': 'cors'
                                            };
                                            var postdta = {
                                              "rpps_biller_id": get_itemx.rpps_biller_id,
                                              "account_number": item.loanAccount,
                                              "phone_number": get_item.mobileNo,
                                              "amount": item.amount,
                                              "currency": "USD",
                                              "payer_born_on": get_item.dob,
                                              "first_name": get_item.fname,
                                              "last_name": get_item.lname,
                                              "address": {
                                                "street": get_item.address.address1,
                                                "zip_code": get_item.address.zip,
                                                "city": get_item.address.city,
                                                "state": get_item.address.state
                                              }
                                            };
                                            var options = {
                                                json: true,
                                                url: url,
                                                method: 'POST',
                                                headers: headers1,
                                                body: postdta,
                                            }
                                            Request(options, function (error, response, body) { 
                                                console.log(response.body);
                                            })
                                        })
                                    })
                                }
                            });
                        }
                    }
                });
            }
            console.log(req.body);
            res.status(200).end();
        } catch(e) {
            console.log('Error',e);
        }
    }
}

// webHooks = new WebHooks({
//     db: {"checkEvents": ["13.56.117.240:3000/webhook"]}, // just an example
// })

// var emitter = webHooks.getEmitter()
 
// emitter.on('*.success', function (shortname, statusCode, body) {
//     console.log('Success on trigger webHook' + shortname + 'with status code', statusCode, 'and body', body)
// })

async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}