const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const fs = require('fs');
const moment = require('moment');
const path = require('path');
const formidable = require('formidable');
var cron = require('node-cron');
const plaid = require('plaid');
var bodyParser = require('body-parser');
//const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
//LIVE PLAID
const plaidClient = new plaid.Client({
    clientID: config.app.PLAID_CLIENT_ID,
    secret: config.app.PLAID_SECRET,
    env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
    handle: config.sila.handle,
    key: config.sila.key
};
Sila.configure(silaconfig);
Sila.disableSandbox();
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');
// Initialize WebHooks module.
var WebHooks = require('node-webhooks')

var http = require("https");
var Request = require("request");
// SENDGRID EMAIL SETUP
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.app.SGAPI);

module.exports = {
    useGiftCard: async(req, res, next)=>{
        try{
            let studentId = req.body.studentId ? req.body.studentId : res.json({
                message: "Please Provide Student ID",
                isError: true
            });
            let giftcard = req.body.studentId ? req.body.giftcard : res.json({
                message: "Please Provide GIFT CARD",
                isError: true
            });
            models.student.updateOne({ 'studentId': studentId }, { $set:{'giftcard':giftcard} }, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 200,
                        details: "Gift Card Used Successfully"
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },
    getGiftCards: async(req, res, next)=>{
        try{
            models.giftcards.find({}, function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else
                {
                    res.json({
                        isError: true,
                        message: "Available Gift Cards",
                        statuscode: 200,
                        details: item
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: 'API Error'
            });
        }
    },
}