const mongoose = require('mongoose');
const models = require('../model/common');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const errorMsgJSON = require('../service/errors.json');
const sendsmscontroller = require('../service/TwilioSms');
const token_gen = require('../service/jwtTokenGenerator');
const config =  require('../configaration/environment');
const simplemailercontroller = require('../service/mailer');
const plaid = require('plaid');
const bodyParser = require('body-parser');
// const plaidClient = new plaid.Client(config.app.PLAID_CLIENT_ID, config.app.PLAID_SECRET, config.app.PUBLIC_KEY, plaid.environments.sandbox, {version: '2019-05-29'});
//LIVE PLAID
const plaidClient = new plaid.Client({
  clientID: config.app.PLAID_CLIENT_ID,
  secret: config.app.PLAID_SECRET,
  env: plaid.environments.production
});
var ACCESS_TOKEN = null;
var PUBLIC_TOKEN = null;
var http = require("https");
const EthCrypto = require('eth-crypto');
const Sila = require('sila-sdk').default;
const silaconfig = {
  handle: config.sila.handle,
  key: config.sila.key
};
Sila.configure(silaconfig);
const { KmsKeyringNode, encrypt, decrypt } = require('@aws-crypto/client-node');


module.exports = {
    savePlaidData: async(req, res, next)=>{
        try{
            let account_num = req.body.account_num;
            let account_routing = req.body.account_routing;
            let account_name = req.body.account_name;
            let account_type = req.body.account_type;
            let account_class = req.body.account_class;
            let studentId = req.body.studentId;
            let actoken = req.body.access_token;
            var request = require("request");
            await models.student.findOne({ 'studentId': studentId }, async function (error, item) {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: error
                    });
                }
                else {
                    const userHandle = studentId+'.silamoney.eth';
                    var wpk = await decryptKeys(item.walletKey);
                    const getallbanks = await Sila.getAccounts(userHandle, wpk);
                    console.log(getallbanks);
                    const allbanks = getallbanks.data;
                    bankobj = allbanks.find(o => o.account_name === account_num);
                    if(bankobj == undefined) {
                        var banks = item.bankAccounts;
                        let obj = banks.find(o => o.accountNum === account_num);
                        if(obj == undefined) {
                            //console.log(wpk);
                            // Direct account-linking flow (restricted by use-case, contact Sila for approval)
                            const response = await Sila.linkAccountDirect(
                              userHandle,
                              wpk,
                              account_num,
                              account_routing,
                              account_num,
                              account_class
                            )
                            console.log(response);
                            if(response.statusCode == 200) {
                                var achaccount = { 'bankacc':response.data.reference, 'roundup': 'false' };
                                var bankdetails = { 'reference': response.data.reference, 'bankName': account_num, 'bankDispName':account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                                //var achaccountdetails = newbody.nodes[0].info;
                                //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                                //if(updatedata) {
                                var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                                if(updatebankdata) {
                                    res.json({
                                        isError: false,
                                        message: "Bank Account Added successfully",
                                        statuscode: 200,
                                        details: bankdetails
                                    });
                                }
                            }
                            else
                            {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['500'],
                                    statuscode: 500,
                                    details: 'Bank Add Failed. Problem Linking Bank to SILA'
                                });
                            }
                        } else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['403'],
                                statuscode: 403,
                                details: 'Bank Already Exists'
                            });
                        }
                    } else {
                        var banks = item.bankAccounts;
                        let obj = banks.find(o => o.accountNum === account_num);
                        if(obj == undefined) {
                            var achaccount = { 'bankacc':account_num, 'roundup': 'false' };
                            var bankdetails = { 'reference': account_num, 'bankDispName':account_name, 'bankName': account_num, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
                            //var achaccountdetails = newbody.nodes[0].info;
                            //var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
                            //if(updatedata) {
                            var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
                            if(updatebankdata) {
                                res.json({
                                    isError: false,
                                    message: "Bank Account Added successfully!",
                                    statuscode: 200,
                                    details: bankdetails
                                });
                            }
                        } else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['403'],
                                statuscode: 403,
                                details: 'Bank Already Exists '
                            });
                        }
                    }
                }
            })
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    isRoundUpsOn:(req, res, next)=>{
        try{
            let accountNum = req.body.accountNum;
            let accountRouting = req.body.accountRouting;
            let bankName = req.body.bankName;
            let bankDispName = req.body.bankDispName;
            let accountType = req.body.accountType;
            let accountClass = req.body.accountClass;
            let syn = req.body.account_class;
            let studentId = req.body.studentId;
            let reference = req.body.reference;
            let access_token = req.body.access_token;

            var data = {
                "reference" : reference,
                "bankName" : bankName,
                "bankDispName" : bankDispName,
                "accountNum" : accountNum,
                "accountRouting" : accountRouting,
                "accountType" : accountType,
                "accountClass" : accountClass,
                "access_token" : access_token,
                "roundup" : true
            };
          
            // let status = req.body.status ? req.body.status : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Notification Status",
            //     isError: true,
            // });
            // let bankacc = req.body.bankacc ? req.body.bankacc : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Bank Account",
            //     isError: true,
            // });
            if ( !req.body.studentId) { return; }
            // let notificationStatus;
            // if(status == "true"){
            //     notificationStatus=true;
            // }
            // else{
            //     notificationStatus=false;
            // }
            // studentId = req.body.studentId;
            models.student.updateOne({ 'studentId': studentId }, {$set:{ 'roundUps': data }}, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else{
                    models.student.findOne({ 'studentId': studentId }, function (error, item) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: "Round Ups Is Turned On",
                                statusCode: 200,
                                details: item
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    isRoundUpsOff:(req, res, next)=>{
        try{
            let studentId = req.body.studentId;
          
            // let status = req.body.status ? req.body.status : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Notification Status",
            //     isError: true,
            // });
            // let bankacc = req.body.bankacc ? req.body.bankacc : res.json({
            //     message: errorMsgJSON['ResponseMsg']['100'] + " - Bank Account",
            //     isError: true,
            // });
            if ( !req.body.studentId) { return; }
            // let notificationStatus;
            // if(status == "true"){
            //     notificationStatus=true;
            // }
            // else{
            //     notificationStatus=false;
            // }
            // studentId = req.body.studentId;
            models.student.updateOne({ 'studentId': studentId }, {$pull:{ 'roundUps': {'roundup':true} }}, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['405'],
                        statuscode: 405,
                        details: null
                    });
                }
                else{
                    models.student.findOne({ 'studentId': studentId }, function (error, item) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['405'],
                                statuscode: 405,
                                details: error
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: "Round Ups Is Turned Off",
                                statusCode: 200,
                                details: null
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getInstitutions: async(req, res, next)=>{
        try {
            var count = 50;
            var offset = 0;
            // Pull institutions
            plaidClient.getInstitutions(count, offset, (err, result) => {
                // Handle err
                //const institutions = result.institutions;
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: result
                });
            });
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });

        }
    },
    getFirstInstitutions: async(req, res, next)=>{
        try{
            var request = require('request');
            var count = req.body.count ? req.body.count : 200;
            var options = {
              'method': 'POST',
              'url': 'https://sandbox.plaid.com/institutions/get',
              'headers': {
                'Content-Type': 'application/json',
                'User-Agent': 'Plaid Postman'
              },
              body: JSON.stringify({"client_id":"5d1fc9f84fa1190016b491a3","secret":"465fe994f2595d372976e04d544ec5","count":count,"offset":0})

            };

            request(options, function (err, resp) { 
                if (err) {
                    console.log("ERROR",resp);
                }
                else {
                    var receiveddata = JSON.parse(resp.body);
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        data: receiveddata
                    });
                }
            });
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    saveLoanData: async(req, res, next)=>{
        try{
            var request = require('request');
            let loandata = req.body.loandata;
            let studentId = req.body.studentId;
            models.student.updateOne({ 'studentId': studentId }, { $push: {"loanDetails":loandata }}, function (updatederror, updatedItem) {
                if (updatederror) {
                    res.json({
                        isError: true,
                        message: "Error",
                        statuscode: 405,
                        details: null
                    });
                    console.log(updatederror);
                }
                else{
                    if(updatedItem.nModified == 1) {
                        res.json({
                            isError: false,
                            message: "Updated Loan Table",
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['405'],
                            statuscode: 405,
                            details: null
                        }); 
                    }
                    console.log(updatedItem);
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    // savePlaidData: async(req, res, next)=>{
    //     try{
    //         let account_num = req.body.account_num;
    //         let account_routing = req.body.account_routing;
    //         let account_name = req.body.account_name;
    //         let account_type = req.body.account_type;
    //         let account_class = req.body.account_class;
    //         let studentId = req.body.studentId;
    //         let actoken = req.body.access_token;
    //         var request = require("request");
    //         models.student.findOne({ 'studentId': studentId }, function (error, item) {
    //             if (error) {
    //                 res.json({
    //                     isError: true,
    //                     message: errorMsgJSON['ResponseMsg']['405'],
    //                     statuscode: 405,
    //                     details: error
    //                 });
    //             }
    //             else {
    //                 var options = {
    //                     'method': 'GET',
    //                     'url': 'https://uat-api.synapsefi.com/v3.1/users/'+item.synpaseId+'?full_dehydrate=no',
    //                     'headers': {
    //                         'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
    //                         'X-SP-USER-IP': '127.0.0.1',
    //                         'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
    //                         'Content-Type': 'application/json'
    //                     }
    //                 };
    //                 request(options, function (error, response, body1) { 
    //                     var newbody = JSON.parse(body1);
    //                     var rt = newbody.refresh_token;
    //                     if (error) {
    //                         throw new Error(error);
    //                     }
    //                     else
    //                     {
    //                         var options = { method: 'POST',
    //                             url: 'https://uat-api.synapsefi.com/v3.1/oauth/'+item.synpaseId,
    //                             headers: {
    //                                 'X-SP-GATEWAY': 'client_id_nBlbiUfWOoV8AkG5Rp3qdMQKFuyzrTEms096DN0P|client_secret_XPfcAZOrnoxDd1whWTI7yCb2FveQ0HmkM6Ui509Y',
    //                                 'X-SP-USER-IP': '127.0.0.1',
    //                                 'X-SP-USER': '|10a4113e4fab640b2568c70bc9b79d9d',
    //                                 'Content-Type': 'application/json'
    //                             },
    //                             body: 
    //                             {
    //                                 "refresh_token":rt,
    //                                 "scope":[
    //                                     "USER|PATCH",
    //                                     "USER|GET",
    //                                     "NODES|POST",
    //                                     "NODES|GET",
    //                                     "NODE|GET",
    //                                     "NODE|PATCH",
    //                                     "NODE|DELETE",
    //                                     "TRANS|POST",
    //                                     "TRANS|GET",
    //                                     "TRAN|GET",
    //                                     "TRAN|PATCH",
    //                                     "TRAN|DELETE",
    //                                     "SUBNETS|GET",
    //                                     "SUBNETS|POST",
    //                                     "SUBNET|GET",
    //                                     "SUBNET|PATCH",
    //                                     "STATEMENTS|GET",
    //                                     "STATEMENT|GET"
    //                                 ]
    //                             },
    //                             json: true 
    //                         }
    //                         request(options, function (error, response, body2) { 
    //                             var oauth = body2.oauth_key;
    //                             if (error) throw new Error(error);
    //                             var options = {
    //                                 'method': 'POST',
    //                                 'url': 'https://uat-api.synapsefi.com/v3.1/users/'+item.synpaseId+'/nodes',
    //                                 'headers': {
    //                                     'X-SP-USER-IP': '127.0.0.1',
    //                                     'X-SP-USER': oauth + '|10a4113e4fab640b2568c70bc9b79d9d',
    //                                     'Content-Type': 'application/json'
    //                                 },
    //                                 body: JSON.stringify(
    //                                     {
    //                                         "type": "ACH-US",
    //                                         "info": {
    //                                             "nickname": account_name,
    //                                             "account_num": account_num,
    //                                             "routing_num": account_routing,
    //                                             "type": account_type,
    //                                             "class": account_class
    //                                         },
    //                                         "extra": {
    //                                             "supp_id": studentId,
    //                                             "other": {
    //                                                 "info": {
    //                                                     "phone_numbers": [
    //                                                         item.mobileNo
    //                                                     ],
    //                                                     "addresses": [
    //                                                         {
    //                                                             "street": item.address.address1,
    //                                                             "city": item.address.city,
    //                                                             "state": item.address.state,
    //                                                             "zipcode": item.address.zip
    //                                                         }
    //                                                     ],
    //                                                     "names": [
    //                                                         item.fname+' '+item.lname
    //                                                     ],
    //                                                     "emails": [
    //                                                         item.EmailId
    //                                                     ]
    //                                                 },
    //                                                 "transactions": req.body.transactions
    //                                             }
    //                                         }
    //                                     }
    //                                 )
    //                             };
    //                             request(options, async function (error, response, body3) { 
    //                                 var newbody = JSON.parse(body3); 
    //                                 if(newbody.success == true) {
    //                                     var listallbanks = [];
    //                                     item.synapseACHId.map(a=>{
    //                                         listallbanks.push(a.bankacc);                                                
    //                                     })
    //                                     var checknodeid = listallbanks.indexOf(newbody.nodes[0]._id);
    //                                     console.log("ALL BANKS ",listallbanks);
    //                                     console.log("ID EXISTS = ",checknodeid);

    //                                     if(checknodeid == -1) {
    //                                         var achaccount = { 'bankacc':newbody.nodes[0]._id, 'roundup': 'false' };
    //                                         var bankdetails = { 'synpaseNode': newbody.nodes[0]._id, 'bankName': account_name, 'accountNum': account_num, 'accountRouting':account_routing, 'accountType': account_type, 'accountClass' : account_class, 'access_token' : actoken, 'roundup': 'false'  }
    //                                         var achaccountdetails = newbody.nodes[0].info;
    //                                         var updatedata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'synapseACHId':achaccount }});      
    //                                         if(updatedata) {
    //                                             var updatebankdata = await models.student.updateOne({ 'studentId': studentId },{ $push: { 'bankAccounts':bankdetails }}); 
    //                                             if(updatebankdata) {
    //                                                 res.json({
    //                                                     isError: false,
    //                                                     message: "Bank Account Added successfully",
    //                                                     statuscode: 200,
    //                                                     details: bankdetails
    //                                                 });
    //                                             }
    //                                         }
    //                                         else
    //                                         {
    //                                             res.json({
    //                                                 isError: true,
    //                                                 message: errorMsgJSON['ResponseMsg']['404'],
    //                                                 statuscode: 404,
    //                                                 details: 'DB Update Failed'
    //                                             });
    //                                         }
    //                                     }
    //                                     else
    //                                     {
    //                                         res.json({
    //                                             isError: true,
    //                                             message: errorMsgJSON['ResponseMsg']['404'],
    //                                             statuscode: 404,
    //                                             details: 'Bank Already Exists'
    //                                         });
    //                                     }
    //                                 }
    //                                 else
    //                                 {
    //                                     res.json({
    //                                         isError: true,
    //                                         message: errorMsgJSON['ResponseMsg']['404'],
    //                                         statuscode: 404,
    //                                         details: newbody
    //                                     });
    //                                 }                    
    //                             });
    //                         });
    //                     }
    //                 });
    //             }
    //             // else
    //             // {
    //             //     res.json({
    //             //         isError: false,
    //             //         message: errorMsgJSON['ResponseMsg']['200'],
    //             //         statuscode: 200,
    //             //         details: test
    //             //     });
    //             //     console.log('ITEM',item);
    //             // }
    //         })
        
    //     }
    //     catch (error) {
    //         res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });
    //     }
    // }
}


async function encryptKeys(data, purpose) {
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
        stage: 'staging',
        purpose: purpose,
        origin: 'us-west-1'
    };

    const { result } = await encrypt(keyring, data, {
        encryptionContext: context,
    });

    return Buffer.from(result).toString("base64");

}

async function decryptKeys(data) {
    //console.log(data);
    const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
    const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });

    var decrypted_data_raw = Buffer.from(data,"base64");

    const { plaintext, messageHeader } = await decrypt(keyring, decrypted_data_raw);

    return Buffer.from(plaintext).toString("ascii");

}