const mongoose = require('mongoose');
var giftCardSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    studentId : [],
    giftCardId : {type: String,default:""},
    amount: {type: Number,default:""},
    type: {type: Number,default:"OPU"} // OPU = Once Per User, OG = Once Globally
    numberOfUses: {type: Number,default:""},
    expiry:{type: String,default:""}
},
{
    timestamps: true 
})


const giftCard = mongoose.model("giftCard", giftCardSchema)

module.exports = giftCard;