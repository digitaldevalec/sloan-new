const studentProfileModel =  require('./student-profile');
const timeTiLiveModel =  require('./timeToLive');
const tempOtpContainerModel =  require('./temp-otp-container');
const instituteModel =  require('./institute');
const notifications =  require('./notifications');
const payment =  require('./payment');
const recurring =  require('./recurring');
const roundups =  require('./roundups');
const giftcards =  require('./giftcards');
module.exports = {
    'student' : studentProfileModel,
    'timeTOLive':timeTiLiveModel,
    'tempOtpContainer':tempOtpContainerModel,
    'institute':instituteModel,
    'notifications':notifications,
    'payment':payment,
    'recurring':recurring,
    'roundups': roundups
    'giftcards': giftcards
}