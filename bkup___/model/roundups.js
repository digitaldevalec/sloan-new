const mongoose = require('mongoose');
const roundupsSchema = new mongoose.Schema({
            "_id" : mongoose.Schema.Types.ObjectId,
            "studentId":String,
            "flag": Boolean,
            "account_id": String,
            "accountnum": String,
            "account_owner": String,
            "amount": Number,
            "roundedupAmount": Number,
            "roundup": Number,
            "authorized_date": String,
            "category":[],
            "category_id": String,
            "date": String,
            "iso_currency_code": String,
            "location": [],
            "name": String,
            "payment_channel": String,
            "payment_meta": [],
            "pending": Boolean,
            "pending_transaction_id": String,
            "transaction_code": String,
            "transaction_id": String,
            "transaction_type": String,
            "unofficial_currency_code": String
},
{
    timestamps: true 
})
const roundupsContainer = mongoose.model("roundups", roundupsSchema)
module.exports = roundupsContainer