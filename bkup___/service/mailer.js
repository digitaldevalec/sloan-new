const nodemailer = require('nodemailer');

module.exports = {
  viaGmail: (data,callback) => {
   
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'sourav.weavers@gmail.com',
        pass: 'Sourav@weavers'
      },
      tls: {
        rejectUnauthorized: false
    }
    }); 

    const mailoption = {
      from: 'SLOAN<sourav.weavers@gmail.com>',
      to: data.receiver,
      subject: data.subject,
      text:data.msg.toString(),
     
    };

    transporter.sendMail(mailoption, (err, info) => {
      if (err) {
        console.log(err)
        callback(err,null)
      } else {
        callback(null,info)
      }
   });
   

  }
 
}